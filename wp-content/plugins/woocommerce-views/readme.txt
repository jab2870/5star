=== WooCommerce Views - Flexible Products Display ===
Contributors: dominykasgel, AmirHelzer
Donate link: http://wp-types.com
Tags: CMS, woocommerce, commerce, ecommerce, e-commerce, products, slider, table, display
Requires at least: 3.0
Tested up to: 3.3.1
Stable tag: 1.1.3.1

Display WooCommerce products in a sortable table, a grid or sliders on any WordPress page and using any theme.

== Description ==

Looking for a flexible way to display your products? WooCommerce Views lets you display products using a table, products grid or product sliders.

You can insert them into any WordPress content as shortcodes, using a convenient insert-icon in the post editor. This plugin is powered by [Views](http://wp-types.com/home/views-create-elegant-displays-for-your-content/) and shows the power of embedding Views into your theme or plugin.

= Products Table =

The products table shows the title, description, cost and image. It includes a search tool, pagination and sortable columns.

= Products Grid =

The grid shows your products in two compact columns. Like the table, it also includes pagination, supporting a large number of products without causing clutter.

= Product Sliders =

You can choose between two versions of product sliders, showing different slide display variations. Both sliders load only 'featured' products, so that you can display only highlighted items on your slider.

Unlike other sliders, these sliders use AJAX to load products from the database. This means that pages load much faster and cleaner and you can have a very large number of products in the slider, without confusing search engines and without causing load delay times.

= Documentation and Extensions =

You can read more about [WooCommerce Views](http://wp-types.com/documentation/views-inside/woocommerce-views/).

This plugin is built with **zero PHP code**. To develop it, we used the [Views](http://wp-types.com/home/views-create-elegant-displays-for-your-content/), exported the configuration and [embedded](http://wp-types.com/documentation/embedded-types-and-views/) it into this plugin.

You can customize these displays to your liking using the full version of Views (costs $49). This will allow to create any other display methods for your products or to change the settings of the Views provided in this plugin.

== Installation ==

1. Upload 'types' to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress


== Frequently Asked Questions ==

= Does this work with other e-commerce plugins? =

No. The display elements in WooCommerce Views are tailored for WooCommerce. We'll release other similar plugins for JigShop and WP E-Commerce.

= How can I customize the appearance of these Views? =

There's a lot that you can achieve with CSS. You can style the fonts and sizes of most display elements. If you want these Views to return something different, you'll need to use the full Views version. This costs $49, but we're sure you'll find it a valuable investment!

== Screenshots ==

1. Table output
2. Grid output
3. Slider1 output
4. Slider2 output

== Changelog ==

= 0.9 =
* First release

== Upgrade Notice ==

= 0.9.0 =
* This is the first release

= 0.9.2 =
* Update embedded versions of Types and Views
* Fix a bug during initialization

= 1.1.3 =
* Sync with Views 1.1.3

= 1.1.3.1 =
* Small bug fixes

