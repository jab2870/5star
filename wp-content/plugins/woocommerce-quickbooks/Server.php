<?php
/*
Plugin Name: SOD QuickBooks Connector
Plugin URI: http://woothemes.com/woocommerce
Description: A Quickbooks connector for woocommerce.
Version: 1.1.7
Author: 61 Designs
Author URI: http://www.sixtyonedesigns.com
License: GPL
Copyright: SixtyOneDesigns 2012
Requires at least: 3.1
Tested up to: 3.3
*/

/**
 * Required functions
 **/
if ( ! function_exists( 'is_woocommerce_active' ) ) require_once( 'woo-includes/woo-functions.php' );

/**
 * Plugin updates
 **/
if (is_admin()) {
	$woo_plugin_updater_catalog_visibility = new WooThemes_Plugin_Updater( __FILE__ );
	$woo_plugin_updater_catalog_visibility->api_key = '7182cf0aa86681e6a7d6106815638899';
	$woo_plugin_updater_catalog_visibility->init();
}
include(plugin_dir_path( __FILE__ ).'/admin/interface.php');
include(plugin_dir_path( __FILE__ ).'/admin/settings.php');
include(plugin_dir_path( __FILE__ ).'QuickBooks.php');
include(plugin_dir_path( __FILE__ ).'/classes/sod.quickbooks.php');
include(plugin_dir_path( __FILE__ ).'/classes/sod.quickbooks.data.woocommerce.php');
include(plugin_dir_path( __FILE__ ).'/classes/sod.quickbooks.webconnector.php');
add_action('admin_init','qb');
function qb(){
	$discount_listid = maybe_unserialize(get_option('_sod_quickbooks_discount_item'));
	
}
/*
 * Initialize Plugin 
 */
register_activation_hook( __FILE__, 'sod_quiickbooks_activation' );

//Generate a unique access key for the QB WebConnector
function sod_quiickbooks_activation(){
	global $quickbooks;
	if (class_exists( 'Woocommerce' ) ) :
		$quickbooks->dsn = 'mysql://'.DB_USER.':'.DB_PASSWORD.'@'.DB_HOST.'/'.DB_NAME;
		if(!QuickBooks_Utilities::initialized($quickbooks->dsn)):
				QuickBooks_Utilities::initialize($quickbooks->dsn);
		endif;
		if(!get_option('_quickbooks_connector_key')):
			/****Generate unique api key for url access **/
			$length = 12;
			$options = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$code = "";
			for($i = 0; $i < $length; $i++)
			{
				$key = rand(0, strlen($options) - 1);
				$code .= $options[$key];
			}
			update_option('_quickbooks_connector_key',$code);
		endif;
		global $wpdb;
  		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		$quickbooks_config = "CREATE TABLE quickbooks_config (
		  quickbooks_config_id int(10) unsigned NOT NULL AUTO_INCREMENT ,
		  qb_username varchar(40) COLLATE utf8_unicode_ci NOT NULL ,
		  module varchar(40) COLLATE utf8_unicode_ci NOT NULL ,
		  cfgkey varchar(40) COLLATE utf8_unicode_ci NOT NULL ,
		  cfgval varchar(40) COLLATE utf8_unicode_ci NOT NULL ,
		  cfgtype varchar(40) COLLATE utf8_unicode_ci NOT NULL ,
		  cfgopts text COLLATE utf8_unicode_ci NOT NULL ,
		  write_datetime datetime NOT NULL ,
		  mod_datetime datetime NOT NULL ,
		  PRIMARY KEY (quickbooks_config_id)
		);";
		dbDelta($quickbooks_config);
		$quickbooks_log = "CREATE TABLE quickbooks_log (
		  quickbooks_log_id int(10) unsigned NOT NULL AUTO_INCREMENT ,
		  quickbooks_ticket_id int(10) unsigned DEFAULT NULL ,
		  batch int(10) unsigned NOT NULL ,
		  msg text COLLATE utf8_unicode_ci NOT NULL ,
		  log_datetime datetime NOT NULL ,
		  PRIMARY KEY  (quickbooks_log_id) ,
		  KEY quickbooks_ticket_id (quickbooks_ticket_id) ,
		  KEY batch (batch)
		);";
		dbDelta($quickbooks_log);
		$quickbooks_oauth = "CREATE TABLE quickbooks_oauth (
		  quickbooks_oauth_id int(10) unsigned NOT NULL AUTO_INCREMENT ,
		  app_username varchar(255) COLLATE utf8_unicode_ci NOT NULL ,
		  app_tenant varchar(255) COLLATE utf8_unicode_ci NOT NULL ,
		  oauth_request_token varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL ,
		  oauth_request_token_secret varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL ,
		  oauth_access_token varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL ,
		  oauth_access_token_secret varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL ,
		  qb_realm varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL ,
		  qb_flavor varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL ,
		  qb_user varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL ,
		  request_datetime datetime NOT NULL ,
		  access_datetime datetime DEFAULT NULL ,
		  touch_datetime datetime DEFAULT NULL ,
		  PRIMARY KEY  (quickbooks_oauth_id)
		);";
		dbDelta($quickbooks_oauth);
		$quickbooks_recur = "CREATE TABLE quickbooks_recur (
		  quickbooks_recur_id int(10) unsigned NOT NULL AUTO_INCREMENT ,
		  qb_username varchar(40) COLLATE utf8_unicode_ci NOT NULL ,
		  qb_action varchar(32) COLLATE utf8_unicode_ci NOT NULL ,
		  ident varchar(40) COLLATE utf8_unicode_ci NOT NULL ,
		  extra text COLLATE utf8_unicode_ci ,
		  qbxml text COLLATE utf8_unicode_ci ,
		  priority int(10) unsigned DEFAULT '0' ,
		  run_every int(10) unsigned NOT NULL ,
		  recur_lasttime int(10) unsigned NOT NULL ,
		  enqueue_datetime datetime NOT NULL ,
		  PRIMARY KEY  (quickbooks_recur_id) ,
		  KEY  qb_username (qb_username,qb_action,ident) ,
		  KEY  priority (priority)
		);";
		dbDelta($quickbooks_recur);
		$sql = "CREATE TABLE quickbooks_queue (
		  quickbooks_queue_id int(10) NOT NULL AUTO_INCREMENT,
		  quickbooks_ticket_id int(10) DEFAULT NULL,
		  qb_username varchar(40) NOT NULL,
		  qb_action varchar(32) NOT NULL,
		  ident varchar(40) NOT NULL,
		  extra text,
		  qbxml text,
		  priority int(10) DEFAULT '0',
		  qb_status char(1) NOT NULL,
		  msg text,
		  enqueue_datetime datetime NOT NULL,
		  dequeue_datetime datetime DEFAULT NULL,
		  PRIMARY KEY  (quickbooks_queue_id),
		  KEY quickbooks_ticket_id (quickbooks_ticket_id),
		  KEY priority (priority),
		  KEY qb_username (qb_username,qb_action,ident,qb_status),
		  KEY qb_status (qb_status)
		); ";
		dbDelta($sql);
		$quickbooks_ticket = "CREATE TABLE quickbooks_ticket (
		  quickbooks_ticket_id int(10) unsigned NOT NULL AUTO_INCREMENT ,
		  qb_username varchar(40) COLLATE utf8_unicode_ci NOT NULL ,
		  ticket char(36) COLLATE utf8_unicode_ci NOT NULL ,
		  processed int(10) unsigned DEFAULT '0' ,
		  lasterror_num varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL ,
		  lasterror_msg varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL ,
		  ipaddr char(15) COLLATE utf8_unicode_ci NOT NULL ,
		  write_datetime datetime NOT NULL ,
		  touch_datetime datetime NOT NULL ,
		  PRIMARY KEY  (quickbooks_ticket_id) ,
		  KEY  ticket (ticket)
		);";
		dbDelta($quickbooks_ticket);
		$quickbooks_user = "CREATE TABLE quickbooks_user (
		  qb_username varchar(40) COLLATE utf8_unicode_ci NOT NULL ,
		  qb_password varchar(255) COLLATE utf8_unicode_ci NOT NULL ,
		  qb_company_file varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL ,
		  qbwc_wait_before_next_update int(10) unsigned DEFAULT '0' ,
		  qbwc_min_run_every_n_seconds int(10) unsigned DEFAULT '0' ,
		  status char(1) COLLATE utf8_unicode_ci NOT NULL ,
		  write_datetime datetime NOT NULL ,
		  touch_datetime datetime NOT NULL ,
		  PRIMARY KEY  (qb_username)
		);";
		dbDelta($quickbooks_user);
		$user = maybe_unserialize(get_option("sod_quickbooks_webconnector"));
		
		if($user):
			$webconnector = new SOD_Quickbooks_Data;
			$wpdb->get_results( $wpdb->prepare( "DELETE FROM quickbooks_user" ) );
			$wpdb->get_results( $wpdb->prepare( "UPDATE quickbooks_queue SET qb_username = '%s'", $user['username'] ) );
			QuickBooks_Utilities::createUser($webconnector->dsn, $user['username'], $webconnector->decode($user['password']));					
		endif;
		$inventory = array(   
			'sync_inv' 				=> null,
			'product_identifier' 	=> 'sku',
			'inv_sync_frequency' 	=> '180',
			'create_new_items' 		=> null,
			'auto_create'			=> null,
			'sync_price'			=> null,
			'default_itemtype'		=> 'inventory'
		);
		$settings = array(
	       'income' 				=> null,
	       'cogs' 					=> null,
	       'deposits' 				=> null,
	       'receivables' 			=> null,
	       'post_orders' 			=> 'on',
	       'post_order_type'		=> 'sales_order',
	       'create_so_invoices' 	=> 'on',
	       'track_parents'			=> null,
	       'track_variations'		=> 'on',
	       'order_status_trigger'	=> 'completed',
	       'prefixes' => array(
           		'sr_prefix' 		=> 'SR',
           		'so_prefix' 		=> 'SO',
           		'invoice_prefix' 	=> 'INV',
           		'payment_prefix' 	=> 'PYMNT'
			),
	       'create_customer_account'=> 'on',
	       'customer_identifier' =>	array(
           		'first' 			=> '_billing_first_name',
           		'second' 			=> '_billing_last_name',
           		'third' 			=> '_customer_user'
			),
		   'customer' 				=> null,
	       'payment_mappings' => array(
	           	'bacs' 				=> null,
	           	'cheque' 			=> null,
	           	'paypal' 			=> null
			),
	       'taxes_mappings' =>array(
           		'reduced-rate' 		=> null, 
           		'zero-rate' 		=> null,
           		'standard' 			=> null
			),
	       'status_mappings' => array(				        
	           	'pending' 			=>'true',
	           	'on-hold' 			=>'true',
	           	'processing' 		=>'true',
	           	'completed' 		=>'false'
	         ),
	       'shipping_item' 			=> null
	       );
		   add_option('sod_quickbooks_inv_defaults', $inventory);
		   add_option('sod_quickbooks_defaults', $settings);
		   update_option('jigoshop_enable_sku', 'yes');
	endif;
}
/* Deactivation hook*/
register_deactivation_hook(__FILE__, 'sod_quiickbooks_deactivation');
function sod_quiickbooks_deactivation(){
	global $wpdb;		
	$delete_all = false;	
	$delete_all = apply_filters('sod_qb_deactivation', $delete_all);	
	if($delete_all):
	    $tables = array(
	    	'quickbooks_user',
	    	'quickbooks_ticket',
	    	'quickbooks_recur',
	    	'quickbooks_queue',
	    	'quickbooks_notify',
	    	'quickbooks_log',
	    	'quickbooks_ident',
			'quickbooks_connection',
			'quickbooks_config'
		);
		$tables = apply_filters('sod_qb_deactivation_tables', $tables);	
	    //Delete any options thats stored also?
		foreach($tables as $table){
			$wpdb->query("DROP TABLE IF EXISTS $table");
		}
		$options = array(
			'sod_quickbooks_webconnector',
			'sod_quickbooks_product_sync',
			'quickbooks_connected',
			'_sod_quickbooks_classes',
			'_sod_quickbooks_expense_accounts',
			'_sod_quickbooks_shipping_items',
			'_sod_quickbooks_salestax_rates',
			'_sod_quickbooks_salestax_codes',
			'_sod_quickbooks_income_accounts',
			'_sod_quickbooks_cogs_accounts',
			'_sod_quickbooks_payment_methods',
			'_sod_quickbooks_deposit_accounts',
			'_sod_quickbooks_asset_accounts',
			'_sod_quickbooks_customer_accounts',
			'_sod_quickbooks_receivables',
			'webconnector_generated',
			'started_setup'
		);
		$options = apply_filters('sod_qb_deactivation_tables', $options);
		foreach($options as $option){
			delete_option($option);
		}
	endif;
}
/*Parse SOAP Server Request*/ 
add_action("parse_request", "sod_qbconnector_request");
function sod_qbconnector_request(){
	global $woocommerce;
	$webconnector = new SOD_Webconnector;
	$quickbooks = new SOD_Quickbooks_Data;
	$quickbooks->dsn = 'mysql://'.DB_USER.':'.DB_PASSWORD.'@'.DB_HOST.'/'.DB_NAME;
	$key = get_option('_quickbooks_connector_key');
	$map = $webconnector->getFunctionMappings();
	$errmap = $webconnector->getErrorMappings();
	$hooks = array(	);
	$log_level = QUICKBOOKS_LOG_VERBOSE;
	$soapserver = QUICKBOOKS_SOAPSERVER_BUILTIN;		// A pure-PHP SOAP server (no PHP ext/soap extension required, also makes debugging easier)
	$soap_options = array();
	$handler_options = array();		// See the comments in the QuickBooks/Server/Handlers.php file
	$driver_options = array(
		'max_log_history' => 100,    // Limit the number of quickbooks_log entries to 1024
    	'max_queue_history' => 1000     // Limit the number of quickbooks_queue entries to 64
    );
	$callback_options = array();
	if(isset($_GET["qbconnector"])){

		//var_dump($item);
		$check_key = $_GET['qbconnector'];
		if ($check_key==$key){
			$connector = get_option('quickbooks_connected');
			
			if(($quickbooks->inventory_settings->sync_inv && $quickbooks->inventory_settings->sync_inv=="on") || ($quickbooks->inventory_settings->sync_price && $quickbooks->inventory_settings->sync_price=="on")){
				$options = get_option('sod_quickbooks_product_sync');
				if(!array_key_exists('last_run',$options)){
					$Queue = new QuickBooks_WebConnector_Queue($quickbooks->dsn);
					$options['last_run']=time();	
					$Queue->enqueue('QUICKBOOKS_INVENTORY_SYNC_START',uniqid(),1);
					update_option('sod_quickbooks_product_sync',$options);
				}else{
					$frequency = !empty($quickbooks->inventory_settings->inv_sync_frequency) ? $quickbooks->inventory_settings->inv_sync_frequency : 5;
					$next_time = (int)$options['last_run'] + (int)($frequency *60);
					if($next_time < time()){
						$Queue = new QuickBooks_WebConnector_Queue($quickbooks->dsn);
						$options['last_run']=time();	
						$Queue->enqueue('QUICKBOOKS_INVENTORY_SYNC_START',uniqid(),1);
						update_option('sod_quickbooks_product_sync',$options);
						
					}
				}
			}
			 do_action('sod_qbpos_main_queue', $quickbooks);
			 //var_dump(SOD_Webconnector::_quickbooks_salesorder_add_request($requestID=12, $user, $action, $ID=25, $extra, $err, $last_action_time, $last_actionident_time, $version, $locale));
			 $Server = new QuickBooks_WebConnector_Server($quickbooks->dsn, $map, $errmap, $hooks, $log_level, $soapserver, QUICKBOOKS_WSDL, $soap_options, $handler_options, $driver_options, $callback_options);
			 $response = $Server->handle(true, true);
			die;
		}else{
			die("Nothing here");
		}
	}else{
		
	}
}

/*
 * Add the query variable for the qbconnector
 */
add_filter('query_vars', 'sod_qbconnector_query_vars');
function sod_qbconnector_query_vars($vars) {
	$vars[] = 'qbconnector';
	return $vars;
}

/*
 * WooCommerce Hooks
 */
//1. Add new order to queue when order is placed
add_action('woocommerce_order_status_completed', 'sod_quickbooks_send_order', 12);
function sod_quickbooks_send_order($order_id){
	global $woocommerce;
	$quickbooks 		= new SOD_Quickbooks_Data;
	$quickbooks->ID	 	= $order_id;
	$quickbooks->load("order");
	$Queue 				= new QuickBooks_WebConnector_Queue($quickbooks->dsn);
	$already_queued		= get_post_meta($order_id, '_qb_initial_queue', true);
	if($quickbooks->settings->order_status_trigger == "completed" && $already_queued != "yes" ):
		if(!$quickbooks->quickbooks):
			if($quickbooks->settings->post_orders =='on'){
				if($quickbooks->settings->create_customer_account =='on'){
				/*1. Check for customer ListID 
				 * if exists, send directly as receipt;
				 */
					if($quickbooks->customerListID){
						switch($quickbooks->settings->post_order_type){
							case "sales_order":
							$Queue->enqueue('QUICKBOOKS_ADD_SO',$order_id,7);
								break;
							case "sales_receipt":
								$Queue->enqueue('QUICKBOOKS_ADD_RECEIPT',$order_id,8);
								break; 
						}
					}else{
						/*2. If No CustomerListID
					 	* send as add customer request, then add SO/SR request
					 	*/
						$Queue->enqueue('QUICKBOOKS_CUST_QUERY',$order_id,6);
					}
				}else{
					/*3. Post Default Account 
				 	* If not posting to unique customer accounts, post to default;
				 	*/
					switch($quickbooks->settings->post_order_type){
							case "sales_order":
							$Queue->enqueue('QUICKBOOKS_ADD_SO',$order_id,7);
								break;
							case "sales_receipt":
							$Queue->enqueue('QUICKBOOKS_ADD_RECEIPT',$order_id,8);
								break;
					} 
				}
				update_post_meta($order_id, '_qb_initial_queue', 'yes');
			} //If not posting orders, do nothing;
		
		endif; //If hasn't been already sent
	endif;
}
//1. Add new order to queue when order is placed
add_action('woocommerce_order_status_processing', 'sod_quickbooks_send_order_as_processing', 12);
function sod_quickbooks_send_order_as_processing($order_id){
	global $woocommerce;
	$quickbooks 		= new SOD_Quickbooks_Data;
	$quickbooks->ID	 	= $order_id;
	$quickbooks->load("order");
	$Queue 				= new QuickBooks_WebConnector_Queue($quickbooks->dsn);
	$already_queued		= get_post_meta($order_id, '_qbpos_initial_queue', true);
	
	if($quickbooks->settings->order_status_trigger == "processing" && $already_queued != "yes" ):
		if(!$quickbooks->quickbooks):
			if($quickbooks->settings->post_orders =='on'){
				if($quickbooks->settings->create_customer_account =='on'){
				/*1. Check for customer ListID 
				 * if exists, send directly as receipt;
				 */
					if($quickbooks->customerListID){
						switch($quickbooks->settings->post_order_type){
							case "sales_order":
							$Queue->enqueue('QUICKBOOKS_ADD_SO',$order_id,7);
								break;
							case "sales_receipt":
								$Queue->enqueue('QUICKBOOKS_ADD_RECEIPT',$order_id,8);
								break; 
						}
					}else{
						/*2. If No CustomerListID
					 	* send as add customer request, then add SO/SR request
					 	*/
						$Queue->enqueue('QUICKBOOKS_CUST_QUERY',$order_id,6);
					}
				}else{
					/*3. Post Default Account 
				 	* If not posting to unique customer accounts, post to default;
				 	*/
					switch($quickbooks->settings->post_order_type){
							case "sales_order":
							$Queue->enqueue('QUICKBOOKS_ADD_SO',$order_id,7);
								break;
							case "sales_receipt":
							$Queue->enqueue('QUICKBOOKS_ADD_RECEIPT',$order_id,8);
								break;
					} 
				}
				update_post_meta($order_id, '_qb_initial_queue', 'yes');
			} //If not posting orders, do nothing;
		
		endif; //If hasn't been already sent
	endif;
}
//1. Add new order to queue when order is placed
add_action('woocommerce_order_status_pending', 'sod_quickbooks_send_order_as_pending', 12);
function sod_quickbooks_send_order_as_pending($order_id){
	global $woocommerce;
	$quickbooks 		= new SOD_Quickbooks_Data;
	$quickbooks->ID	 	= $order_id;
	$quickbooks->load("order");
	$Queue 				= new QuickBooks_WebConnector_Queue($quickbooks->dsn);
	$already_queued		= get_post_meta($order_id, '_qbpos_initial_queue', true);
	
	if($quickbooks->settings->order_status_trigger == "pending" && $already_queued != "yes"):
		if(!$quickbooks->quickbooks):
			if($quickbooks->settings->post_orders =='on'){
				if($quickbooks->settings->create_customer_account =='on'){
				/*1. Check for customer ListID 
				 * if exists, send directly as receipt;
				 */
					if($quickbooks->customerListID){
						switch($quickbooks->settings->post_order_type){
							case "sales_order":
							$Queue->enqueue('QUICKBOOKS_ADD_SO',$order_id,7);
								break;
							case "sales_receipt":
								$Queue->enqueue('QUICKBOOKS_ADD_RECEIPT',$order_id,8);
								break; 
						}
					}else{
						/*2. If No CustomerListID
					 	* send as add customer request, then add SO/SR request
					 	*/
						$Queue->enqueue('QUICKBOOKS_CUST_QUERY',$order_id,6);
					}
				}else{
					/*3. Post Default Account 
				 	* If not posting to unique customer accounts, post to default;
				 	*/
					switch($quickbooks->settings->post_order_type){
							case "sales_order":
							$Queue->enqueue('QUICKBOOKS_ADD_SO',$order_id,7);
								break;
							case "sales_receipt":
							$Queue->enqueue('QUICKBOOKS_ADD_RECEIPT',$order_id,8);
								break;
					} 
				}
				update_post_meta($order_id, '_qb_initial_queue', 'yes');
			} //If not posting orders, do nothing;
		
		endif; //If hasn't been already sent
	endif;
}
// add_action('admin_init', 'sod_check');
// function sod_check(){
	// $tax_rates 			= get_option('woocommerce_tax_rates');
	// var_dump($tax_rates);
// }
?>