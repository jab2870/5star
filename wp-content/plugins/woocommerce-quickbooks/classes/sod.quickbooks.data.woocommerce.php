<?php 
class SOD_Quickbooks_Data extends SOD_Quickbooks{
	var $settings;
	var $inventory;
	public function __construct()
	{
		$this->dsn = 'mysql://'.DB_USER.':'.DB_PASSWORD.'@'.DB_HOST.'/'.DB_NAME;
		$this->order_types = array(
			"sales_receipt"			=> "Sales Receipt",
			"sales_order"			=> "Sales Order"
		);
		$this->customer_mapping = array(
			''					 	=> 'None',
			'_billing_first_name'	=> 'First Name',
			'_billing_last_name'	=> 'Last Name',
			'_customer_user'		=> 'Customer ID',
			'_customer_login'		=> 'Customer Login'
		);
		$inventory = array(   
			'sync_inv' 				=> null,
			'product_identifier' 	=> 'sku',
			'inv_sync_frequency' 	=> '180',
			'create_new_items' 		=> null,
			'auto_create'			=> null,
			'sync_price'			=> null,
			'default_itemtype'		=> 'inventory'
		);
		$settings = array(
	       'income' 				=> null,
	       'cogs' 					=> null,
	       'deposits' 				=> null,
	       'receivables' 			=> null,
	       'post_orders' 			=> 'on',
	       'post_order_type'		=> 'sales_order',
	       'create_so_invoices' 	=> 'on',
	       'track_parents'			=> null,
	       'track_variations'		=> 'on',
	       'order_status_trigger'	=> 'completed',
	       'prefixes' => array(
           		'sr_prefix' 		=> 'SR',
           		'so_prefix' 		=> 'SO',
           		'invoice_prefix' 	=> 'INV',
           		'payment_prefix' 	=> 'PYMNT'
			),
	       'create_customer_account'=> 'on',
	       'customer_identifier' =>	array(
           		'first' 			=> '_billing_first_name',
           		'second' 			=> '_billing_last_name',
           		'third' 			=> '_customer_user'
			),
		   'customer' 				=> null,
	       'payment_mappings' => array(
	           	'bacs' 				=> null,
	           	'cheque' 			=> null,
	           	'paypal' 			=> null
			),
	       'taxes_mappings' =>array(
           		'reduced-rate' 		=> null, 
           		'zero-rate' 		=> null,
           		'standard' 			=> null
			),
	       'status_mappings' => array(				        
	           	'pending' 			=>'true',
	           	'on-hold' 			=>'true',
	           	'processing' 		=>'true',
	           	'completed' 		=>'false'
	         ),
	       'shipping_item' 			=> null
	       );
		
		$iventory_obj = get_option('sod_quickbooks_inv_defaults')!=false ? get_option('sod_quickbooks_inv_defaults') : $inventory;
		$settings_obj = get_option('sod_quickbooks_defaults')!=false ? get_option('sod_quickbooks_defaults') : $settings;
		$this->settings = (object)$settings_obj;
		$this->inventory_settings = (object)$iventory_obj;
		
	}
	
	function get_sales_tax(){
		$tax_info=array(
			'codes'=>get_option('_sod_quickbooks_salestax_codes'),
			'rates'=>get_option('_sod_quickbooks_salestax_rates')
			);
		return $tax_info; 
	}
	function update_quickbooks_response_info( $arr , $key=null){
		
		$quickbooks		= new SOD_Quickbooks_Data();
		$quickbooks->ID	= $this->ID;
		$quickbooks->load('item');
		$identifier 	= $quickbooks->inventory_settings->product_identifier =="sku"? $quickbooks->sku : $quickbooks->ID;
		if($key):
			
			foreach($arr as $arr_key=>$arr_value){
				
				$quickbooks_data[$key][$arr_key]=$arr_value;
				
				update_post_meta($this->ID , '_quickbooks_data', $quickbooks_data);
			}
				
		else:
			
			if($identifier == $arr['Name']):
				
				update_post_meta($this->ID , '_quickbooks_data', $arr);
			
				update_post_meta($this->ID , '_qb_listid', $arr['ListID']);
				
				update_post_meta($this->ID , '_qb_item_type', $arr['ItemType']);
				
				update_post_meta($this->ID , '_qb_fullname', $arr['FullName']);
				
			endif;
			
		endif;
	
	}
	function check_defaults(){
		$id = uniqid();
		$Queue = new  QuickBooks_WebConnector_Queue($this->dsn);
		$started = get_option('started_setup');
		$options = array(
			'_sod_quickbooks_income_accounts'=>'QUICKBOOKS_INCOME_ACCOUNTS_SETUP',
			'_sod_quickbooks_cogs_accounts'=>'QUICKBOOKS_COGS_ACCOUNTS_SETUP',
			'_sod_quickbooks_payment_methods'=>'QUICKBOOKS_PAYMENT_METHODS',
			'_sod_quickbooks_deposit_accounts'=>'QUICKBOOKS_DEPOSIT_ACCTS',
			'_sod_quickbooks_asset_accounts'=>'QUICKBOOKS_ASSET_ACCTS',
			'_sod_quickbooks_customer_accounts'=>'QUICKBOOKS_CUST_ACCT_SETUP',
			'_sod_quickbooks_receivables'=>'QUICKBOOKS_RECEIVABLES',
			'_sod_quickbooks_salestax_codes'=>'QUICKBOOKS_SALESTAX_SETUP',
			'_sod_quickbooks_salestax_rates'=>'QUICKBOOKS_SALESTAX_RATES_SETUP',
			'_sod_quickbooks_shipping_items'=>'QUICKBOOKS_ALL_INV_SETUP',
			'_sod_quickbooks_expense_accounts'=>'QUICKBOOKS_EXPENSE_ACCOUNT_SETUP',
			
			"_sod_quickbooks_discount_account"=>'QUICKBOOKS_DISOCUNT_ACCT_ADD',
			"_sod_quickbooks_discount_item"=>'QUICKBOOKS_DISOCUNT_ITEM_ADD',
		);
		if(empty($started) && get_option('quickbooks_connected')):
			foreach($options as $name=>$queue_value){
				if(!get_option($name)){
					$Queue->enqueue($queue_value,$id,99);	
				}	
			}
			update_option('started_setup','started');
		endif;
	}

	function qb_auto_create(){
		global $wpdb;	
		$id 		= uniqid();
		$quickbooks = new SOD_Quickbooks_Data;
		$Queue 		= new  QuickBooks_WebConnector_Queue($quickbooks->dsn);
		$options 	= array(
			'QUICKBOOKS_INCOME_ACCT_AUTO_ADD'		=>'99991',
			'QUICKBOOKS_COGS_ACCT_AUTO_ADD'			=>'99992',
			'QUICKBOOKS_DEPOSIT_ACCT_AUTO_ADD'		=>'99993',
			'QUICKBOOKS_ASSET_ACCT_AUTO_ADD'		=>'99994',
			'QUICKBOOKS_AR_ACCT_AUTO_ADD'			=>'99995',
			'QUICKBOOKS_EXPENSE_ACCT_AUTO_ADD'		=>'99996',
			'QUICKBOOKS_NO_TAX_AUTO_ADD'			=>'99997',
			'QUICKBOOKS_SHIPPING_AUTO_ADD'			=>'99998',
			'QUICKBOOKS_TAX_ITEMS_AUTO_ADD'			=>'99999',
		);
		$wpdb->get_results( $wpdb->prepare( "TRUNCATE TABLE quickbooks_queue" ) );
		foreach($options as $option=>$id){
				$Queue->enqueue( $option, $id, 9999 );	
		}
		
		
	}
	function recheck_defaults(){
		global $wpdb;
		$id 		= uniqid();
		$quickbooks = new SOD_Quickbooks_Data;
		$Queue 		= new  QuickBooks_WebConnector_Queue($quickbooks->dsn);
		$started 	= get_option('started_setup');
		$options 	= array(
			'_sod_quickbooks_income_accounts'=>'QUICKBOOKS_INCOME_ACCOUNTS_SETUP',
			'_sod_quickbooks_cogs_accounts'=>'QUICKBOOKS_COGS_ACCOUNTS_SETUP',
			'_sod_quickbooks_payment_methods'=>'QUICKBOOKS_PAYMENT_METHODS',
			'_sod_quickbooks_deposit_accounts'=>'QUICKBOOKS_DEPOSIT_ACCTS',
			'_sod_quickbooks_asset_accounts'=>'QUICKBOOKS_ASSET_ACCTS',
			'_sod_quickbooks_customer_accounts'=>'QUICKBOOKS_CUST_ACCT_SETUP',
			'_sod_quickbooks_receivables'=>'QUICKBOOKS_RECEIVABLES',
			'_sod_quickbooks_salestax_codes'=>'QUICKBOOKS_SALESTAX_SETUP',
			'_sod_quickbooks_salestax_rates'=>'QUICKBOOKS_SALESTAX_RATES_SETUP',
			'_sod_quickbooks_shipping_items'=>'QUICKBOOKS_ALL_INV_SETUP',
			'_sod_quickbooks_expense_accounts'=>'QUICKBOOKS_EXPENSE_ACCOUNT_SETUP',
			"_sod_quickbooks_discount_account"=>'QUICKBOOKS_DISOCUNT_ACCT_ADD',
			"_sod_quickbooks_discount_item"=>'QUICKBOOKS_DISOCUNT_ITEM_ADD',
		);
		
		foreach($options as $name=>$queue_value){
			
				$Queue->enqueue($queue_value,$id,99);	
				
		}
		update_option('started_setup','started');
		
	}
	function get_accounts($type){
		switch($type){
			case "Income":
				$option = "_sod_quickbooks_income_accounts";
				break;
			case "Currencies":
				$option = "_sod_quickbooks_currencies";
				break;
			case "COGS":
				$option = "_sod_quickbooks_cogs_accounts";
				break;
			case "Deposits":
				$option = "_sod_quickbooks_deposit_accounts";
				break;
			case "Receivables":
				$option = "_sod_quickbooks_receivables";
				break;
			case "Expense":
				$option = "_sod_quickbooks_expense_accounts";
				break;
			case "Currency Receivables":
				$option = "_sod_quickbooks_ara_accounts";
				break;
			case "Assets":
				$option = "_sod_quickbooks_asset_accounts";
				break;
			case "Customers":
				$option = "_sod_quickbooks_customer_accounts";
				break;
			case "PaymentMethods":
				$option = "_sod_quickbooks_payment_methods";
				break;
			case "Shipping":
				$option = "_sod_quickbooks_shipping_items";
				break;
			case "SalesTaxCodes":
				$option = "_sod_quickbooks_salestax_codes";
				break;
			case "SalesTaxRates":
				$option = "_sod_quickbooks_salestax_rates";
				break;
			case "Classes":
				$option = "_sod_quickbooks_classes";
				break;
			case "DiscountAccount":
				$option = "_sod_quickbooks_discount_account";
				break;
			case "DiscountItem":
				$option = "_sod_quickbooks_discount_item";
				break;
		}
		return get_option($option);
	}
	function set_accounts($type, $arr){
		switch($type){
			case "Income":
				$option = "_sod_quickbooks_income_accounts";
				break;
			case "Currencies":
				$option = "_sod_quickbooks_currencies";
				break;
			case "COGS":
				$option = "_sod_quickbooks_cogs_accounts";
				break;
			case "Deposits":
				$option = "_sod_quickbooks_deposit_accounts";
				break;
			case "Receivables":
				$option = "_sod_quickbooks_receivables";
				break;
			case "Expense":
				$option = "_sod_quickbooks_expense_accounts";
				break;
			case "Currency Receivables":
				$option = "_sod_quickbooks_ara_accounts";
				break;
			case "Assets":
				$option = "_sod_quickbooks_asset_accounts";
				break;
			case "Customers":
				$option = "_sod_quickbooks_customer_accounts";
				break;
			case "PaymentMethods":
				$option = "_sod_quickbooks_payment_methods";
				break;
			case "Shipping":
				$option = "_sod_quickbooks_shipping_items";
				break;
			case "SalesTaxCodes":
				$option = "_sod_quickbooks_salestax_codes";
				break;
			case "SalesTaxRates":
				$option = "_sod_quickbooks_salestax_rates";
				break;
			case "Classes":
				$option = "_sod_quickbooks_classes";
				break;
			case "DiscountAccount":
				$option = "_sod_quickbooks_discount_account";
				break;
			case "DiscountItem":
				$option = "_sod_quickbooks_discount_item";
				break;	
		}
		update_option($option,$arr);
		return true;
	}
	function set_preferences($arr){
		update_option('quickbooks_connected',$arr);
	}
	function update_customer_listid($value){
		update_post_meta($this->ID,'_customerListID',$value);
		update_user_meta($this->ID,'_customerListID',$value);
	}
	function update_price($product,$price){
		update_post_meta($product->ID,'_regular_price',$price['SalesPrice']);
		update_post_meta($product->ID,'_price',$price['SalesPrice']);
	}
	function update_stock($qty){
		update_post_meta($ID,'_stock',$qty);
		if((int)$qty>0){
			update_post_meta($ID,'_stock_status','instock');
		}else{
			update_post_meta($ID,'_stock_status','outofstock');
		}
		$this->kill_transients($this->ID);
	}
	function get_cart_tax_classes(){
		$temp = array_filter(array_map('trim', explode("\n", get_option('woocommerce_tax_classes'))));
		$tax_classes = array();
		$tax_classes['standard']="Standard"; 
		foreach($temp as $item){
			$tax_classes[str_replace(" ","_",strtolower($item))]=$item;
		}
		return $tax_classes;
	}
	function get_cart_tax_rates(){
		$tax_rates = get_option('woocommerce_tax_rates');
		return $tax_rates;
	}
	function get_woocommerce_local_tax_rates(){
		$tax_rates = get_option('woocommerce_local_tax_rates');
		return $tax_rates;
	}
	function inventory_sync_response($arr){
		
		$identifier = $this->inventory_settings->product_identifier;
		
		$global_stock_management = get_option('woocommerce_manage_stock');
		
		switch($identifier){
			
			case "ID":
				
				$this->update_item_by_id($arr, $global_stock_management);
					
				break;
				
			case "sku":
				
				$this->update_item_by_sku($arr, $global_stock_management);
						
				break;
		}
		return true;
	}
	
	function inventory_check_for_new(){
		wp_reset_query();
			$products = $this->posts_without_meta('_quickbooks_data','product','','','');
			
			$products = apply_filters('sod_qb_products_missing_qb_data', $products);
			
			if($products){
				foreach($products as $product){
					$item = new SOD_Quickbooks_Data;
					$item->ID = $product->ID;
					$item->load("item");
					/*Track Inventory*/
					if(($item->manage_stock && $item->sync_status =="on")):
						
						if($item->has_children):
							
							if($item->inventory_settings->track_parents == "on"):
								
								if( $item->inventory_settings->auto_create == "on" ):
									
									$Queue = new QuickBooks_WebConnector_Queue($this->dsn);
									
									$Queue->enqueue('QUICKBOOKS_ITEM_INVENTORY_ADD',$item->ID,98);
									
								endif;
								
							endif;
							
						else:
							
							if( $item->inventory_settings->auto_create == "on" ):
								
								$Queue = new QuickBooks_WebConnector_Queue($this->dsn);
								
								$Queue->enqueue('QUICKBOOKS_ITEM_INVENTORY_ADD',$item->ID,98);
								
							endif;
							
						endif;
						
					endif;
					if($item->sync_status =="on" && $item->children_with_stock):
						
						if($item->has_children):
							
							if($item->inventory_settings->track_parents == "on"):
								
								if( $item->inventory_settings->auto_create == "on" ):
									
									$Queue = new QuickBooks_WebConnector_Queue($this->dsn);
									
									$Queue->enqueue('QUICKBOOKS_ITEM_INVENTORY_ADD',$item->ID,98);
									
								endif;
								
							endif;
							
						else:
							
							if( $item->inventory_settings->auto_create == "on" ):
								
								$Queue = new QuickBooks_WebConnector_Queue($this->dsn);
								
								$Queue->enqueue('QUICKBOOKS_ITEM_INVENTORY_ADD',$item->ID,98);
								
							endif;
							
						endif;
						
					endif;
				}	
			}
			
			$variations = $this->posts_without_meta('_quickbooks_data','product_variation','','','');
			
			$variations = apply_filters('sod_qb_variations_missing_qb_data', $variations);
			if($variations){
				foreach($variations as $variation){
					$var = new SOD_Quickbooks_Data;
					$var->ID = $variation->ID;
					$var->load("item");
					$parent = new SOD_Quickbooks_Data;
					$parent->ID = $var->item_parent;
					$parent->load("item");
					
					if(($var->stock !="no" && $parent->sync_status =="on")||($parent->sync_status="on" && $parent->manage_stock)):
						
						if( $item->inventory_settings->auto_create == "on" && $item->inventory_settings->track_variations == "on" ):
							
							$Queue = new QuickBooks_WebConnector_Queue($this->dsn);
							
							$Queue->enqueue('QUICKBOOKS_ITEM_INVENTORY_ADD',$var->ID,1);
							
						endif;
					endif;	
				}	
			}
	
	}
	
	function recheck_stock_status(){
		
		$args = array( 
			'post_type'=> array(
				'product',
				'product_variation'
			),
			'posts_per_page'=>-1
		);
		
		$query = new WP_Query();
		
		$query->query($args);
		
		$products = $query->posts;
		
		$global_stock_management = get_option('woocommerce_manage_stock');
		
		if($products){
			
			foreach($products as $product){
				
				$woo  = new WC_Product($product->ID);
				
				if($product->post_parent):
					
					$parent_id 		= $product->post_parent;
					
					$sync_status 	= get_post_meta($parent_id, '_sync_status', true) ? get_post_meta($parent_id, '_sync_status', true) : false;
					
					$manage_stock 	= get_post_meta($parent_id, '_manage_stock', true) ? get_post_meta($parent_id, '_manage_stock', true) : false;
					
				else:
					
					$sync_status 	= get_post_meta($product->ID, '_sync_status', true) ? get_post_meta($product->ID, '_sync_status', true) : false;;
					
					$manage_stock 	= get_post_meta($product->ID, '_manage_stock', true) ? get_post_meta($product->ID, '_manage_stock', true) : false;;
					
				endif;
				
				switch($global_stock_management){
					
					case"yes":
						if($quickbooks->inventory_settings->change_stock_status == "on"):
							
							if((int)$woo->get_stock_quantity() >0 && $manage_stock=="yes"):
								
									update_post_meta($product->ID,'_stock_status','instock');
								
							elseif((int)$woo->get_stock_quantity() ==0 && $this->check_variants_stock_levels()==true && $manage_stock=="yes"):
								
									update_post_meta($product->ID,'_stock_status','instock');
								
							elseif((int)$woo->get_stock_quantity() <=0 && $this->check_variants_stock_levels()==false && $manage_stock=="yes"):
								
									update_post_meta($product->ID,'_stock_status','outofstock');
								
							else:
								
								update_post_meta($product->ID,'_stock_status','instock');
								
							endif;
							
							if($woo->is_type('variable')):
								
								$woo->variable_product_sync();
								
							endif;
							
						endif;
						
						break;
						
					case"no":
						
						$arr = maybe_unserialize(get_post_meta($product->ID,'_quickbooks_data'));
						
						if($quickbooks->inventory_settings->change_stock_status == "on"):
							
							if((int)$arr[0]['QuantityOnHand'] > 0 ):
								
								update_post_meta($product->ID,'_stock_status','instock');
								
							elseif((int)$arr[0]['QuantityOnHand'] < 0 ):
									
								update_post_meta($product->ID,'_stock_status','outofstock');
								
							elseif((int)$arr[0]['QuantityOnHand'] == 0 ):
							
								update_post_meta($product->ID,'_stock_status','outofstock');
								
							endif;
						
						endif;
						
						break;
				}
			}
		};		
		do_action('sod_qb_recheck_stock_status_after');		
	}
	function load($object){
		switch($object){
			case "order":
				$data = $this->get_order_details();
				$customer = get_userdata($data['_customer_user'][0]);
				if($data):
					$this->ID 						= $data['ID'];
					$this->order_date 				= $data['post_date'];
					$this->billing_first_name 		= isset($data['_billing_first_name'][0]) ? ($data['_billing_first_name'][0]) : '';
					$this->billing_last_name 		= isset($data['_billing_last_name'][0]) ? ($data['_billing_last_name'][0]) : '';
					$this->billing_company 			= isset($data['_billing_company'][0]) ? ($data['_billing_company'][0]) : '';
					$this->billing_address_1 		= isset($data['_billing_address_1'][0]) ? ($data['_billing_address_1'][0]) : '';
					$this->billing_address_2 		= isset($data['_billing_address_2'][0]) ? ($data['_billing_address_2'][0]) : '';
					$this->billing_city 			= isset($data['_billing_city'][0]) ? ($data['_billing_city'][0]) : '';
					$this->billing_postcode 		= isset($data['_billing_postcode'][0]) ? ($data['_billing_postcode'][0]) : '';
					$this->billing_country 			= isset($data['_billing_country'][0]) ? ($data['_billing_country'][0]) : '';
					$this->billing_state 			= isset($data['_billing_state'][0]) ? ($data['_billing_state'][0]) : '';
					$this->billing_email 			= isset($data['_billing_email'][0]) ? ($data['_billing_email'][0]) : '';
					$this->billing_phone			= isset($data['_billing_phone'][0]) ? ($data['_billing_phone'][0]) : '';
					$this->shipping_first_name 		= isset($data['_shipping_first_name'][0]) ? ($data['_shipping_first_name'][0]) : '';
					$this->shipping_last_name 		= isset($data['_shipping_last_name'][0]) ? ($data['_shipping_last_name'][0]) : '';
					$this->shipping_company 		= isset($data['_shipping_company'][0]) ? ($data['_shipping_company'][0]) : '';
					$this->shipping_address_1 		= isset($data['_shipping_address_1'][0]) ? ($data['_shipping_address_1'][0]) : '';
					$this->shipping_address_2 		= isset($data['_shipping_address_2'][0]) ? ($data['_shipping_address_2'][0]) : '';
					$this->shipping_city 			= isset($data['_shipping_city'][0]) ? ($data['_shipping_city'][0]) : '';
					$this->shipping_postcode 		= isset($data['_shipping_postcode'][0]) ? ($data['_shipping_postcode'][0]) : '';
					$this->shipping_country 		= isset($data['_shipping_country'][0]) ? ($data['_shipping_country'][0]) : '';
					$this->shipping_state 			= isset($data['_shipping_state'][0]) ? ($data['_shipping_state'][0]) : '';
					$this->shipping_method 			= isset($data['_shipping_method'][0]) ? ($data['_shipping_method'][0]) : '';
					$this->shipping_method_title 	= isset($data['_shipping_method_title'][0]) ? ($data['_shipping_method_title'][0]) : '';
					$this->payment_method 			= isset($data['_payment_method'][0]) ? ($data['_payment_method'][0]) : '';
					$this->payment_method_title 	= isset($data['_payment_method_title'][0]) ? ($data['_payment_method_title'][0]) : '';
					$this->order_discount 			= isset($data['_order_discount'][0]) ? $data['_order_discount'][0] : '';
					$this->cart_discount 			= isset($data['_cart_discount'][0]) ? $data['_cart_discount'][0] : '';
					$this->order_tax 				= isset($data['_order_tax'][0]) ? $data['_order_tax'][0] : '';
					$this->order_shipping 			= isset($data['_order_shipping'][0]) ? $data['_order_shipping'][0] : '';
					$this->order_shipping_tax 		= isset($data['_order_shipping_tax'][0]) ? $data['_order_shipping_tax'][0] : '';
					$this->order_total 				= isset($data['_order_total'][0]) ? $data['_order_total'][0] : '';
					$this->items 					= isset($data['_order_items'][0]) ? maybe_unserialize($data['_order_items'][0]) : array();
					$this->taxes 					= isset($data['_order_taxes'][0]) ? maybe_unserialize($data['_order_taxes'][0]) : array();
					$this->customer_user 			= $data['_customer_user'][0];
					$this->customer_login 			= isset($customer->user_login) ? $customer->user_login : '';
					$this->customerListID			= isset($data['_customerListID'][0]) ? $data['_customerListID'][0] : '';
					$this->quickbooks 				= array_key_exists('_quickbooks_data',$data) && $data['_quickbooks_data'] !=''?unserialize($data['_quickbooks_data'][0]):false;
					$this->use_listid 				= (isset($this->customerListID) && $this->customerListID) ? true : false; 
					$this->pending					= isset($this->settings->pending) ? $this->settings->pending : '';
					do_action('sod_qb_load_order', $this, $data);
					endif;
				break;
		case "item":
				$data = $this->get_item_details();
				if($data):
					
					$sku = array_key_exists('_sku',$data) && $data['_sku'][0] !="" ? $data['_sku'][0]:$this->ID;
					$this->ID 						= $data['ID'];
					$this->name						= $this->inventory_settings->product_identifier =="ID" ? $data['ID'] : str_replace('‐','-', $sku);
					$this->sales_desc				= str_replace('‐','-', $data['post_title']);
					$this->status					= $data['post_status'];
					$this->item_parent				= $data['post_parent'];
					$this->downloadable 			= $data['_downloadable'][0]=="yes"?true:false;
					$this->virtual 					= $data['_virtual'][0]=="yes"?true:false;
					$this->sku 						= str_replace('‐','-', $sku);
					$this->price 					= (int)$data['_price'][0] > 0 ? $data['_price'][0] : 0;
					$this->stock					= array_key_exists('_stock',$data) && $data['_stock'][0] >=0 ? $data['_stock'][0] : "no" ;
					$this->manage_stock				= $data['_manage_stock'][0]=="yes"?true:false;
					$this->stock_status				= $data['_stock'][0] >0 ? 'instock' :'outofstock';
					$this->sale_price				= $data['_sale_price'][0];
					$this->regular_price			= (int)$data['_regular_price'][0] > 0 ? $data['_regular_price'][0] : 0; 
					$this->tax_status				= $data['_tax_status'][0];
					$this->tax_class 				= $data['_tax_class'][0];
					$this->product_type				= $data['post_type'];
					$this->sync_status				= $data['_sync_status'][0];
					$this->quickbooks				= array_key_exists('_quickbooks_data',$data) && $data['_quickbooks_data'] !=''?unserialize($data['_quickbooks_data'][0]):false;
					$this->has_parent 				= $data['post_parent'] > 0 ? true: false;
					
					$this->has_children				= count($this->get_product_variants()) >0 ? true:false;
					if($this->has_parent):
						$variation 					= new WC_Product_Variation($data['ID']);
						$this->variation_data 		= $variation->get_variation_attributes();
					else:
						$this->variation_data		= false;
					endif;  
					$this->children_with_stock		= $this->check_variants_stock() == true ? true:false;
					$this->children_with_stock_levels	= $this->check_variants_stock_levels() == true ? true:false;
					do_action('sod_qb_load_item', $this, $data);
				endif; 
				break;
		}
	}
	
	function get_order_details(){
		
		if(get_post($this->ID, ARRAY_A) && get_post_custom($this->ID)):
			
			$item_data = array_merge(get_post($this->ID, ARRAY_A), get_post_custom($this->ID));
			
		endif;
		
		$item_data = apply_filters('sod_qbpos_get_order_details', $item_data);
		
		return $item_data;
	}
	function get_item_details(){
		
		if(get_post($this->ID, ARRAY_A) && get_post_custom($this->ID)):
			
			$item_data = array_merge(get_post($this->ID, ARRAY_A), get_post_custom($this->ID));
			
		endif;
		
		$item_data = apply_filters('sod_qbpos_get_item_details', $item_data);
		
		return $item_data;
	}
	function get_product_variants(){
		
		$args = array('post_parent'=>$this->ID,'post_type'=>'product_variation');
		
		return get_children($args);	
	}
	function check_variants_stock(){
		$products_with_stock = array();
		
		$products = $this->get_product_variants();
		
		if($products and count($products)>0):
			
			foreach($products as $product){
				
				$var = new SOD_Quickbooks_Data;
				
				$var->ID = $product->ID;
				
				$var->load("item");
				
				if($var->stock != "no"):
					
					$products_with_stock[]=$var->name;
					
				endif;
			}
		endif;
		
		if(count($products_with_stock)>0):
			
			return true;
			
		else:
			
			return false;
			
		endif;
	}
	function check_variants_stock_levels(){
		
		$products_with_stock = array();
		
		$status = false;
		
		$products = $this->get_product_variants();
		
		if($products and sizeof($products)>0):
			
			foreach($products as $product){
				
				$var = new SOD_Quickbooks_Data;
				
				$var->ID = $product->ID;
				
				$var->load("item");
				
				if($var->stock != "no" && (int)$var->stock > 0):
				
					$products_with_stock[]=$var->name;
				
				endif;
			
			}
		
		endif;
		
		if(sizeof($products_with_stock) > 0):
		
			return true;
		
		else:
		
			return false;
		
		endif;
		
		return $status;
	}
	/*
	$meta_key    - What meta key to check against (required)
	$post_type   - What post type to check (default - post)
	$post_status - What post status to check (default - publish)
	$fields      - Whether to query all the post table columns, or just a select one ... all, titles, ids, or guids (all returns an array of objects, others return an array of values)
	*/
	function posts_without_meta( $meta_key = '', $post_type = 'post', $post_status = 'publish', $fields = 'all' ) {
		global $wpdb;
		if( !isset( $meta_key ) || !isset( $post_type ) || !isset( $post_status ) || !isset( $fields ) )
			return false;
		// Meta key is required
		if( empty( $meta_key ) )
			return false;
		// All parameters are expected to be strings
		if( !is_string( $meta_key ) || !is_string( $post_type ) || !is_string( $post_status ) || !is_string( $fields ) )
			return false;
		if( empty( $post_type ) )
			$post_type = 'post';
		if( empty( $post_status ) )
			$post_status = 'publish';
		if( empty( $fields ) )
			$fields = 'all';
		// Since all parameters are strings, bind them into one for a cheaper preg match (rather then doing one for each)
		$possibly_unsafe_text = $meta_key . $post_type . $post_status . $fields;
		// Simply die if anything not a letter, number, underscore or hyphen is present
		if( preg_match( '/([^a-zA-Z0-9_-]+)/', $possibly_unsafe_text ) ) {
			wp_die( 'Invalid characters present in call to function (valid chars are a-z, 0-9, A-Z, underscores and hyphens).' );
			exit;
		}
		switch( $fields ) :
			case 'ids':
				$cols = 'p.ID';
				break;
			case 'titles':
				$cols = 'p.post_title';
				break;
			case 'guids':
				$cols = 'p.guid';
				break;
			case 'all':
			default:
				$cols = 'p.*';
				break;
		endswitch;
		if( 'all' == $fields )
			$result = $wpdb->get_results( $wpdb->prepare( "
				SELECT $cols FROM {$wpdb->posts} p
				WHERE NOT EXISTS
				(
					SELECT pm.* FROM {$wpdb->postmeta} pm
					WHERE p.ID = pm.post_id
					AND pm.meta_key = '%s'
				)
				AND p.post_type = '%s'
				AND p.post_status = '%s'
				", 
				$meta_key, 
				$post_type, 
				$post_status 
			) );
		// get_col is nicer for single column selection (less data to traverse)
		else 
			$result = $wpdb->get_col( $wpdb->prepare( "
				SELECT $cols FROM {$wpdb->posts} p
				WHERE NOT EXISTS
				(
					SELECT pm.* FROM {$wpdb->postmeta} pm
					WHERE p.ID = pm.post_id
					AND pm.meta_key = '%s'
				)
				AND p.post_type = '%s'
				AND p.post_status = '%s'
				", 
				$meta_key, 
				$post_type, 
				$post_status 
			) );
		return $result;
	}
	function kill_transients($post_id){
		global $wpdb;
		delete_transient('woocommerce_products_onsale');
		if ($post_id>0) :
			$post_id = (int) $post_id;
			delete_transient('woocommerce_product_total_stock_'.$post_id);
		else :
			$wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_woocommerce_product_total_stock_%')");
		endif;
	}
	public function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }
 	public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
    public function encode($value){ 
 	    if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext)); 
    }
    public function decode($value){
        if(!$value){return false;}
        $crypttext = $this->safe_b64decode($value); 
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }
	public static function get_tax_class($tax_class){
		$quickbooks = new SOD_Quickbooks_Data;
		//$tax_class = $item['_tax_class'];
		//$tax_status = $item['_tax_status'];
		if(empty($tax_class)){
			$tax_class 			= "standard";
			$new_tax_class 		= str_replace("-","_",$tax_class);
			$tax_codes_array 	= $quickbooks->settings->taxcodes_mappings; 
			$taxid 				= $tax_codes_array[$new_tax_class];
		}else{
			$new_tax_class 		= str_replace("-","_",$tax_class);
			$tax_codes_array 	= $quickbooks->settings->taxcodes_mappings;
			$taxid 				= $tax_codes_array[$new_tax_class];
		}
		return $taxid;
	}
	function get_name(){
		$array  = $this->settings->customer_identifier;
		$first 	= apply_filters('sod_qb_customer_name_first', 	substr($array['first'],1) );
		$second = apply_filters('sod_qb_customer_name_second', 	substr($array['second'],1) );
		$third 	= apply_filters('sod_qb_customer_name_third', 	substr($array['third'],1) );
		if(isset($first)):
			if(isset($this->$first) && $this->$first !=""):
				$first = $this->$first;
			else:
				$first = str_replace('billing', 'shipping', $first);
				if(isset($this->$first)):
					$first = $this->$first;
				endif;
			endif;
		else:
			$first = '';
		endif;
		if(isset($second)):
			if(isset($this->$second) && $this->$second !=""):
				$second = $this->$second;
			else:
				$second = str_replace('billing', 'shipping', $second);
				if(isset($this->$second)):
					$second = $this->$second;
				endif;
			endif;
		else:
			$second = '';
		endif;
		if(isset($third)):
			if(isset($this->$third) && $this->$third !=""):
				$third = $this->$third;
			else:
				$third = str_replace('billing', 'shipping', $third);
				if(isset($this->$third)):
					$third = $this->$third;
				endif;
			endif;
		else:
			$third = '';
		endif;
		$customer = $first . " " . $second . " " . $third;
		return $customer;
	}
	function get_customer_info($quickbooks,$order){
		if(!$quickbooks->settings->create_customer_account){
			$cust_ident = '<ListID>'.$quickbooks->settings->customer.'</ListID>';
		}else{
			if($quickbooks->settings->create_customer_account=="on"){
				if(!$order->_customerListID){
					
					$cust_ident = '<FullName>'.trim(webconnector::map_cust_identifier($order->ID)).'</FullName>';
				}else{
					$cust_ident = '<ListID>'.$order->_customerListID.'</ListID>';
				}
			}elseif($quickbooks->settings->create_customer_account=="off" || $quickbooks->settings->create_customer_account==""){
				$cust_ident = '<ListID>'.$quickbooks->settings->customer.'</ListID>';
			}
		}
		return $cust_ident;		
	}
	function get_item_info($item){
		$quickbooks = new SOD_Quickbooks_Data;
		$attributes = false;
		/*
		 * This is for variations
		 */
		if(isset($item['variation_id']) && $item['variation_id']!=="" && $item['variation_id']!=="0"){
				
			$meta = get_post_custom($item['variation_id']);
				
			if($quickbooks->inventory_settings->track_variations == "on"):
				
				$qb_data = get_post_meta($item['variation_id'],'_quickbooks_data',true);
			
			else:
			
				$qb_data = get_post_meta( $item['id'] , '_quickbooks_data' , true );
			
			endif;
			//if(array_key_exists('ListID',$qb_data)){
			//	$ident ='<ListID>'. $qb_data['ListID']. '</ListID>';
			//}else{
				$identifier = $quickbooks->inventory_settings->product_identifier;
				switch($identifier){
					case "sku":
						$qb_identifier = isset($qb_data['FullName']) ? $qb_data['FullName'] : $meta['_sku'][0]; 
						if(isset($meta['_sku'])){
							$ident ='<FullName>'.$qb_identifier.'</FullName>';
						}
						break;
					case "ID":
						$qb_identifier = isset($qb_data['FullName']) ? $qb_data['FullName'] : $item['variation_id'][0];
						$ident ='<FullName>'. $qb_identifier .'</FullName>';
						break;
						
				}
			//}
			$variation 		= new SOD_Quickbooks_Data;
			$variation->ID  = $item['variation_id'];
			$variation->load('item');
			$variation_data = isset($item['item_meta']) ? $item['item_meta'] : array();
			
			if($variation_data):
				foreach($variation_data as $key=>$value){
					$attribute_name = isset($value['meta_name']) ? ucwords(str_replace("pa_", "", $value['meta_name'])): "";
					$attribute_value = isset($value['meta_value']) ? ucwords($value['meta_value']) : "";
					$attributes[] .= $attribute_name .": " . $attribute_value;
				}
			endif;
			
				if(isset($attributes)):
					if($quickbooks->settings->use_attributes_as_desc=="on"):
						$attributes ="<Desc>" . implode(",",$attributes) . '</Desc>';
					else:
						$attributes = "";
					endif;
				endif;
			
			$tax = $this->get_tax_class($meta['_tax_class'][0]);
		}else{
			/*
			 * This is for simple products
			 */
			$qb_data 		= maybe_unserialize(get_post_meta($item['id'],'_qb_listid', true));
			$qb_full_data 	= get_post_meta($item['id'],'_quickbooks_data',true);
			$meta 	 		= get_post_custom($item['id']);
			if(is_array($qb_data)){
				if(isset($qb_data['ListID'])){
					$ident ='<ListID>'. $qb_data. '</ListID>';
				}else{
					$identifier = $quickbooks->inventory_settings->product_identifier;
					switch($identifier){
						case "sku":
							$qb_identifier = isset($qb_full_data['FullName']) ? $qb_full_data['FullName'] : $meta['_sku'][0];
							if(isset($meta['_sku'])){
								$ident ='<FullName>'.$qb_identifier.'</FullName>';
							}
							break;
						case "ID":
							$qb_identifier = isset($qb_full_data['FullName']) ? $qb_full_data['FullName'] : $item['id'][0];
							$ident ='<FullName>'.$qb_identifier.'</FullName>';
							break;
					}	
				}
			}else{
				$identifier = $quickbooks->inventory_settings->product_identifier;
				switch($identifier){
					case "sku":
						$qb_identifier = isset($qb_full_data['FullName']) ? $qb_full_data['FullName'] : $meta['_sku'][0];
						if(isset($meta['_sku'])){
							$ident ='<FullName>'.$qb_identifier.'</FullName>';
						}
						break;
					case "ID":
						$qb_identifier = isset($qb_full_data['FullName']) ? $qb_full_data['FullName'] : $item['id'][0];
						$ident ='<FullName>'.$qb_identifier.'</FullName>';
						break;
				}
			}
			$tax = $this->get_tax_class($meta['_tax_class'][0]);
		}
		
		$tax_line="";
		if(!empty($tax) && $tax !==""){
			$tax_line ='<SalesTaxCodeRef><ListID>'.$tax.'</ListID></SalesTaxCodeRef>';
		}else{
			$tax_line="<SalesTaxCodeRef><FullName>NON</FullName></SalesTaxCodeRef>";
		}
		switch($this->settings->post_order_type){
			case "sales_receipt":
				$line_item = '<SalesReceiptLineAdd><ItemRef>'.$ident.'</ItemRef>' . $attributes . '<Quantity>'.$item['qty'].'</Quantity><Amount>'.number_format($item['line_subtotal'],2,'.','').'</Amount>'.$tax_line.'</SalesReceiptLineAdd>';//.$tax_line;
				break;
			case "sales_order":
				$line_item = '<SalesOrderLineAdd><ItemRef>'.$ident.'</ItemRef>' . $attributes . '<Quantity>'.$item['qty'].'</Quantity><Amount>'.number_format($item['line_subtotal'],2,'.','').'</Amount>'.$tax_line.'</SalesOrderLineAdd>';
				break;
		}
		$line_item = apply_filters('quickbooks_webconnector_sales_order_line_items', $line_item, $qb_data, $ident, $item, $this->settings->post_order_type);
	return $line_item;
	}
	function get_sales_tax_code($ListID){
		$items = get_option('_sod_quickbooks_all_items');
		$tax_code ='';
		foreach($items as $key=>$value){
			if(is_array($value)){
				if($value['ListID'] ==$ListID){
					$tax_code = $value['SalesTaxCodeRef'];
				}
			}
		}
		return $tax_code;
	}
	function get_sales_tax_listid($order_id){
		$tax_rates = array(
			'global'=>array(),
			'local'=>array()
		);
		$tax_rates_array = $this->settings->taxrates;
		$local_tax_rates_array = $this->settings->taxrates_local;
		
		if($this->taxes){
			foreach($this->taxes as $tax_rate){
				if((float)$tax_rate['cart_tax'] > 0):
					$label = $this->format_tax_rate($tax_rate['label']);
					
					if(isset($tax_rates_array[$label])):
						$tax_rates['global'][] = $tax_rates_array[$label];
					elseif(isset($local_tax_rates_array[$label])):
						$tax_rates['local'][] = $local_tax_rates_array[$label];
					endif;
				endif;
			}
		}
		// }else{
			// $tax_rates['global'][] = $this->settings->no_tax;
		// }
		return $tax_rates; 	
	}
	function get_local_sales_tax_listid($order_id){
		$quickbooks = new SOD_Quickbooks_Data;
		$order_sales_tax = get_post_meta($order_id,'_order_taxes',true);
		$tax_rates = array();
		if($order_sales_tax){
			foreach($order_sales_tax as $tax_rate){
				if((float)$tax_rate['cart_tax'] > 0):
					$label = $quickbooks->format_tax_rate($tax_rate['label']);
					$local_tax_rates_array = $this->settings->taxrates_local;
					$tax_rates[] = $local_tax_rates_array[$label];
				endif;
			}
			return $tax_rates; 	
		}else{
			return false;
		}
	}
	function sod_tax_row_label( $selected ) {
		global $woocommerce;
		$return = '';
		// Get counts/countries
		$counties_array = array();
		$states_count = 0;
		if ($selected) foreach ($selected as $country => $value) :
			$country = woocommerce_clean($country);
			if (sizeof($value)>0 && $value[0]!=='*') :
				$states_count+=sizeof($value);
			endif;
			if (!in_array($country, $counties_array)) $counties_array[] = $woocommerce->countries->countries[$country];
		endforeach;
		
		$states_text = '';
		$countries_text = implode(', ', $counties_array);
		// Show label
		if (sizeof($counties_array)==0) :
			$return .= __('No countries selected', 'woocommerce');
		elseif ( sizeof($counties_array) < 6 ) :
			if ($states_count>0) $states_text = sprintf(_n('(1 state)', '(%s states)', $states_count, 'woocommerce'), $states_count);
			$return .= $countries_text . ' ' . $states_text;
		else :
			if ($states_count>0) $states_text = sprintf(_n('and 1 state', 'and %s states', $states_count, 'woocommerce'), $states_count);
			$return .= sprintf(_n('1 country', '%1$s countries', sizeof($counties_array), 'woocommerce'), sizeof($counties_array)) . ' ' . $states_text;
		endif;
		return $return;
	}
	function format_tax_rate($label){
		global $woocommerce;
		if (!$label) :
			$label = $woocommerce->countries->tax_or_vat();
		endif;
		// Add % to label
		$temp = str_replace(array('(',')','%','.'),'', $label);
		$formatted = str_replace(" ","_",strtolower($temp));
		return $formatted;
	}
	function update_item_by_id($arr, $global_stock_management){
		global $wpdb;
		$gsm 		= get_option('woocommerce_manage_stock');
	
		$quickbooks = new SOD_Quickbooks_Data();
	
		$id  	 	= $arr['Name'];//apply_filters('sod_qb_id_query_args', $arr['Name'], $arr);
	
		$product 	= get_post($id);
	
		if($product):
	
			$wc_product = new WC_Product($product->ID);
	
			if($product->post_parent):
	
				$parent_id 		= $product->post_parent;
	
				$sync_status 	= get_post_meta($parent_id, '_sync_status', true) ? get_post_meta($parent_id, '_sync_status', true) : false;
	
				$manage_stock 	= get_post_meta($parent_id, '_manage_stock', true) ? get_post_meta($parent_id, '_manage_stock', true) : false;
	
			else:
	
				$sync_status 	= get_post_meta($product->ID, '_sync_status', true) ? get_post_meta($product->ID, '_sync_status', true) : false;;
	
				$manage_stock 	= get_post_meta($product->ID, '_manage_stock', true) ? get_post_meta($product->ID, '_manage_stock', true) : false;;
	
			endif;
	
			if($sync_status == 'on'):
	
				update_post_meta($product->ID,'_quickbooks_data',$arr);
	
				update_post_meta($product->ID,'_qb_name',$arr['Name']);
				
				update_post_meta($product->ID,'_qb_fullname',$arr['FullName']);
	
				update_post_meta($product->ID,'_qb_item_type',$arr['ItemType']);
				
				if($this->inventory_settings->sync_inv=="on"):
					/*
					 * Transients - qty
					 */
					$qty = $arr['QuantityOnHand'];
	
					$transient_id = 'wc_product_total_stock_'.$product_id;
	
					delete_transient($transient_id);
	
					set_transient($transient_id,$qty);
					/*
						 * Check Stock Managements
						/*If manage_stock returns true, we don't have a variant*/
					if($gsm == "yes"):
						
						if($quickbooks->inventory_settings->change_stock_status == "on"):
							/*
							 * 1. Qty > 0, manage stock and not a variation
							 */
							if((int)$qty >0 && $manage_stock=="yes" && !$wc_product->post->post_parent):
								
									update_post_meta($product->ID,'_stock_status','instock');
							/*
							 * 2. Qty = 0, manage stock and not a variation
							 */	
							elseif((int)$qty==0 && $manage_stock=="yes" && $wc_product->get_total_stock() > 0 ):
								
									update_post_meta($product->ID,'_stock_status','instock');
							
							/*
							 * 2. Qty = 0, manage stock and not a variation
							 */	
							elseif($manage_stock == false):
								
									update_post_meta($product->ID,'_stock_status','instock');
								
							elseif((int)$qty<=0 && $wc_product->get_total_stock()<=0 && $manage_stock == "yes"):
								
									update_post_meta($product->ID,'_stock_status','outofstock');
								
							else:
								
								update_post_meta($product->ID,'_stock_status','instock');
								
							endif;
							
						endif;
					else:
						if($quickbooks->inventory_settings->change_stock_status == "on"):
							
							if( (int)$qty <=0 ):
								
								update_post_meta($product->ID,'_stock_status','outofstock');
								
							elseif((int)$qty >0 ):
								
								update_post_meta($product->ID,'_stock_status','instock');
								
							endif;
						
						endif;
						
					endif;
							
				endif;
				
				if($quickbooks->inventory_settings->sync_price=="on"):
					update_post_meta($product->ID,'_regular_price',$arr['SalesPrice']);
					update_post_meta($product->ID,'_price',$arr['SalesPrice']);
					if($wc_product->is_type('variable')):
				
						$wc_product->variable_product_sync();
				
					endif;
					
				endif;
				
				delete_transient('wc_products_onsale');
				
				delete_transient('wc_hidden_product_ids');
				
				delete_transient('wc_hidden_from_search_product_ids');
				
				$wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_woocommerce_unfiltered_product_ids_%')");
				
				$wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_woocommerce_layered_nav_count_%')");
				
				delete_transient('wc_product_total_stock_'.$product->ID);
				
				delete_transient('wc_product_children_ids_'.$product->ID);
				
				update_post_meta($product->ID,'_stock',$qty);
			
			endif;
			
			do_action('sod_qb_inv_sync_after', $product);	
		
		else:
		
			do_action('sod_qb_product_not_found',$arr);
		
		endif;
	
	}
	function update_item_with_pos_data($ID, $arr){
		$sync 						= get_post_meta($ID, '_sync_status', true);
		
		$global_stock_management 	= get_option('woocommerce_manage_stock');
		
		update_post_meta($ID,'_quickbooks_data',$arr);
		
		update_post_meta($ID,'_qb_item_number',$arr['ItemNumber']);
		
		update_post_meta($ID,'_qb_item_type',$arr['ItemType']);
		
		update_post_meta($ID,'_qb_listid',$arr['ListID']);
		
		if($sync=='on'):
			
			if($this->inventory_settings->sync_inv=="on"):
			
				$id = $item->ID;
				
				$qty = $arr['QuantityOnHand'];
				
				$transient_id = 'wc_product_total_stock_'.$id;
				
				delete_transient($transient_id);
				
				set_transient($transient_id,$qty);
				
				update_post_meta($item->ID,'_stock',$qty);
				
				/*If manage_stock returns true, we don't have a variant*/
				if($global_stock_management == "yes"):
					
					if($quickbooks->inventory_settings->change_stock_status == "on"):
						
						if((int)$arr['QuantityOnHand'] >0 && $item->manage_stock):
							
								update_post_meta($item->ID,'_stock_status','instock');
							
						elseif((int)$arr['QuantityOnHand'] ==0 && $item->children_with_stock_levels==true):
							
							update_post_meta($item->ID,'_stock_status','instock');
						
						elseif(!$item->manage_stock && $item->children_with_stock_levels==true):
						
							update_post_meta($item->ID,'_stock_status','instock');
						
						elseif((int)$arr['QuantityOnHand'] <=0 && $item->children_with_stock_levels==false):
						
							update_post_meta($item->ID,'_stock_status','outofstock');
						
						else:
							
							update_post_meta($item->ID,'_stock_status','instock');
						
						endif;
					
					endif;
				else:
					if($quickbooks->inventory_settings->change_stock_status == "on"):
						
						if( (int)$arr['QuantityOnHand'] <=0 ):
						
							update_post_meta($item->ID,'_stock_status','outofstock');
						
						elseif((int)$arr['QuantityOnHand'] >0 ):
						
							update_post_meta($item->ID,'_stock_status','instock');
						
						endif;
						
					endif;
					
				endif;
						
			endif;
			if($quickbooks->inventory_settings->sync_price=="on"):
				
				update_post_meta($item->ID,'_regular_price',$arr['SalesPrice']);
				update_post_meta($item->ID,'_price',$arr['SalesPrice']);
				
				$woocommerce = new WC_Product($product->ID);
				
				if($woocommerce->is_type('variable')):
					
					$woocommerce->variable_product_sync();
					
				endif;
				
				global $wpdb;
				
				delete_transient('woocommerce_products_onsale');
				
				delete_transient('woocommerce_hidden_product_ids');
				
				delete_transient('woocommerce_hidden_from_search_product_ids');
				
				$wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_woocommerce_unfiltered_product_ids_%')");
				
				$wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_woocommerce_layered_nav_count_%')");
				
				if ($product->ID>0) :
					
					$product->ID = (int) $product->ID;
					
					delete_transient('woocommerce_product_total_stock_'.$product->ID);
					
					delete_transient('woocommerce_product_children_ids_'.$product->ID);
					
				endif;
				
			endif;
			
		endif;
	}
	function update_item_by_sku($arr, $global_stock_management){
		global $wpdb;
		
		$gsm 		= get_option('woocommerce_manage_stock');
		
		$quickbooks = new SOD_Quickbooks_Data();
		
		$args = array( 
			'post_type'=> array(
				'product',
				'product_variation',
			),
			'meta_query' => array(
				array(
					'key' => '_'.$quickbooks->inventory_settings->product_identifier,
					'value' => $arr['Name'],
				)
			)
		);
		//$args = apply_filters('sod_qb_sku_query_args', $args, $arr);
		
		$query = new WP_Query();
		
		$query->query($args);
		
		$products = $query->posts;
		
		if($products):
		
			foreach($products as $product){
				
				$wc_product = new WC_Product($product->ID);
				
				if($product->post_parent > 0 ):
					
					$parent_id 		= $product->post_parent;
					
					$sync_status 	= get_post_meta($parent_id, '_sync_status', true) ? get_post_meta($parent_id, '_sync_status', true) : false;
					
					$manage_stock 	= get_post_meta($parent_id, '_manage_stock', true) ? get_post_meta($parent_id, '_manage_stock', true) : false;
					
				else:
					$sync_status 	= get_post_meta($product->ID, '_sync_status', true) ? get_post_meta($product->ID, '_sync_status', true) : false;;
					
					$manage_stock 	= get_post_meta($product->ID, '_manage_stock', true) ? get_post_meta($product->ID, '_manage_stock', true) : false;;
					
				endif;
				
				if($sync_status == 'on'):
					
					update_post_meta($product->ID,'_quickbooks_data',$arr);
					
					update_post_meta($product->ID,'_qb_listid',$arr['ListID']);
					
					update_post_meta($product->ID,'_qb_fullname',$arr['FullName']);
					
					update_post_meta($product->ID,'_qb_item_type',$arr['ItemType']);
					
					if($this->inventory_settings->sync_inv=="on"):
						/*
						 * Transients - qty
						 */
						$qty = $arr['QuantityOnHand'];
						
						$transient_id = 'wc_product_total_stock_'.$product->ID;
						
						delete_transient($transient_id);
						
						set_transient($transient_id,$qty);
						
						update_post_meta($product->ID,'_stock',$qty);
							/*
							 * Check Stock Managements
							/*If manage_stock returns true, we don't have a variant*/
						if($gsm == "yes"):
							
							if($quickbooks->inventory_settings->change_stock_status == "on"):
								/*
								 * 1. Qty > 0, manage stock and not a variation
								 */
								if((int)$qty >0 && $manage_stock=="yes" && !$wc_product->post->post_parent):
									
										update_post_meta($product->ID,'_stock_status','instock');
								/*
								 * 2. Qty = 0, manage stock and not a variation
								 */	
								elseif((int)$qty==0 && $manage_stock=="yes" && $wc_product->get_total_stock() > 0 ):
									
										update_post_meta($product->ID,'_stock_status','instock');
								
								/*
								 * 2. Qty = 0, manage stock and not a variation
								 */	
								elseif($manage_stock == false):
									
										update_post_meta($product->ID,'_stock_status','instock');
									
								elseif((int)$qty<=0 && $wc_product->get_total_stock()<=0 && $manage_stock == "yes"):
									
										update_post_meta($product->ID,'_stock_status','outofstock');
									
								else:
									
									update_post_meta($product->ID,'_stock_status','instock');
									
								endif;
								
							endif;
							
						else:
							if($quickbooks->inventory_settings->change_stock_status == "on"):
								
								if( (int)$qty <=0 ):
									
									update_post_meta($product->ID,'_stock_status','outofstock');
									
								elseif((int)$qty >0 ):
									
									update_post_meta($product->ID,'_stock_status','instock');
									
								endif;
								
							endif;
							
						endif;
								
					endif;
					if($quickbooks->inventory_settings->sync_price=="on"):
						
						//$quickbooks->update_price($product,$arr);
						update_post_meta($product->ID,'_regular_price', $arr['SalesPrice']);
						update_post_meta($product->ID,'_price', $arr['SalesPrice']);
						if($wc_product->is_type('variable')):
							
							$wc_product->variable_product_sync();
							
						endif;
					endif;
					
					delete_transient('wc_products_onsale');
					
					delete_transient('wc_hidden_product_ids');
					
					delete_transient('wc_hidden_from_search_product_ids');
					
					$wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_woocommerce_unfiltered_product_ids_%')");
					
					$wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_woocommerce_layered_nav_count_%')");
					
					delete_transient('wc_product_total_stock_'.$product->ID);
					
					delete_transient('wc_product_children_ids_'.$product->ID);
					
					
					
				endif;
			}

			do_action('sod_qb_inv_sync_after', $product);
			
		else:
			
			do_action('sod_qb_product_not_found',$arr);
			
		endif;
	}
}

add_action( 'admin_init', 'sod_init_quickbooks' );
function sod_init_quickbooks(){
	global $quickbooks;
	$quickbooks = new SOD_Quickbooks_Data();	
	$quickbooks->dsn = 'mysql://'.DB_USER.':'.DB_PASSWORD.'@'.DB_HOST.'/'.DB_NAME;
}
		
	
