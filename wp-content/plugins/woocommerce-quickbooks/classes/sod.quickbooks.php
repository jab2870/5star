<?php
	abstract class SOD_Quickbooks{
		var $dsn;
		var $ID; 						
		var $order_date; 		
		var $billing_first_name;
		var $billing_last_name; 
		var $billing_company;	
		var $billing_address_1;
		var $billing_address_2;
		var $billing_city; 	
		var $billing_postcode; 
		var $billing_country; 	
		var $billing_state; 	
		var $billing_email; 	
		var $billing_phone;	
		var $shipping_first_name; 
		var $shipping_last_name; 	
		var $shipping_company; 	
		var $shipping_address_1; 	
		var $shipping_address_2; 	
		var $shipping_city; 		
		var $shipping_postcode; 	
		var $shipping_country; 	
		var $shipping_state; 		
		var $shipping_method; 		
		var $shipping_method_title;
		var $payment_method; 		
		var $payment_method_title; 
		var $order_discount; 		
		var $cart_discount; 		
		var $order_tax; 			
		var $order_shipping; 		
		var $order_shipping_tax; 	
		var $order_total; 			
		var $items; 				
		var $taxes; 				
		var $customer_user; 		
		var $customerListID;		
		var $quickbooks; 			
		var $use_listid; 			 
		var $pending;				
		function __construct(){
			//$this->dsn = 'mysql://'.DB_USER.':'.DB_PASSWORD.'@'.DB_HOST.'/'.DB_NAME;
		}
		//abstract function update_quickbooks_response_info($arr);
		abstract function check_defaults();
		abstract function get_accounts($type);
		abstract function set_accounts($type, $arr);
		abstract function set_preferences($arr);
		abstract function update_customer_listid($value);
		abstract function update_price($product, $price);
		abstract function update_stock($qty);
		abstract function get_cart_tax_classes();
		abstract function get_cart_tax_rates();
		abstract function inventory_sync_response($arr);
		
		abstract function inventory_check_for_new();
		
		abstract function load($object);
		abstract function get_order_details();
		abstract function get_item_details();
		abstract function get_name();
		abstract function get_item_info($item);
		abstract function get_sales_tax_code($ListID);
		abstract function get_sales_tax_listid($order_id);
	}
