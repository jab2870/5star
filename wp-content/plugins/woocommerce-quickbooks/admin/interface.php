<?php
/*
 * Queue Admin JS/CSS 
 */
//Initialize Admin JS, CSS
add_action('admin_enqueue_scripts', 'sod_quickbooks_admin_js');
function sod_quickbooks_admin_js(){
	wp_enqueue_script( 'quickbooks_jquery', plugins_url('/js/admin.js', __FILE__) );
	wp_enqueue_script( 'jquery_chosen', plugins_url('/js/jquery.chosen.js', __FILE__) );
	$connected = get_option('quickbooks_connected');
	$generated = $connected['AutoLogin']=="true" ? get_option('webconnector_generated'):'1';
	$site_data = array(
		'siteurl'=>get_site_url(),
		'requeue_nonce'=>wp_create_nonce('requeue_nonce'),
		'refresh_nonce'=>wp_create_nonce('refresh_nonce'),
		'sync_item_nonce'=>wp_create_nonce('sync_item_nonce'),
		'create_qwc_nonce'=>wp_create_nonce('create_qwc_nonce'),
		'check_qwc_nonce'=>wp_create_nonce('check_qwc_nonce'),
		'refresh_account_nonce'=>wp_create_nonce('refresh_account_nonce'),
		'wc_generated'=>$generated//get_option('webconnector_generated')
	);
	wp_localize_script( 'quickbooks_jquery', 'site', $site_data);
	
	//wp_register_style( 'qbconnector', plugins_url('/admin/css/admin.css', __FILE__) );
	wp_enqueue_style( 'qbconnector',plugins_url('/css/admin.css', __FILE__) );
	wp_enqueue_style( 'chosen',plugins_url('/css/chosen.css', __FILE__) );
}
/**
 * Admin Notices
 */
add_action( "admin_print_styles", 'quickbooks_admin_notices_styles' );
function quickbooks_admin_install_notice() {
	?>
	<div id="message" class="updated quickbooks-message wc-connect">
		<div class="squeezer">
			<h4><?php _e( '<strong>Take control of your order flow!</strong> &#8211; Create a QWC file and get connected', 'woocommerce' ); ?></h4>
			<p class="submit"><a href="<?php echo add_query_arg('tab', 'sod_web_qbconnector_setup', admin_url('admin.php?page=quickbooks_setup')); ?>" class="button-primary"><?php _e( 'Generate Webconnector File', 'woocommerce' ); ?></a></p>
		</div>
	</div>
	<?php
}
function quickbooks_admin_notices_styles() {
	
	// Installed notices
	if( isset($_GET['page']) && $_GET['page']=="quickbooks_setup"):
		
		if (get_option('webconnector_generated')!=1 || !get_option('quickbooks_connected')) {
			add_action( 'admin_notices', 'quickbooks_admin_install_notice' );	
		}
	endif;
}
/*
 * Add the Plugin Admin Tabs
 */
function sod_quickbooks_options_tabs() {
	$current_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'sod_qbconnector_setup';//$this->general_settings_key;
	$tabs = array(
		'sod_qbconnector_setup'=>'Quickbooks Setup',
		'sod_qbconnector_productssync'=>'Product Sync',
		'sod_web_qbconnector_setup'=>'Webconnector',
		'sod_quickbooks_status'=>'History'
		);
		screen_icon('quickbooks');
	echo '<h2 class="nav-tab-wrapper">';
	foreach ( $tabs as $tab_key => $tab_caption ) {
		$active = $current_tab == $tab_key ? 'nav-tab-active' : '';
		echo '<a class="nav-tab ' . $active . '" href="?page=quickbooks_setup&tab=' . $tab_key . '">' . $tab_caption . '</a>';	
	}
	echo '</h2>';
}
/*
 * Product Grid and Order Grids
 * 
 *
 * /*Adds QB Status Column to Orders Woocommerce Grid*/
add_filter('manage_edit-shop_order_columns', 'sod_qbconnector_edit_custom_order_columns',20);
function sod_qbconnector_edit_custom_order_columns($columns){
		//$columns = array();
	$columns["qb_status"] = __("Quickbooks Status", 'woocommerce');
	return $columns;
}
/*Adds QB Status Column to Products Woocommerce Grid*/
add_filter('manage_edit-product_columns', 'sod_qbconnector_product_edit_custom_order_columns',20);
function sod_qbconnector_product_edit_custom_order_columns($columns){
		//$columns = array();
	$columns["in_qb"] = __("Quickbooks Sync", 'woocommerce');
	return $columns;
}
/*Adds Start/Stop Sync Buttons to Woocommerce Products Grid*/
add_action('manage_product_posts_custom_column', 'sod_qbconnector_product_order_columns', 20);
function sod_qbconnector_product_order_columns($column) {
	global $post;
	$meta = get_post_meta($post->ID,'_sync_status',true);
	switch ($column) {
		case "in_qb" :
			if($meta=="on"){
				$checked='checked="checked"';
			}else{
				$checked='';
			};
			$html = '<div id="qb-status">
						<input name="product_sync" class="switch" data-id="'.$post->ID.'" id="product_sync'.$post->ID.'" '. $checked.' type="checkbox"/>
					</div>';
			echo $html;
		break;
	}
}
/*Adds Start/Stop Sync Buttons to Woocommerce Products Grid*/
add_action('manage_shop_order_posts_custom_column', 'sod_qbconnector_order_columns', 99);
function sod_qbconnector_order_columns($column) {
	global $post, $quickbooks;
	
	$order 				= new SOD_Quickbooks_Data;
	$order->ID 			= $post->ID;
	$order->load("order");
	
	$payment_id 		= isset($order->payment_method) && $order->payment_method !="" ? $quickbooks->settings->payment_mappings->{$order->payment_method} : false;
	$shipping_method 	= isset($order->shipping_method) ? $order->shipping_method : false;
	$cust_ident 		= isset($order->customerListID) ? $order->customerListID : false ;
	$listids 			= array();
	$initial_queue		= get_post_meta($order->ID, '_qb_initial_queue', true) ? get_post_meta($order->ID,'_qb_initial_queue', true) : false;
	$use_stored_xml 	= get_post_meta($order->ID, '_qb_use_stored_xml', true) == "yes" ? 'checked="checked"':"";
	$auto_post_fail		= get_post_meta($order->ID,'_qb_auto_add_failed', true) ? get_post_meta($order->ID,'_qb_auto_add_failed', true) : false;
	$error_msg			= get_post_meta($order->ID,'_qb_error_msg', true) ? get_post_meta($order->ID,'_qb_error_msg', true) : false;
	$requeued_status	= get_post_meta($order->ID,'_qb_order_requeued', true) ? get_post_meta($order->ID,'_qb_order_requeued', true) : false;
	
	switch ($column) {
		
		case "qb_status" :
			if(isset($order->items)):
				foreach($order->items as $item){
					if($item['variation_id']!=="0" && $item['variation_id']!==""):
							$qbdata = get_post_meta($item['variation_id'], '_quickbooks_data', true);
							$listids[$item['variation_id']] = $qbdata['ListID'];
					else:
						if($item['id']):
							$qbdata = get_post_meta($item['id'], '_quickbooks_data', true);
							$listids[$item['id']] = $qbdata['ListID'];
						endif;
					endif;		
				}
			endif;
			 ?>
			<ul id="validity-checks">
				<?php if($payment_id):
					$error = false;
					?>
					<li class="success payment"></li>
				<?php else:
					$error = true;
				?>
					<li class="error payment"></li>
				<?php endif; ?>
				
				<?php if($cust_ident):
					$error = false;
					?>
					<li class="success customer">
						
					</li>
				<?php else:
					$error = false;
				?>
					<li class="alert customer"></li>
				<?php endif; ?>
				<?php 
				if($order->items):
					if(count($listids) == count($order->items)):
						$error = false;
						?>
						<li class="success items"></li>
					<?php else:
						$error = true;
					?>
						<li class="error items"></li>
					<?php endif;
				else:?>
					<li class="error no-items"></li>
				 <?php 
				endif;?>
			</ul>
				
			<?php 
			echo '<ul id="quickbooks">';
			
			if($order->quickbooks && $requeued_status != "yes"):
				echo sprintf( __('<mark class="%s">%s</mark>', 'woocommerce'), sanitize_title('posted'), __('posted', 'woocommerce') );
			elseif($auto_post_fail == 'yes' && $requeued_status != "yes"):
				echo sprintf( __('<mark class="%s">%s</mark>', 'woocommerce'), sanitize_title('requeue'), __('failed', 'woocommerce') );
			elseif($initial_queue == 'yes' && $requeued_status != "yes"):
				echo sprintf( __('<mark class="%s">%s</mark>', 'woocommerce'), sanitize_title('requeued'), __('queued', 'woocommerce') );
			elseif($requeued_status == 'yes'):
				echo sprintf( __('<mark class="%s">%s</mark>', 'woocommerce'), sanitize_title('requeued'), __('requeued', 'woocommerce') );
			else:
				echo sprintf( __('<mark class="%s">%s</mark>', 'woocommerce'), sanitize_title('requeue'), __('Not Posted', 'woocommerce') );
			endif;
			echo '</ul>';
			
			if($error_msg):
				echo '<p class="">'.$error_msg.'</p>';
			endif;
			if($auto_post_fail == "yes" && $quickbooks->settings->store_xml == "on"):
				echo '<p class="">
						<label for="qb_use_stored_xml">'.__('Use stored xml?').'</label>
						<input type="checkbox" '.$use_stored_xml.' id="qb_use_stored_xml" data-id="'.$order->ID.'" name="qb_use_stored_xml" />
					</p>';
			endif;
			
			if($initial_queue != 'yes' && $requeued_status != "yes" ):
				if($order->quickbooks == false):
				echo '<p class="requeue">
							<a href="#" id="'.$order->ID.'" class="button">'.__('Requeue Order').'</a>
					</p>';
				endif;
			endif;
			break;
			
	}
}

/*
 * Product and Order Meta Boxes 
 * 
/*Adds QB Meta Box to Woocommerce Order*/
add_action( 'add_meta_boxes', 'sod_qbconnector_meta_boxes' );
function sod_qbconnector_meta_boxes() {
	add_meta_box( 'sod_quickbooks_orders', __('Quickbooks', 'woocommerce'), 'sod_qbconnector_meta_box', 'shop_order', 'side', 'default');
	//add_meta_box( 'sod_quickbooks_products', __('Quickbooks Sync', 'woocommerce'), 'sod_qbconnector_product_meta_box', 'product', 'side', 'default');
}
/*
 * Meta Box for product Edit page
 * Allows you to turn inventory syncing on/off
 */
add_action('woocommerce_product_write_panel_tabs','sod_qbconnector_product_data_tab',99);
function sod_qbconnector_product_data_tab(){?>
	<li class="quickbooks_data_tab quickbooks_options"><a href="#quickbooks_data"><?php _e('QuickBooks', 'woocommerce'); ?></a></li>
<?php }
add_action('woocommerce_product_after_variable_attributes', 'sod_qb_display_variation_qb_data', 50, 2);
function sod_qb_display_variation_qb_data($loop, $variation_data){
	
	$qb_data = maybe_unserialize($variation_data['_quickbooks_data'][0]);
	$to_display = array('ListID', 'Name', 'ItemType', 'SalesPrice', 'QuantityOnHand');
	$to_display = apply_filters('sod_qb_variation_data_to_display', $to_display);
	$disabled  	= apply_filters('sod_qb_disable_variation_fields', 'disabled="disabled"');
	$i=0;
	if(isset($qb_data)):
		foreach($qb_data as $key=>$value){
			if(in_array($key, $to_display)):
				if($i == 2 ) echo "<tr>";
			  ?> 
			 
					<td>
						<label><?php echo $key . ' '; ?><a class="tips" data-tip="<?php _e('This is a QuickBooks Field that can\'t be edited, but is used as a reference to see what QuickBooks data is stored for the item.', 'woocommerce'); ?>" href="#">[?]</a></label>
						<input style="background:#e6e6e6;" type="text" size="5" <?php echo $disabled;?> name="<?php echo $key;?>" value="<?php if (isset($value)) echo $value; ?>" />
					</td>
			<?php 
				
	  			$i++;
				if($i == 2 ) echo "</tr>";
				if($i > 2) $i = 1;
				
			endif;
		}?>
		<tr><td colspan="2"><a href="#" class="resync_product_data left button" rel="<?php echo $variation_data['variation_post_id'];?>"> <?php _e('Clear Stored QuickBooks Data and Recheck','sod_qbconnector');?></a></td></tr>
		
	<?php 
	endif;
}
add_action('woocommerce_process_product_meta','sod_qb_save_product_meta', 10, 2);
function sod_qb_save_product_meta($post_id, $post){
	$sync_status 	= get_post_meta($post_id, '_sync_status', true);
	$quickbooks 	= new SOD_Quickbooks_Data;
	$Queue 			= new QuickBooks_WebConnector_Queue($quickbooks->dsn);
	
	if($sync_status == "on"):
		
		$Queue->enqueue('QUICKBOOKS_ITEM_INVENTORY_QUERY', $post_id);
		
	endif;
	
	if(isset($_POST['qb_item_type'])):
	
		update_post_meta($post_id,'_qb_item_type', $_POST['qb_item_type']);
		
	endif;
	
	if(isset($_POST['qb_item_number'])):
	
		update_post_meta($post_id,'_qb_item_number', $_POST['qb_item_number']);
		
	endif;
}
add_action('woocommerce_product_write_panels','sod_qbconnector_product_meta_box',99);
function sod_qbconnector_product_meta_box($post) {
	global $quickbooks, $post;
	$meta 	 			= get_post_meta($post->ID,'_sync_status',true);
	$qb_data 			= maybe_unserialize(get_post_meta($post->ID,'_quickbooks_data',true));
	
	$item_types 		= array( "Inventory" => "Inventory", "NonInventory" => "Non-Inventory", "Service" => "Service");
	$item_type 			= get_post_meta($post->ID,'_qb_item_type',true);
	$qbpos_item_number 	= get_post_meta($post->ID,'_qb_fullname',true);
	$data_to_show		= array('ListID','Name','SalesPrice','QuantityOnHand','TaxCode');
	if($meta=="on"){
		$checked='checked="checked"';
	}else{
		$checked='';
	};?>
	<div id="quickbooks_data" class="panel woocommerce_options_panel">
		
		<p class="form-field">
			<label for="product_sync<?php echo $post->ID;?>"><?php _e('Turn On QuickBooks Sync', 'sod_qbconnector'); ?></label>
			<input name="product_sync" class="switch checkbox" data-id="<?php echo $post->ID;?>" id="product_sync<?php echo $post->ID;?>" <?php echo $checked;?> type="checkbox"/>
		</p>
		<p class="form-field">
			<label for="qb_item_type"><?php _e('QuickBooks Item Type','sod_qbconnector');?></label>
			<?php 
				$disabled = isset($item_type) ? 'disabled="disabled"' : "";
				$disabled = apply_filters('sod_qb_enable_item_type', $disabled);
			?>
			<select id="qb_item_type" <?php echo $disabled;?> name="qb_item_type">
				<?php foreach($item_types as $key=>$value){
					$selected = $item_type == $value ? 'selected="selected"':"";
					echo '<option '.$selected.' value="'.$value.'">'.$key.'</option>';
				};?>
			</select>
		</p>
		<p class="form-field">
			<?php 
				$disabled = isset($qbpos_item_number) ? 'disabled="disabled"' : "";
				$disabled = apply_filters('sod_qb_enable_item_number', $disabled);
			?>
			<label for="qb_item_number"><?php _e('Manually enter QuickBooks Item Name / Number to link with', 'sod_qbconnector'); ?></label>
			<input name="qb_item_number" <?php echo $disabled;?> id="qb_item_number" value='<?php echo $qbpos_item_number;?>' type="text"/>
		</p>
		<hr class="light-grey"/>
		<div>
		<h4 class="left"><?php _e('Most Recent QuickBooks Data','sod_qbconnector');?></h4>
		<a href="#" class="resync_product_data left" rel="<?php echo $post->ID;?>"> <?php _e('Clear Stored QuickBooks Data and recheck','sod_qbconnector');?></a>
		<div class="clear"></div>
		<p><?php _e('The most recent data that was synced from QuickBooks. This data is read only and can\'t be changed. If you would like to update this data, please update the data in QuickBooks and then re-sync your inventory.','sod_qbconnector');?></p>
	<?php if($qb_data && is_array($qb_data)){
		
		foreach($qb_data as $qb_key=>$qb_value){
			
			?>
			<p class="form-field">
				<label for="<?php echo $qb_key;?>"><?php echo $qb_key;?></label>
				<input name="<?php echo $qb_key;?>" class=""  id="<?php echo $qb_key;?>" type="text" readonly="readonly" value="<?php echo $qb_value;?>"/>
			</p>
			
	<?php }
	}
	?></div>
	</div>
<?php 	
}
/*
 * Order totals meta box
 * Displays the quickbooks posting details on the order view page
 */
function sod_qbconnector_meta_box(){
	global $post, $quickbooks;
	$order 				= new SOD_Quickbooks_Data;
	$order->ID 			= $post->ID;
	$order->load("order");
	
	$payment_id 		= isset($order->payment_method) && $order->payment_method !="" ? $quickbooks->settings->payment_mappings->{$order->payment_method} : false;
	$shipping_method 	= isset($order->shipping_method) ? $order->shipping_method : false;
	$cust_ident 		= isset($order->customerListID) ? $order->customerListID : false ;
	$listids 			= array();
	$use_stored_xml 	= get_post_meta($order->ID, '_qb_use_stored_xml', true) == "yes" ? 'checked="checked"':"";
	$auto_post_fail		= get_post_meta($order->ID,'_qb_auto_add_failed', true) ? get_post_meta($order->ID,'_qb_auto_add_failed', true) : false;
	$error_msg			= get_post_meta($order->ID,'_qb_error_msg', true) ? get_post_meta($order->ID,'_qb_error_msg', true) : false;
	$requeued_status	= get_post_meta($order->ID,'_qb_order_requeued', true) ? get_post_meta($order->ID,'_qb_order_requeued', true) : false;
	$initial_queue		= get_post_meta($order->ID, '_qb_initial_queue', true) ? get_post_meta($order->ID,'_qb_initial_queue', true) : false;
	
	if(isset($order->items)):
		foreach($order->items as $item){
			if($item['variation_id']!=="0" && $item['variation_id']!==""):
					$qbdata = get_post_meta($item['variation_id'], '_quickbooks_data', true);
					if(isset($qbdata['ListID'])):
						$listids[$item['variation_id']] = $qbdata['ListID'];
					endif;
			else:
				if($item['id']):
					$qbdata = get_post_meta($item['id'], '_quickbooks_data', true);
					if(isset($qbdata['ListID'])):
						$listids[$item['id']] = $qbdata['ListID'];
					endif;
				endif;
			endif;		
		}
	endif;
	
	 ?>
	<ul id="validity-checks">
		<?php if($payment_id):
			$error = false;
			?>
			<li class="success"><?php _e('Payment Method\'s successfully mapped');?></li>
		<?php else:
			$error = true;
		?>
			<li class="error"><?php _e('Payment method no mapped');?></li>
		<?php endif; ?>
		
		<?php if($cust_ident):
			$error = false;
			?>
			<li class="success"><?php _e('Customer exists in Quickbooks');?></li>
		<?php else:
			$error = false;
		?>
			<li class="alert"><?php _e('Customer will be created in Quickbooks');?></li>
		<?php endif; ?>
		<?php 
		if($order->items):
			if(count($listids) == count($order->items)):
				$error = false;
				?>
				<li class="success"><?php _e('All products on order appear to be synced with Quickbooks');?></li>
			<?php else:
				$error = true;
			?>
				<li class="error"><?php _e('Some products on the order have not been synced. The order may not post. It\'s suggested you revalidate your products from the extension\'s setup tab.');?></li>
			<?php endif;
		else:?>
			<li class="error"><?php _e('No Products on order');?></li>
		 <?php 
		endif;?>
	</ul>
		
	<?php 
	echo '<ul id="quickbooks">';
	
	if($order->quickbooks && $requeued_status != "yes"):
		echo sprintf( __('<mark class="%s">%s</mark>', 'woocommerce'), sanitize_title('posted'), __('posted', 'woocommerce') );
	elseif($auto_post_fail == 'yes' && $requeued_status != "yes"):
		echo sprintf( __('<mark class="%s">%s</mark>', 'woocommerce'), sanitize_title('requeue'), __('failed', 'woocommerce') );
	elseif($requeued_status == 'yes' && $initial_queue != "yes"):
	
		echo sprintf( __('<mark class="%s">%s</mark>', 'woocommerce'), sanitize_title('requeued'), __('requeued', 'woocommerce') );
	elseif($requeued_status != 'yes' && $initial_queue == "yes"):
		echo sprintf( __('<mark class="%s">%s</mark>', 'woocommerce'), sanitize_title('requeued'), __('Queued', 'woocommerce') );
	else:
		echo sprintf( __('<mark class="%s">%s</mark>', 'woocommerce'), sanitize_title('requeue'), __('Not Posted', 'woocommerce') );
	endif;
	echo '</ul>';
	
	if($error_msg):
		echo '<p class="">'.$error_msg.'</p>';
	endif;
	if($quickbooks->settings->post_orders =='on'):
		if($auto_post_fail == "yes" && $quickbooks->settings->store_xml == "on"):
			echo '<p class="">
				<label for="qb_use_stored_xml">'.__('Use stored xml?').'</label>
				<input type="checkbox" '.$use_stored_xml.' id="qb_use_stored_xml" data-id="'.$order->ID.'" name="qb_use_stored_xml" />
				</p>';
		endif;
	
		//if($initial_queue != 'yes' && $requeued_status != "yes" ):
			if($order->quickbooks == false):
			echo '<p class="requeue">
						<a href="#" id="'.$order->ID.'" class="button">'.__('Requeue Order').'</a>
				</p>';
			endif;
		//endif;	
	endif;
}

/*
 * Admin Menu Items
/*/
add_action('admin_menu', 'sod_qbconnector_admin_menu');
function sod_qbconnector_admin_menu() {
	global $menu;
	//add_submenu_page('woocommerce', __('Quickbooks', 'woocommerce'), __('Quickbooks', 'woocommerce'), 'shop_manager', 'quickbooks_setup', 'sod_qbconnector_setup');
	add_submenu_page('woocommerce', __('Quickbooks', 'woocommerce'), __('Quickbooks', 'woocommerce'), 'administrator', 'quickbooks_setup', 'sod_qbconnector_setup');
}

/*
 * Plugin Options Init
 */
add_action( 'admin_init', 'sod_quickbooks_default_settings' );
function sod_quickbooks_default_settings() {
	register_setting('sod_quickbooks_defaults', 'sod_quickbooks_defaults' );
	register_setting('sod_quickbooks_webconnector', 'sod_quickbooks_webconnector' );
	register_setting( 'sod_quickbooks_inv_defaults', 'sod_quickbooks_inv_defaults' );
	register_setting( 'sod_quickbooks_status', 'sod_quickbooks_status' );
	/*
	 * Setup Settings Section
	 */
	add_settings_section( 'sod_qbconnector_setup_description', 'Quickbooks Setup', 'sod_qbconnector_setup_description', 'sod_qbconnector_setup');
	add_settings_section( 'sod_web_qbconnector_setup_description', '', 'sod_web_qbconnector_setup_description', 'sod_web_qbconnector_setup');
	add_settings_section( 'sod_qbconnector_productsync_description', 'Product Validation', 'sod_qbconnector_productssync_description', 'sod_qbconnector_productssync');
	add_settings_section( 'sod_quickbooks_account_settings', 'Default Account Options', 'sod_quickbooks_account_settings', 'sod_qbconnector_setup');
	add_settings_section( 'sod_quickbooks_posting_settings', 'Order Posting Options', 'sod_quickbooks_posting_settings', 'sod_qbconnector_setup');
	add_settings_section( 'sod_quickbooks_customer_settings', 'Customer Options', 'sod_quickbooks_customer_settings', 'sod_qbconnector_setup');
	add_settings_section( 'sod_quickbooks_payments_settings', 'Payment Method Mappings', 'sod_quickbooks_payments_settings', 'sod_qbconnector_setup');
	add_settings_section( 'sod_quickbooks_salestax_settings', 'Sales Tax Mappings', 'sod_quickbooks_salestax_settings', 'sod_qbconnector_setup');
	add_settings_section( 'sod_quickbooks_shipping_settings', 'Shipping Settings', 'sod_quickbooks_shipping_settings', 'sod_qbconnector_setup');
	add_settings_section( 'sod_quickbooks_inventory_settings', 'Product Sync Settings', 'sod_quickbooks_inventory_settings', 'sod_qbconnector_productssync');
	add_settings_section( 'sod_quickbooks_webconnector', 'Webconnector Configuration', 'sod_web_qbconnector_setup_description', 'sod_web_qbconnector_setup');
	add_settings_section( 'sod_quickbooks_webconnector', 'WebConnector QWC File Generation', 'sod_quickbooks_qwc_settings', 'sod_web_qbconnector_setup');
	add_settings_section( 'sod_quickbooks_status_grid', 'WebConnector History', 'sod_quickbooks_status_grid', 'sod_quickbooks_status');
	
}
function sod_quickbooks_status_grid(){
	global $wpdb;
	$types = array('log','queue');
	$log_selected_class="";
	$queue_selected_class="";
	
	add_query_arg('type',$types);
	 $pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
	 $limit = 50;
	 $offset = ( $pagenum - 1 ) * $limit;
	 $type = $_GET['type'] ?$_GET['type']:"queue";
	 if(isset($type)):
		if($type=="log"){
			$log_selected_class='class="selected"';
			$total = count($wpdb->get_results( "SELECT quickbooks_log_id FROM quickbooks_log"));
			$rows = $wpdb->get_results( "SELECT quickbooks_log_id AS ID, log_datetime AS Date, msg AS Message FROM quickbooks_log LIMIT $limit OFFSET $offset" );
			$columns = array("ID","Date","Message");   	
		  }elseif($type=="queue"){
		  	$queue_selected_class='class="selected"';
			$total = count($wpdb->get_results( "SELECT quickbooks_queue_id FROM quickbooks_queue"));
			$rows = $wpdb->get_results( "SELECT quickbooks_queue_id AS ID, dequeue_datetime AS Date,qb_action AS Action,qb_status AS Status, msg AS Message FROM quickbooks_queue ORDER BY ID LIMIT $limit OFFSET $offset" );
			$columns = array("ID","Date","Action","Status","Message");   
		  };
	endif;
	 
	 $num_of_pages = ceil( $total / $limit );
	 $page_links = paginate_links( array(
	    'base' => add_query_arg( 'pagenum', '%#%' ),
	    'format' => '',
	    'prev_text' => __( '&laquo;', 'aag' ),
	    'next_text' => __( '&raquo;', 'aag' ),
	    'total' => $num_of_pages,
	    'current' => $pagenum
	 ));?>
	 
	 	<div id="quickbooks_table">	 
			<?php 
				if ( $page_links ) {
				    echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
				}
			?>
			<div id="record_types">
				<h3><?php _e('Data');?>: </h3>
				<ul>
					<li <?php echo $queue_selected_class;?>><a href="admin.php?page=quickbooks_setup&tab=sod_quickbooks_status&pagenum=1&type=queue" data-type="open" class="coupon-filter"><?php _e('Quickbooks Queue','qbconnector');?></a></li>
					<li <?php echo $log_selected_class;?>><a href="admin.php?page=quickbooks_setup&tab=sod_quickbooks_status&pagenum=1&type=log" class="coupon-filter" data-type="open"><?php _e('Quickbooks Log','qbconnector');?></a></li>
					
				</ul>
			</div>
		 	<table class="wp-list-table widefat pages" cellspacing="0">
				<thead>
					<tr>
						<th scope="col" class="manage-column desc" style=""></th>
						<?php foreach($columns as $column){?>
						<th scope="col" id="<?php echo strtolower($column);?>" class="manage-column column-<?php echo strtolower($column);?>" style="">
							<?php echo $column;?>
						</th>
					<?php }?>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th scope="col" class="manage-column desc" style=""></th>
						<?php foreach($columns as $column){?>
						<th scope="col" id="<?php echo strtolower($column);?>" class="manage-column column-<?php echo strtolower($column);?>" style="">
							<?php echo $column;?>
						</th>
					<?php }?>
					</tr>
				</tfoot>
				<tbody id="the-list">
					<?php 
						switch($type){
							case "queue":
								sod_qbconnector_get_queue_rows($rows);
								break;
								case "log":
								sod_qbconnector_get_log_rows($rows);	
								break;		
					
				}?>
			</tbody>
	
<?php }
function sod_qbconnector_get_queue_rows($rows){
	foreach ($rows as $key => $data){
		switch($data->Status){
			case"s":
				$data->Status = "Success";
			break;
			case"q":
				$data->Status ="Queued";
			break;
			case"e":
				$data->Status ='Error';
			break;
			case"h":
				$data->Status = "Handled";
			break;
		};?>
		<tr class="hentry alternate iedit author-self <?php echo $used_class;?>" valign="top">
			<td></td>
			<td class="id column-id">
				<strong>
					<label id="id"><?php echo $data->ID;?></label>
				</strong>
			</td>
			<td class="date column-date" style="min-width:125px;">
				<label id="date" ><?php echo $data->Date;?></label>
			</td>
			<td class="action column-action" style="min-width:125px;">
				<label id="action"><?php echo sod_qbconnector_action_translater($data->Action);?></label>
			</td>
			<td class="status column-status" style="min-width:125px;">
				<label id="status"><?php echo $data->Status;?></label>
			</td>
			<td class="message column-message">
				<label id="message"><?php echo $data->Message;?></label>
			</td>
		</tr>
<?php 
}
}

function sod_qbconnector_get_log_rows($rows){
	foreach ($rows as $key => $data){
	?>
		<tr class="hentry alternate iedit author-self <?php echo $used_class;?>" valign="top">
			<td></td>
			<td class="id column-id">
				<strong>
					<label id="id"><?php echo $data->ID;?></label>
				</strong>
			</td>
			<td class="date column-date" style="min-width:125px;">
				<label id="date" ><?php echo $data->Date;?></label>
			</td>
			<td class="message column-message">
				<label id="message"><?php echo $data->Message;?></label>
			</td>
		</tr>
<?php }
}
function sod_qbconnector_setup_description() {
	$html =  '<div class="desc" style="float:left;">
				<div class="left">
					<label>'.__('General Quickbooks Account Mappings and Options. Make sure that <em><strong>ALL</strong></em> options are selected before you start syncing.', 'sod_qbconnector').'</label>
				</div>
				<div style="clear:both;"></div>
				<div class="left" style="padding:15px 0; margin:0 20px; float:left;">
					<a href="#" class="recheck_defaults left button" >'. __('Recheck All Accounts','sod_qbconnector') . '</a>
				</div>
				<div class="left" style="padding:15px 0; margin:0 20px; float:left;">
					<a href="#" class="auto_create_accounts left button" >'. __('Auto-Create Accounts in QuickBooks','sod_qbconnector') . '</a>
				</div>
				<div class="left" style="padding:15px 0;float:left;">
					<a href="#" class="recover_orders left button" >'. __('Recover Orders','sod_qbconnector') . '</a>
				</div>
			</div><div style="clear:both;"></div>';
	echo $html;
	}
function sod_qbconnector_productssync_description() {
		$html =  '<div class="desc">
				<div class="left" style="width:500px;">
					<label>'.__('Click the button to re-validate <strong>ALL</strong> of your website products. This process will check to make sure that ALL of your products exist in QuickBooks. This will help to ensure that orders will post successfully. This will NOT update the pricing / inventory counts on your website. This is only going to update the internal QuickBooks data used to programmatically interact with QuickBooks. <br/><br/><em>Note: Depending on the number of products you have, this can take a while. After clicking this button, please start the web connector. <strong>If you have a large catalog, this will take a LONG time.</strong></em>', 'sod_qbconnector').'</label>
				</div>
				<div class="left" style="clear:both;padding:15px 0;">
					<a href="#" class="revalidate left button" >'. __('Validate All Products ','sod_qbconnector') . '</a>
				</div>
			</div><div style="clear:both;"></div>';
	echo $html;
}
function sod_web_qbconnector_setup_description(){
	$html = '<div id="faqs"">
			<h3>Quickbooks Web Connector Setup</h3>
			<h4>What\'s the QuickBooks Web Connector?</h4>
			<div>
				<p>The QuickBooks Web Connector is a software application that runs on Microsoft Windows that enables specially designed web-based applications to exchange data with QuickBooks products.
				Quickbooks Webconnector QWC file setup and generation. You should be able to find it on your computer by going to <em>Quickbooks > Web Connector</em>
				</p>
			</div>
			<h4>Why\'s everything disabled?</h4>
			<div>
				<p>All of the setup fields are disabled until you generate and download a QuickBooks Web Connector file.</p>
			</div>
			<h4>What do I do after I download the qwc file?</h4>
			<div>
				<p>After you\'ve downloaded the Woocommerce qwc file to connect QuickBooks to your website, open up the QuickBooks Web Connector application from your computer.
				Then click the <em>Add Application</em> button in the QuickBooks Web Connector and select the qwc file that you just downloaded. Once the Web Connector loads your file, your website will start syncing with QuickBooks. 
				After the website verifies that it has a connection to QuickBooks the rest of the setup fields will become enabled.
				</p>
			</div>
			<h4>What username and password do I use?</h4>
			<div>
				<p>This is completely up to you. The username and password are <em><strong>ONLY</strong></em> used by the webconnector. It should <em><strong>not</strong></em> be your Wordpress credentials or your Quickbooks credentials. This is just so the
				webconnector can communicate with QuickBooks.
				</p>
			</div>
			<h4>What ssl path do I use?</h4>
			<div>
				<p>
					The SSL path is the https address for the root of your wordpress install. You don\'t need to have a dedicated ssl. Most shared certificates will work.
				</p>
			</div>
			</div>';
	
	echo $html;

}
function sod_qbconnector_action_translater($item){
	$action = array(
				'QUICKBOOKS_CHECK_CONNECTION'		=> 'Check Connection to QuickBooks',
				'QUICKBOOKS_ADD_CUSTOMER' 			=> 'Add Customer',
				'QUICKBOOKS_ADD_CUST_RCPT'			=> 'Add Customer',
				'QUICKBOOKS_ADD_RCPT_CUST'			=> 'Add Customer',
				'QUICKBOOKS_ADD_RECEIPT' 			=> 'Add Receipt',
				'QUICKBOOKS_ADD_SO' 				=> 'Add Sales Order',
				'QUICKBOOKS_ADD_INVOICE'			=> 'Add Invoice',
				'QUICKBOOKS_ADD_PAYMENT'			=> 'Add Payment',
				'QUICKBOOKS_CUST_QUERY'				=> 'Customer Query', 
				'QUICKBOOKS_CUST_ACCT_SETUP'		=> 'Setup Customer Accounts',
				'QUICKBOOKS_CLASSES'				=> 'Setup Classes',
				'QUICKBOOKS_ITEM_INVENTORY_QUERY' 	=> 'Inventory Item Query',
				'QUICKBOOKS_ITEM_INVENTORY_ADD' 	=> 'Add Inventory Item',
				'QUICKBOOKS_ITEM_INV_UPDATE' 		=> 'Inventory Item Update',
				'QUICKBOOKS_INVENTORY_SYNC_START' 	=> 'Start Inventory Items Sync',
				'QUICKBOOKS_NONINV_SYNC_START' 		=> 'Start Non-Inventory Items Sync',
				'QUICKBOOKS_NONINV_ADD' 			=> 'Non-Inventory Item Add',
				'QUICKBOOKS_NONINV_UPDATE' 			=> 'Non-Inventory Item Update',
				'QUICKBOOKS_INCOME_ACCOUNTS_SETUP'	=> 'Setup Income Accounts',
				'QUICKBOOKS_EXPENSE_ACCOUNT_SETUP'	=> 'Setup Expense Accounts',
				'QUICKBOOKS_COGS_ACCOUNTS_SETUP'	=> 'Setup COGs Accounts',
				'QUICKBOOKS_SALESTAX_SETUP'			=> 'Setup Sales Taxes',
				'QUICKBOOKS_SALESTAX_RATES_SETUP'	=> 'Setup Sales Tax Rates',
				'QUICKBOOKS_PAYMENT_METHODS'		=> 'Setup Payment Methods',
				'QUICKBOOKS_ASSET_ACCTS'			=> 'Setup Asset Accounts',
				'QUICKBOOKS_DEPOSIT_ACCTS'			=> 'Setup Deposit Accounts',
				'QUICKBOOKS_ALL_INV_SETUP'			=> 'Setup Other Accounts',
				'QUICKBOOKS_RECEIVABLES'			=> 'Setup Receivables Accounts',
				'QUICKBOOKS_DISOCUNT_ACCT_ADD'		=> 'Discount Account Add',
				'QUICKBOOKS_DISOCUNT_ITEM_ADD'		=> 'Discount Item Add',
				'QUICKBOOKS_DISCOUNT_ITEM_QUERY'	=> 'Discount Item Query',
				'QUICKBOOKS_DISCOUNT_ACCT_QUERY'	=> 'Discount Account Query',
				'QUICKBOOKS_INVENTORY_VALIDATE'		=> 'Product Validation'
		);
		$action = apply_filters('sod_qb_action_translater', $action);
		if(isset($action[$item])){
			return $action[$item];
		}else{
			return false;
		}
		
}
/*
 * Settings Form using the WP Settings API
 */ 	
function sod_qbconnector_setup(){
	global $quickbooks;
	
	$quickbooks->check_defaults();
	$tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'sod_qbconnector_setup';
		?>
		<div class="wrap">
			<?php sod_quickbooks_options_tabs();?>
			<?php if($tab=="sod_qbconnector_setup"){?>
					<div class="quickbooks-webconnector">
						<a href="http://marketplace.intuit.com/webconnector/" target="_blank">Click Here to Download QuickBooks Web Connector</a>
					</div>
				<form method="post" action="options.php" class="quickbooks">
					<?php 
						wp_nonce_field( 'sod_quickbooks_defaults' );
						settings_fields('sod_quickbooks_defaults');
						do_settings_sections( $tab );
						
					?>
					<input type="submit" class="button-primary" value="<?php _e('Save Settings') ?>" />
				</form>
			<?php }elseif ($tab =='sod_qbconnector_productssync') {?>
				<div class="quickbooks-webconnector">
					<a href="http://marketplace.intuit.com/webconnector/" target="_blank">Click Here to Download QuickBooks Web Connector</a>
				</div>
				<form method="post" action="options.php" class="quickbooks">
					
					<?php 
						wp_nonce_field( 'sod_quickbooks_inv_defaults' );
						settings_fields('sod_quickbooks_inv_defaults');
						do_settings_sections( $tab );
					?>
					<input type="submit" class="button-primary" value="<?php _e('Save Settings') ?>" />
				</form>
			<?php }elseif ($tab =='sod_quickbooks_status') {?>
				<div class="quickbooks-webconnector">
					<a href="http://marketplace.intuit.com/webconnector/" target="_blank">Click Here to Download QuickBooks Web Connector</a>
				</div>
					<?php 
						wp_nonce_field( 'sod_quickbooks_status' );
						settings_fields('sod_quickbooks_status');
						do_settings_sections( $tab );
					?>
					
				</form>
			<?php }elseif ($tab =='sod_web_qbconnector_setup') {?>
				<div class="quickbooks-webconnector">
					<a href="http://marketplace.intuit.com/webconnector/" target="_blank">Click Here to Download QuickBooks Web Connector</a>
				</div>
				<?php 
				wp_nonce_field( 'sod_quickbooks_defaults' );
				settings_fields('sod_quickbooks_webconnector');
				do_settings_sections( $tab );
			}?>
		</div>
		<div class="clear"></div>
<?php 
}
/*
 * Plugin Settings form builder
 * See /admin/settings.php for the array of fields
 */
function sod_form_builder($meta_box, $post_id=0){
	global $quickbooks;
	$quickbooks = new SOD_QuickBooks_Data;
	
	// Use nonce for verification
	echo '<input type="hidden" name="sod_qb_product_meta" value="', wp_create_nonce(basename(__FILE__)), '" />';
	echo '<table class="form-table">';
	$meta_array = get_option($meta_box['meta_key'],array());
	
	$section =$meta_box['section'];
	foreach ($meta_box['fields'] as $field) {
    $id = $field['id'];
		
    $meta = '';
	if($id && !empty($id) && $id !==""):
		if(array_key_exists($id,$meta_array)):
			$meta = $meta_array[$id];
		else:
			$meta = '';
		
		endif;
	endif; 
	echo '<tr>
			<th  scope="rpw" class="label">';
			
			 echo '<label for="'.$meta_box['meta_key'].'[' . $field['id'] . ']">';
			 if(isset($field['desc'])):
			 	//echo '<a class="tips" style="float:left; margin:0 10px 0 0;" data-tip="'.$field['desc']. '" href="#">[?]</a>';
			 endif;
			 echo '<h4 style="float:left;">' . $field['name'] . '</h4>';
			 
			 
			 '</label>';
			 
             if(array_key_exists('refresh_link',$field)){
					echo '<span style="clear:both; float:left;">
								<a href="#" class="refresh" data-option="'.$field['refresh_link']['option'].'">'.$field['refresh_link']['title'].'</a>	
						</span><br/>';	
				};
			// if(array_key_exists('add_link',$field)){
					// echo '<span>
								// <a href="#" class="add" data-option="'.$field['add_link']['option'].'">'.$field['add_link']['title'].'</a>	
						// </span><br/>';	
				// };
//            
           
            echo '</th><td>';
    switch ($field['type']) {
        case 'text':
			echo '<input type="text" name="'.$meta_box['meta_key'].'[', $field['id'], ']" id="'.$meta_box['meta_key'].'[', $field['id'], ']" value="', $meta ? $meta : $field['std'], '" />';
			if(array_key_exists('suffix',$field)):
				echo '<span class="right suffix">'.$field['suffix'].'</span>';
			endif;
            break;
		 case 'label':
            echo '<label name="'.$meta_box['meta_key'].'[', $field['id'], ']" id="'.$meta_box['meta_key'].'[', $field['id'], ']">',$field['text'] , '</label>';
            break;
        case 'textarea':
            echo '<textarea name="'.$meta_box['meta_key'].'[', $field['id'], ']" id="'.$meta_box['meta_key'].'[', $field['id'], ']" cols="60" rows="4" style="width:97%">', $meta ? $meta : $field['std'], '</textarea>', '<br />', $field['desc'];
            break;
        case 'qbdata-select':
			if(empty($field['options'])){
				echo '<mark class="refreshing">Retrieving Accounts from Quickbooks</mark>';
			} else {
			// if(array_key_exists('add_link',$field)){
				// echo '<input type="text" name="'.$meta_box['meta_key'].'[', $field['id'], ']" id="'.$meta_box['meta_key'].'[', $field['id'], ']" value="', $meta ? $meta : $field['std'], '" />';
			// }
            echo '<select name="'.$meta_box['meta_key'].'[', $field['id'], ']" class="'. $field['class'].'">';
			    foreach ($field['options'] as $key=>$value) {
                    	//echo '<option value="'.$value['ListID'].'"',$quickbooks->$section->$id == $value['ListID'] ? ' selected="selected"' : '',' >'.$key.'</option>';
                 	if($id):
						
						$selector = $quickbooks->$section->$id;
					else:
						$selector="";	
					endif;
			    	echo '<option value="'.$value['ListID'].'"',$selector== $value['ListID'] ? ' selected="selected"' : '',' >'.$key.'</option>';
                //}
                }
                echo '</select>';
				
			}
            break;
		case 'select':
		
			//$quickbooks->$section->$id
			if(empty($field['options'])){
				echo '<mark class="refreshing">Retrieving Accounts from Quickbooks</mark>';
			} else {
			   	echo '<select name="'.$meta_box['meta_key'].'[', $field['id'], ']" class="'. $field['class'].'">';
			    foreach ($field['options'] as $key=>$value) {
                    echo '<option value="'.$key.'"',$quickbooks->$section->$id == $key ? ' selected="selected"' : '',' >'.$value.'</option>';
                }
	            echo '</select>';
			}
			break;
		case 'select-array':
			
			if(empty($field['options'])){
				echo '<mark class="refreshing">Retrieving Accounts from Quickbooks</mark>';
			} else {
			foreach($field['options'] as $key=>$value){
				$array = $quickbooks->$section->$id;
				
				echo '<select name="'.$meta_box['meta_key'].'[', $field['id'], '][',$key,']" class="'. $value['class'].'">';
				    foreach ($value['options'] as $option=>$option_value) {
				    	
	                    echo '<option value="'.$option.'"',$array[$key] == $option ? ' selected="selected"' : '',' >'.$option_value.'</option>';
	                }
	            echo '</select>';
            }
			}
			break;
		case 'mapping':
		echo '<div class="mapping">';
			echo '<div class="mapping-heading">';
			echo 	'<h4>'.$field['options']['labels']['heading'].'</h4>';
			echo 	'<h4>'.$field['options']['selects']['heading'].'</h4>';
			echo '</div>';
			
			foreach($field['options']['labels']['data'] as $key=>$value){
				echo '<div class="mapping-row">';
					echo '<div class="label">';
					if(is_object($value)){
						echo '	<label id="'.$key.'">'.$value->title.'</label>';
					}elseif ($key){
						echo '	<label id="'.$key.'">'.$value.'</label>';
					}else{
						echo '	<label id="'.$value.'">'.$value.'</label>';
					}
					echo '</div>';
					echo '<div class="select">';
					if(empty($field['options']['selects']['data'])){
						echo '<mark class="refreshing">Retrieving Accounts from Quickbooks</mark>';
					} else {
						
						if(is_object($value)){
							
							echo '	<select name="'.$meta_box['meta_key'].'[', $field['id'], '][',$key,']" class="',$field['options']['selects']['class'],'">';
						}else{
							$temp = str_replace(array('(',')','%','.'),'',$value);
							echo '	<select name="'.$meta_box['meta_key'].'[', $field['id'], '][',str_replace(" ","_",strtolower($temp)),']" class="',$field['options']['selects']['class'],'">';
							
						};
							echo '<option value="" >Select an Option</option>';
								foreach($field['options']['selects']['data'] as $option=>$option_value){
									
									if(is_array($option_value)){
										if($id && $key && !$temp):
											$array = $quickbooks->$section->$id;
											$selector = $array[$key];
										elseif ($id && $temp):
											$new_key = str_replace(" ","_",strtolower($temp));
											$array = $quickbooks->$section->$id;
											$selector = $array[$new_key];
										else:
											$selector="";
										endif;
										if(array_key_exists('ListID', $option_value)):
											echo '<option value="'.$option_value['ListID'].'"', $selector== $option_value['ListID'] ? ' selected="selected"' : '',' >'.$option.'</option>';
										endif;
										//echo '<option value="'.$option_value['ListID'].'"',$quickbooks->$section->$id->$key == $option_value['ListID'] ? ' selected="selected"' : '',' >'.$option.'</option>';	
									}else{
										$index = str_replace(" ","_",strtolower($temp));
										
										echo '<option value="'.$option.'"',$quickbooks->$section->$id->$index == $option ? ' selected="selected"' : '',' >'.$option_value.'</option>';
									}
									
								}
						echo '	</select>';
						if(array_key_exists('refresh_link',$field)){
						echo '<span>
								<a href="#" class="refresh" data-option="'.$field['refresh_link']['option'].'">'.$field['refresh_link']['title'].'</a>	
						</span>';	
				}
					}
					echo '</div>';
				echo '</div>';
			echo '</div>';
				}
			break;
			
		case 'textbox_mapping':
		echo '<div class="mapping">';
			echo '<div class="mapping-heading">';
			echo 	'<h4>'.$field['options']['labels']['heading'].'</h4>';
			echo 	'<h4>'.$field['options']['selects']['heading'].'</h4>';
			echo '</div>';
			foreach($field['options']['labels']['data'] as $key=>$value){
				echo '<div class="mapping-row">';
					echo '<div class="label">';
					if(is_object($value)){
						echo '	<label id="'.$key.'">'.$value->title.'</label>';
						
					}else{
						echo '	<label id="'.$value.'">'.$value.'</label>';
					}
					echo '</div>';
					echo '<div class="text_box">';
						$stored = $quickbooks->$section->$id;
						echo '<input type="text" name="'.$meta_box['meta_key'].'[', $field['id'], '][',$key,']" id="'.$meta_box['meta_key'].'[', $field['id'], '][',$key,']" value="'.$stored[$key].'" size="30" />';
					echo '</div>';
				echo '</div>';
			echo '</div>';
				}
			break;
        case 'radio':
            foreach ($field['options'] as $option) {
                echo '<input type="radio" name="'.$meta_box['meta_key'].'[', $field['id'], ']" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
            }
            break;
        case 'checkbox':
			echo '
            		<input type="checkbox" class="'.$field['class'].'"  name="'.$meta_box['meta_key'].'[', $field['id'], ']" id="'.$meta_box['meta_key'].'[', $field['id'], ']"', $meta ? ' checked="checked"' : '', ' />
            	
            	';
            break;
    }
    echo     '<td>',
        '</tr>';
}
echo '</table>';
}
/*
 * Ajax Calls 
 */

// Check qwc download
add_action('wp_ajax_check_qwc', 'check_qwc');
function check_qwc(){
	$nonce = $_POST['check_qwc_nonce'];
	//echo $nonce;
	// check to see if the submitted nonce matches with the
	// generated nonce we created earlier
	if ( ! wp_verify_nonce( $nonce, 'check_qwc_nonce' ) )
    die ( 'Busted!');
	$check 		= apply_filters('sod_qb_wc_generated', get_option('webconnector_generated') );
	$connected 	= apply_filters('sod_qb_is_connected', get_option('quickbooks_connected') );
	
	if($connected){
		echo $check;	
	}else{
		return false;
	}
	
}
/* Post QWC details to db when generating file*/
add_action('wp_ajax_create_qwc', 'create_qwc');  
function create_qwc(){
	global $wpdb;
	$nonce = $_POST['create_qwc_nonce'];
	//echo $nonce;
	// check to see if the submitted nonce matches with the
	// generated nonce we created earlier
	if ( ! wp_verify_nonce( $nonce, 'create_qwc_nonce' ) )
    die ( 'Busted!');
	$options = $_POST['sod_quickbooks_webconnector'];
	$webconnector = new SOD_Quickbooks_Data;
		$args = array(
			'frequency'=>'',
			'username'=>$_POST['username'],
			'password'=>$webconnector->encode($_POST['password']),
			'ssl'=>$_POST['ssl'],
		);
	update_option("sod_quickbooks_webconnector", $args);
	$wpdb->get_results( $wpdb->prepare( "DELETE FROM quickbooks_user" ) );
	$wpdb->get_results( $wpdb->prepare( "UPDATE quickbooks_queue SET qb_username = '%s'", $_POST['username'] ) );
	$dsn = 'mysql://'.DB_USER.':'.DB_PASSWORD.'@'.DB_HOST.'/'.DB_NAME;
	QuickBooks_Utilities::createUser($dsn, $_POST['username'], $_POST['password']);
	update_option('webconnector_generated',1);
	//$connector = get_option('quickbooks_connected');
	//if(!$connector['AutoLogin']){
	$Queue = new QuickBooks_WebConnector_Queue($webconnector->dsn);
	$Queue->enqueue('QUICKBOOKS_CHECK_CONNECTION',uniqid(),0);	
	//}
	echo "User Created";
}
add_action('wp_ajax_auto_create_accounts', 'sod_auto_create');
function sod_auto_create(){
	$quickbooks = new SOD_Quickbooks_Data;
	$quickbooks->qb_auto_create();
}
/*
 * Refresh Accounts to pull back from QB
 */
add_action('wp_ajax_recheck_defaults', 'recheck_defaults');
function recheck_defaults(){
	$nonce = $_POST['refresh_account_nonce'];
	if ( ! wp_verify_nonce( $nonce, 'refresh_account_nonce' ) )
    die ( 'Busted!');
	$quickbooks = new SOD_Quickbooks_Data;
	$quickbooks->recheck_defaults();
}

add_action('wp_ajax_refresh_accounts', 'refresh_accounts');
function refresh_accounts(){
	$nonce = $_POST['refresh_account_nonce'];
	if ( ! wp_verify_nonce( $nonce, 'refresh_account_nonce' ) )
    die ( 'Busted!');
	$option = $_POST['option'];
	delete_option($option);
	delete_option('started_setup');
	$quickbooks = new SOD_Quickbooks_Data;
	$quickbooks->check_defaults();
}
add_action('wp_ajax_validate_products', 'sod_validate_products');
function sod_validate_products(){
	$nonce = $_POST['refresh_account_nonce'];
	if ( ! wp_verify_nonce( $nonce, 'refresh_account_nonce' ) )
    die ( 'Busted!');
	$quickbooks = new SOD_Quickbooks_Data;
	$Queue = new QuickBooks_WebConnector_Queue($quickbooks->dsn);
	$args = array( 
		'post_type'=> array(
			'product',
		),
		'posts_per_page'=>-1
	);
	$query = new WP_Query();
	
	$query->query($args);
	
	$products = $query->posts;
	if($products){
		
		foreach($products as $product){
			delete_post_meta($product->ID,'_quickbooks_data');
			delete_post_meta($product->ID,'_qb_item_type');
			delete_post_meta($product->ID,'_qb_fullname');
			delete_post_meta($product->ID,'_qb_listid');
			delete_post_meta($product->ID,'_qb_auto_add_try_count');
			$Queue->enqueue('QUICKBOOKS_ITEM_INVENTORY_QUERY', $product->ID, 9999);
		}
	}
	$args = array( 
		'post_type'=> array(
			'product_variation'
		),
		'posts_per_page'=>-1
	);
	$query = new WP_Query();
	
	$query->query($args);
	
	$products = $query->posts;
	if($products){
		
		foreach($products as $product){
			delete_post_meta($product->ID,'_quickbooks_data');
			delete_post_meta($product->ID,'_qb_item_type');
			delete_post_meta($product->ID,'_qb_fullname');
			delete_post_meta($product->ID,'_qb_listid');
			$Queue->enqueue('QUICKBOOKS_ITEM_INVENTORY_QUERY', $product->ID, 999);
		}
	}
}
add_action('wp_ajax_prod_refresh', 'prod_refresh');
function prod_refresh(){
	$nonce = $_POST['refresh_nonce'];
	if ( ! wp_verify_nonce( $nonce, 'refresh_nonce' ) )
    die ( 'Busted!');
	$option = $_POST['id'];
	delete_post_meta($option,'_quickbooks_data');
	delete_post_meta($option,'_qb_item_type');
	delete_post_meta($option,'_qb_fullname');
	delete_post_meta($option,'_qb_listid');
	$quickbooks = new SOD_Quickbooks_Data;
	$Queue = new QuickBooks_WebConnector_Queue($quickbooks->dsn);
	$Queue->enqueue('QUICKBOOKS_ITEM_INVENTORY_QUERY', $option);
}

/*
 * Turn SYNC on/off for individual items
 */
add_action('wp_ajax_sync_item', 'sync_item');
function sync_item(){
	$nonce = $_POST['sync_item_nonce'];
	if ( ! wp_verify_nonce( $nonce, 'sync_item_nonce' ) )
    die ( 'Busted!');
	$ID = $_POST['id'];
	$sync_status = $_POST['sync_status'];
	if($sync_status=="checked"){
		$sync = "on";
	}else{
		$sync = "";
	}
	update_post_meta($ID,'_sync_status',$sync); 
}


function update_account(){
	if ( ! wp_verify_nonce( $nonce, 'update_account_nonce' ) )
    die ( 'Busted!');
	$ID = $_POST['id'];
	$listid = $_POST['listid'];
	$account = $_POST['account'];
	update_post_meta($ID,$account,$listid);
}
add_action('wp_ajax_recover_orders', 'sod_quickbooks_recover_orders');
function sod_quickbooks_recover_orders(){
	//Validate permissions	
	$nonce = $_POST['refresh_account_nonce'];
	// check to see if the submitted nonce matches with the
	// generated nonce we created earlier
    if ( ! wp_verify_nonce( $nonce, 'refresh_account_nonce' ) )
	die ( 'Busted!');
	/*
	 * Move to requeueing order
	 */
	$quickbooks = new SOD_Quickbooks_Data;
	$orders 	= $quickbooks->posts_without_meta('_quickbooks_data','shop_order','','','');
	$Queue 		= new QuickBooks_WebConnector_Queue($quickbooks->dsn);
	if($orders):
		foreach($orders as $order){
			
			$quickbooks->ID = $order->ID;
			$quickbooks->load("order");
			$status = wp_get_post_terms($order->ID, 'shop_order_status', array("fields" => "names"));
			if($status[0] == $quickbooks->settings->order_status_trigger):
				if(get_post_meta( $order->ID , '_customerListID' )){
					
					delete_post_meta( $order->ID , '_customerListID' );
						
				};	
				update_post_meta($order->ID, '_qb_order_requeued','yes');
				if($quickbooks->settings->post_orders =='on'){
					if($quickbooks->settings->create_customer_account =='on'){
						/*1. Check for customer ListID 
						 * if exists, send directly as receipt;
						 */
						if($quickbooks->customerListID){
							switch($quickbooks->settings->post_order_type){
								case "sales_order":
								$Queue->enqueue('QUICKBOOKS_ADD_SO',$order->ID,7);
									break;
								case "sales_receipt":
									$Queue->enqueue('QUICKBOOKS_ADD_RECEIPT',$order->ID,8);
									break; 
							}
						}else{
							/*2. If No CustomerListID
							 * send as add customer request, then add SO/SR request
							 */
							$Queue->enqueue('QUICKBOOKS_CUST_QUERY',$order->ID,6);
						}
					}else{
						/*3. Post Default Account 
						 * If not posting to unique customer accounts, post to default;
						 */
						switch($quickbooks->settings->post_order_type){
								case "sales_order":
								$Queue->enqueue('QUICKBOOKS_ADD_SO',$order->ID,7);
									break;
								case "sales_receipt":
									$Queue->enqueue('QUICKBOOKS_ADD_RECEIPT',$order->ID,8);
									break;
						} 
					}
				} //If not posting orders, do nothing;
			endif;
		}
	endif;	
}
add_action('wp_ajax_requeue_order', 'sod_quickbooks_requeue_order');  
function sod_quickbooks_requeue_order(){
	//Validate permissions	
	$nonce = $_POST['requeue_nonce'];
	// check to see if the submitted nonce matches with the
	// generated nonce we created earlier
    if ( ! wp_verify_nonce( $nonce, 'requeue_nonce' ) )
	die ( 'Busted!');
	/*
	 * Move to requeueing order
	 */
	$order_id = $_POST['order_id'];
	if(get_post_meta($order_id,'_customerListID')){
		delete_post_meta($order_id,'_customerListID');	
	};
	$quickbooks = new SOD_Quickbooks_Data;
	$quickbooks->ID = $order_id;
	$quickbooks->load("order");
	$Queue = new QuickBooks_WebConnector_Queue($quickbooks->dsn);
	update_post_meta($order_id, '_qb_order_requeued','yes');
	if($quickbooks->settings->post_orders =='on'){
		if($quickbooks->settings->create_customer_account =='on'){
			/*1. Check for customer ListID 
			 * if exists, send directly as receipt;
			 */
			if($quickbooks->customerListID){
				switch($quickbooks->settings->post_order_type){
					case "sales_order":
					$Queue->enqueue('QUICKBOOKS_ADD_SO',$order_id,7);
						break;
					case "sales_receipt":
						$Queue->enqueue('QUICKBOOKS_ADD_RECEIPT',$order_id,8);
						break; 
				}
			}else{
				/*2. If No CustomerListID
				 * send as add customer request, then add SO/SR request
				 */
				$Queue->enqueue('QUICKBOOKS_CUST_QUERY',$order_id,6);
			}
		}else{
			/*3. Post Default Account 
			 * If not posting to unique customer accounts, post to default;
			 */
			switch($quickbooks->settings->post_order_type){
					case "sales_order":
					$Queue->enqueue('QUICKBOOKS_ADD_SO',$order_id,7);
						break;
					case "sales_receipt":
						$Queue->enqueue('QUICKBOOKS_ADD_RECEIPT',$order_id,8);
						break;
			} 
		}
	} //If not posting orders, do nothing;	
}
