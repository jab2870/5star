<?php
//require_once('../../../wp-load.php');
/**
 * Require the utilities class
 */

$options = $_POST['sod_quickbooks_webconnector'];
if(isset($options) && !empty($options)){
	require_once($options['plugin_dir'].'../QuickBooks.php');
	$characters 			= 'ABCD0123456789';
	$string 				= '';
	$random_string_length 	= 8;
 	for ($i = 0; $i < $random_string_length; $i++) {
      	$string .= $characters[rand(0, strlen($characters) - 1)];
 	}
	$ssl_path 				= $options['ssl'];
	$name 					= 'WooCommerce QuickBooks Connector';				// A name for your server (make it whatever you want)
	$descrip 				= "QuickBooks Connector for Woocommerce."; 		// A description of your server
	$api_key 				= $options['key'];
	$appurl 				= 	$ssl_path."/?qbconnector=".$api_key;//plugins_url('sod-qb-qwc.php', __FILE__);	// This *must* be httpS:// (path to your QuickBooks SOAP server)
	$appsupport 			= $ssl_path."/?qbconnector=support"; 		// This *must* be httpS:// and the domain name must match the domain name above
	$username 				= $options['username'];		// This is the username you stored in the 'quickbooks_user' table by using QuickBooks_Utilities::createUser()
	$fileid 				= $string . '-86F1-4FCC-B1FF-966DE1813D21';		// Just make this up, but make sure it keeps that format
	$ownerid 				= $string . '-86F1-4FCC-B1FF-166DE1813D21';		// Just make this up, but make sure it keeps that format
	$qbtype 				= QUICKBOOKS_TYPE_QBFS;	// You can leave this as-is unless you're using QuickBooks POS
	$readonly 				= false; // No, we want to write data to QuickBooks
	// Generate the XML file
	$QWC 					= new QuickBooks_WebConnector_QWC($name, $descrip, $appurl, $appsupport, $username, $fileid, $ownerid, $qbtype, $readonly, $run_every_n_seconds);
	$xml 					= $QWC->generate();
	// Send as a file download
	header('Content-type: text/xml');
	header('Content-Disposition: attachment; filename="quickbooks-connector.qwc"');
	print($xml);
	exit;
}