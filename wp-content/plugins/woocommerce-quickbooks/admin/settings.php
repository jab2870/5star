<?php 
/*
 * Default Accounts
 */
function sod_quickbooks_account_settings(){
	global $quickbooks, $woocommerce;
	$types = array("Income","COGS","Deposits","Receivables","Expense","Assets");
	foreach($types as $type){
		$quickbooks_accounts[$type] = $quickbooks->get_accounts($type);	
	}
	$meta_box = array(
		    'id' => 'sod_quickbooks_products',
		    'title' => __('Quickbooks Options', 'woocommerce'),
		    'page' => 'product',
		    'context' => 'normal',
		    'priority' => 'default',
		    'section'=>'settings',
		    'meta_key'=> 'sod_quickbooks_defaults',
		    'fields' => array(
	        array(
	            'name' => 'Income Account',
	            'desc' => 'Select the default income account to use',
	            'id' => 'income',
	            'type' => 'qbdata-select',
	            'options'=> $quickbooks_accounts['Income'],
	            'class' =>'chzn-select wide',
	            'refresh_link'=>array(
					'option'=>'_sod_quickbooks_income_accounts',
					'title' =>'Refresh Income Accounts from QB'
				),
				'add_link'=>array(
					'option'=>'_sod_quickbooks_income_accounts',
					'title' =>'Add New Income Account'
				)
	        ),
	         array(
	            'name' => 'COGs Account',
	            'desc' => 'Select the default COGs account to use.',
	            'id' => 'cogs',
	            'type' => 'qbdata-select',
	             'options'=> $quickbooks_accounts['COGS'],
	              'class' =>'chzn-select wide',
	            'refresh_link'=>array(
					'option'=>'_sod_quickbooks_cogs_accounts',
					'title' =>'Refresh COGS Accounts from QB'
				),
				'add_link'=>array(
					'option'=>'_sod_quickbooks_cogs_accounts',
					'title' =>'Add New COGS Account'
				)
	        ),
	        array(
	            'name' => 'Deposit Account',
	            'desc' => 'Select the default deposit account to use.',
	            'id' => 'deposits',
	            'type' => 'qbdata-select',
	            'options'=> $quickbooks_accounts['Deposits'],
	            'class' =>'chzn-select wide',
	            'refresh_link'=>array(
					'option'=>'_sod_quickbooks_deposit_accounts',
					'title' =>'Refresh Deposit Accounts from QB'
				),
				'add_link'=>array(
					'option'=>'_sod_quickbooks_deposit_accounts',
					'title' =>'Add New Deposit Account'
				)
	        ),
	        array(
	            'name' => 'Asset Account',
	            'desc' => 'Select the default asset account to use.',
	            'id' => 'assets',
	            'type' => 'qbdata-select',
	            'options'=> $quickbooks_accounts["Assets"],
	            'class' =>'chzn-select wide',
	            'refresh_link'=>array(
					'option'=>'_sod_quickbooks_asset_accounts',
					'title' =>'Refresh Asset Accounts from QB'
				),
				'add_link'=>array(
					'option'=>'_sod_quickbooks_asset_accounts',
					'title' =>'Add New Asset Account'
				)
	        ),
	          array(
	            'name' => 'Accounts Receivable',
	            'desc' => 'Select the default account receivable.',
	            'id' => 'receivables',
	            'type' => 'qbdata-select',
	            'options'=> $quickbooks_accounts['Receivables'],
	            'class' =>'chzn-select wide',
	            'refresh_link'=>array(
					'option'=>'_sod_quickbooks_receivables',
					'title' =>'Refresh Recaivables Accounts from QB'
				),
				'add_link'=>array(
					'option'=>'_sod_quickbooks_receivables',
					'title' =>'Add New Receivables Account'
				)
	        ),
			 array(
	            'name' => 'Expense Account',
	            'desc' => 'Select the default account expense.',
	            'id' => 'expense',
	            'type' => 'qbdata-select',
	            'options'=> $quickbooks_accounts['Expense'],
	            'class' =>'chzn-select wide',
	            'refresh_link'=>array(
					'option'=>'_sod_quickbooks_expense_accounts',
					'title' =>'Refresh Expense Accounts from QB'
				),
				'add_link'=>array(
					'option'=>'_sod_quickbooks_expense_accounts',
					'title' =>'Add New Expense Account'
				)
	        )
	   	)
	);
	$meta_box = apply_filters('sod_qb_account_mapping_fields', $meta_box);
	sod_form_builder($meta_box);
}
/*
 * Posting Options
 */
function sod_quickbooks_posting_settings(){
	global $quickbooks;
	$prefix_labels = array(
		'sr_prefix'=>'Sales Receipt Prefix',
		'so_prefix'=>'Sales Order Prefix',
		'invoice_prefix'=>'Invoice Prefix',
		'payment_prefix'=>'Payment Prefix'
	);
	$prefix_text_boxes = array(
		'sr_prefix',
		'so_prefix',
		'invoice_prefix',
		'payment_prefix'
		);
	$meta_box = array(
		    'id' => 'sod_quickbooks_products',
		    'title' => __('Quickbooks Options', 'woocommerce'),
		    'page' => 'product',
		    'context' => 'normal',
		    'priority' => 'default',
		    'section'=>'settings',
		    'meta_key'=> 'sod_quickbooks_defaults',
		    'fields' => array(
		     array(
	            'name' => 'Post Orders to QuickBooks',
	            'desc' => 'Choose whether to post orders to QuickBooks or not.',
	            'id' => 'post_orders',
	            'type' => 'checkbox',
	            'class' =>''
	         ),
	           array(
	            'name' => 'Store Order XML?',
	            'desc' => 'Choose whether to store order XML',
	            'id' => 'store_xml',
	            'type' => 'checkbox',
	            'class' =>''
	         ),
	   		array(
	            'name' => 'How Should Orders be Posted To Quickbooks?',
	            'desc' => 'Choose whether to use Sales Receipts or Sales Orders.',
	            'id' => 'post_order_type',
	            'type' => 'select',
	            'options'=> array(
							"sales_receipt"=>"Sales Receipt",
							"sales_order"=>"Sales Order"
				),
	            'class' =>'chzn-select wide'
	            
	        ),
	        
	        array(
	            'name' => 'Should the attribute values for variable products be used as the Sales Receipt / Sales Order line item description?',
	            'desc' => 'Choose whether to use Sales Receipts or Sales Orders.',
	            'id' => 'use_attributes_as_desc',
	            'type' => 'checkbox',
	            'class' =>''
	            
	        ),
	        array(
	            'name' => 'Auto-create Invoices and Payments',
	            'desc' => 'Choose whether to automatically create invoices or payments for sales orders.',
	            'id' => 'create_so_invoices',
	            'type' => 'checkbox',
	            'class' =>''
	            
	     	   ),
	     	 array(
	            'name' 	=> 'Order Status Trigger to send order to QuickBooks?',
	            'desc' 	=> 'Choose whether to use Sales Receipts or Sales Orders.',
	            'id' 	=> 'order_status_trigger',
	            'type' 	=> 'select',
	            'std'	 =>'completed',
	            'options'=> array(
							"pending"	=>"Pending",
							"processing"=>"Processing",
							"completed"	=>"Completed"
							
				),
	            'class' =>'chzn-select wide'
	            
	        ),
	     	   array(
		            'name' => 'Add prefixes to your sales receipts, sales orders, invoices and paymnets',
		            'desc' => 'Enter a prefix for each item to better keep track of website orders in QuickBooks. By default, the order id is used.',
		            'id' => 'prefixes',
		            'type' => 'textbox_mapping',
		            'options'=>array(
		            	'labels'=>array(
		            		'heading'=>'Item',
		            		'data'=>$prefix_labels
		            		),
		            	'selects'=>array(
		            		'heading'=>'Prefix',
		            		'class' =>'chzn-select medium',
		            		'data'=>$prefix_text_boxes
			            )
		           )
				)
	   		)
	   	);
		$meta_box = apply_filters('sod_qb_order_posting_fields', $meta_box);
	   	sod_form_builder($meta_box);
	
	}
/*
 * Customer Settings
 */
function sod_quickbooks_customer_settings(){
	global $quickbooks, $woocommerce;
	$quickbooks = new SOD_QuickBooks_Data;
	$mapping_fields = array(
		'first'=>array(
			'name' => 'First',
			'type' => 'select',
			'class' =>'chzn-select',
			'options'=>$quickbooks->customer_mapping
		),
		'second'=>array(
			'name' => 'Second',
			'type' => 'select',
			'class' =>'chzn-select medium',
			'options'=>$quickbooks->customer_mapping
		),
		'third'=>array(
			'name' => 'Third',
			'type' => 'select',
			'class' =>'chzn-select medium',
			'options'=>$quickbooks->customer_mapping
		)
	);
	$mapping_fields	= apply_filters('sod_qb_customer_identifiers', $mapping_fields, $quickbooks);
	
	$customers = $quickbooks->get_accounts("Customers");
	
	$meta_box = array(
		    'id' => 'sod_quickbooks_products',
		    'title' => __('Quickbooks Options', 'woocommerce'),
		    'page' => 'product',
		    'context' => 'normal',
		    'priority' => 'default',
		    'section'=>'settings',
		    'meta_key'=> 'sod_quickbooks_defaults',
		    'fields' => array(
		    array(
	            'name' => 'Create Individual Customer Accounts in Quickbooks',
	            'desc' => 'Choose whether to automatically create individual customer accounts in QuickBooks. If you choose no, you\'ll need to specify an default account to post to.',
	            'id' => 'create_customer_account',
	            'type' => 'checkbox',
	            'class' =>'switch'
	           ),
	        array(
	            'name' => 'How Should Customer Names be created in QuickBooks',
	            'desc' => 'Choose whether to use Sales Receipts or Sales Orders.',
	            'id' => 'customer_identifier',
	            'type' => 'select-array',
	            'options'=> $mapping_fields
	         ),
		     array(
	            'name' => 'Default Customer Account',
	            'desc' => 'If not creating individual accounts, please select the default QuickBooks account to post orders to.',
	            'id' => 'customer',
	            'type' => 'qbdata-select',
	            'options'=> $customers,
	            'class' =>'chzn-select wide',
	            'refresh_link'=>array(
					'option'=>'_sod_quickbooks_customer_accounts',
					'title' =>'Refresh Customer Accounts from QB'
				)
	        ),
	   		)
	   	);
		$meta_box = apply_filters('sod_qb_customer_mapping_fields', $meta_box);
	   	sod_form_builder($meta_box);

}
/*
 * Payment Mappings
 */
function sod_quickbooks_payments_settings(){
	global $quickbooks,$woocommerce;
	$payments = $quickbooks->get_accounts("PaymentMethods");
	$w_payments=$woocommerce->payment_gateways->payment_gateways();
	if(empty($w_payments)){
		echo  'No Payment Methods available';
	}else{
	$meta_box = array(
		    'id' => 'sod_quickbooks_products',
		    'title' => __('Quickbooks Options', 'woocommerce'),
		    'page' => 'product',
		    'context' => 'normal',
		    'priority' => 'default',
		    'section'=>'settings',
		    'meta_key'=> 'sod_quickbooks_defaults',
		    'fields' => array(
		    	array(
		            'name' => 'Map Your WooCommerce Payment Methods to Your QuickBooks Payment Methods',
		            'desc' => 'Choose whether to automatically create individual customer accounts in QuickBooks. If you choose no, you\'ll need to specify an default account to post to.',
		            'id' => 'payment_mappings',
		            'type' => 'mapping',
		            'options'=>array(
		            	'labels'=>array(
		            		'heading'=>'Installed Payment Method',
		            		'data'=>$w_payments
		            		),
		            	'selects'=>array(
		            		'heading'=>'Quickbooks Payment Methods',
		            		'class' =>'chzn-select medium',
		            		'data'=>$payments
			            )
		           ),
		           'refresh_link'=>array(
						'option'=>'_sod_quickbooks_payment_methods',
						'title' =>'Refresh Payment Methods'
					)
				)
			)
		);
		$meta_box = apply_filters('sod_qb_payment_mapping_fields', $meta_box);
	   	sod_form_builder($meta_box);
	}
}
/*
 * Salestax Settings
 */
function sod_quickbooks_salestax_settings(){
	global $quickbooks, $woocommerce;
	$taxes = $quickbooks->get_sales_tax();
	$w_taxes_codes = $quickbooks->get_cart_tax_classes();
	$w_tax_rates = $quickbooks->get_cart_tax_rates();
	$w_local_rates = $quickbooks->get_woocommerce_local_tax_rates();
	$new_rates = array(); 
	$new_local_rates = array();
	if($w_tax_rates):
		foreach($w_tax_rates as $rate){
			$formated = $quickbooks->format_tax_rate($rate['label']);
			$new_rates[$formated]= $rate['label'];
		};
	else:
		$w_tax_rates = array();	
	endif;
	if($w_local_rates):
		foreach($w_local_rates as $rate){
			$formated = $quickbooks->format_tax_rate($rate['label']);
			$new_local_rates[$formated]= $rate['label'];
		};
	else:
		$w_local_rates=array();	
	endif;
	$meta_box = array(
		    'id' => 'sod_quickbooks_products',
		    'title' => __('Quickbooks Options', 'woocommerce'),
		    'page' => 'product',
		    'context' => 'normal',
		    'priority' => 'default',
		    'section'=>'settings',
		    'meta_key'=> 'sod_quickbooks_defaults',
		    'fields' => array(
		    	array(
	            	'name' => 'QuickBooks Sales Tax Vendor',
	            	'desc' => 'Every order in QuickBooks has to have a sales tax item applied to it. If you\'re not required to collectt sales tax for the order, what is your zero-percent sales tax rate to use.',
	            	'id' => 'sales_tax_vendor',
	            	'type' => 'text',
	             	'class' =>'wide',
		            
	        	),
		    	array(
	            	'name' => 'Default No Tax Account',
	            	'desc' => 'Every order in QuickBooks has to have a sales tax item applied to it. If you\'re not required to collectt sales tax for the order, what is your zero-percent sales tax rate to use.',
	            	'id' => 'no_tax',
	            	'type' => 'qbdata-select',
	             	'options'=> $taxes['rates'],
		            'class' =>'chzn-select wide',
		            'refresh_link'=>array(
						'option'=>'_sod_quickbooks_salestax_rates',
						'title' =>'Refresh Sales Tax Rates from QB'
					)
	        	),
		    	array(
		            'name' => 'Map Your WooCommerce Sales Tax Codes to Your QuickBooks Sales Tax Codes',
		            'desc' => 'Choose whether to automatically create individual customer accounts in QuickBooks. If you choose no, you\'ll need to specify an default account to post to.',
		            'id' => 'taxcodes_mappings',
		            'type' => 'mapping',
		            'options'=>array(
		            	'labels'=>array(
		            		'heading'=>'Website Sales Tax Codes',
		            		'data'=>$w_taxes_codes
		            		),
		            	'selects'=>array(
		            		'heading'=>'Quickbooks Sales Tax Codes',
		            		'class' =>'chzn-select medium',
		            		'data'=>$taxes['codes']
			            )
			           ),
					 'refresh_link'=>array(
						'option'=>'_sod_quickbooks_salestax_codes',
						'title' =>'Refresh Tax Codes'
					
		           )
				),
				array(
		            'name' => 'Map Your Local WooCommerce Sales Tax Rates to Your QuickBooks Sales Rates. <span class="alert">All WooCommerce Tax Rates must have a unique label assigned in order for them to show up here.</span>',
		            'desc' => 'Choose whether to automatically create individual customer accounts in QuickBooks. If you choose no, you\'ll need to specify an default account to post to.',
		            'id' => 'taxrates_local',
		            'type' => 'mapping',
		            'options'=>array(
		            	'labels'=>array(
		            		'heading'=>'Website Local Sales Tax Rates',
		            		'data'=>$new_local_rates
		            		),
		            	'selects'=>array(
		            		'heading'=>'Quickbooks Sales Tax Rates',
		            		'class' =>'chzn-select medium',
		            		'data'=>$taxes['rates']
			            )
		           )
				),
				array(
		            'name' => 'Map Your WooCommerce Sales Tax Rates to Your QuickBooks Sales Rates. <span class="alert">All WooCommerce Tax Rates must have a unique label assigned in order for them to show up here.</span>',
		            'desc' => 'Choose whether to automatically create individual customer accounts in QuickBooks. If you choose no, you\'ll need to specify an default account to post to.',
		            'id' => 'taxrates',
		            'type' => 'mapping',
		            'options'=>array(
		            	'labels'=>array(
		            		'heading'=>'Website Sales Tax Rates',
		            		'data'=>$new_rates
		            		),
		            	'selects'=>array(
		            		'heading'=>'Quickbooks Sales Tax Rates',
		            		'class' =>'chzn-select medium',
		            		'data'=>$taxes['rates']
			            )
		           )
				)
			)
		);
		$meta_box = apply_filters('sod_qb_salestax_mapping_fields', $meta_box);
	   	sod_form_builder($meta_box);
}
/*
 * Order Status Mappings
 */

/*
 * Shipping Status Mappings
 */
function sod_quickbooks_shipping_settings(){
	global $quickbooks;
	$shipping = $quickbooks->get_accounts("Shipping");
		$meta_box = array(
		    'id' => 'sod_quickbooks_products',
		    'title' => __('Quickbooks Options', 'woocommerce'),
		    'page' => 'product',
		    'context' => 'normal',
		    'priority' => 'default',
		    'section'=>'settings',
		    'meta_key'=> 'sod_quickbooks_defaults',
		    'fields' => array(
		     array(
	            'name' => 'Which item should be used for shipping charges?',
	            'desc' => 'If not creating individual accounts, please select the default QuickBooks account to post orders to.',
	            'id' => 'shipping_item',
	            'type' => 'qbdata-select',
	            'options'=> $shipping,
	            'class' =>'chzn-select wide',
	            'refresh_link'=>array(
					'option'=>'_sod_quickbooks_shipping_items',
					'title' =>'Refresh Shipping from QB'
				),
				'add_link'=>array(
					'option'=>'_sod_quickbooks_shipping_items',
					'title' =>'Add New Shipping Item'
				)
	        ),
	   		)
	   	);
		$meta_box = apply_filters('sod_qb_shipment_mapping_fields', $meta_box);
	   	sod_form_builder($meta_box);
 }
function sod_quickbooks_classes_settings(){
	global $quickbooks;
	$classes = $quickbooks->get_accounts("Classes");
		$meta_box = array(
		    'id' => 'sod_quickbooks_products',
		    'title' => __('Quickbooks Options', 'woocommerce'),
		    'page' => 'product',
		    'context' => 'normal',
		    'priority' => 'default',
		    'section'=>'settings',
		    'meta_key'=> 'sod_quickbooks_defaults',
		    'fields' => array(
		     array(
	            'name' => 'Which class would you like ot use to group orders?',
	            'desc' => 'If not creating individual accounts, please select the default QuickBooks account to post orders to.',
	            'id' => 'class',
	            'type' => 'qbdata-select',
	            'options'=> $classes,
	            'class' =>'chzn-select wide',
	            'refresh_link'=>array(
					'option'=>'_sod_quickbooks_classes',
					'title' =>'Refresh Classes from QB'
				)
	        ),
	   		)
	   	);
		$meta_box = apply_filters('sod_qb_class_mapping_fields', $meta_box);
	   	sod_form_builder($meta_box);
 }
/*
 * Inventory Options
 */
function sod_quickbooks_inventory_settings(){
	global $quickbooks;
	$accounts = $quickbooks->get_accounts("Income");
	$cogs = $quickbooks->get_accounts("COGS");
	$identifiers = array(
		'sku'=>'Sku',
		'ID'=>'Product ID'
	);
	$qbpos_identifiers = array(
		'UPC'=>'UPC',
		'ALU'=>'ALU',
		'ItemNumber'=>'Item Number',
	); 
	$cases = array(
		'_manage_stock'=>'Inventory Items',
		'_no_manage_stock'=>'Non-Inventory Items',
		'downloadable_products'=>'Downloadable Goods',
		'virtual_product'=>'Virtual Products'
	);
	$item_types = array(
		'non_inventory'=>'Non-Inventory',
		'inventory'=>'Inventory',
		'service'=>'Service'
	);
	$fields = array(
		    'id' => 'sod_quickbooks_products',
		    'title' => __('Quickbooks Options', 'woocommerce'),
		    'page' => 'product',
		    'context' => 'normal',
		    'section' => 'inventory_settings',
		    'meta_key'=> 'sod_quickbooks_inv_defaults',
		    'fields' => array(
		    	array(
	            	'name' => 'Sync Inventory On-Hand from QuickBooks',
	            	'desc' => 'Choose whether to automatically create individual customer accounts in QuickBooks. If you choose no, you\'ll need to specify an default account to post to.',
	            	'id' => 'sync_inv',
	            	'type' => 'checkbox',
	            	'class' =>'switch'
	           ),
	           array(
	            	'name' => 'Sync Price from QuickBooks',
	            	'desc' => 'Choose whether to automatically create individual customer accounts in QuickBooks. If you choose no, you\'ll need to specify an default account to post to.',
	            	'id' => 'sync_price',
	            	'type' => 'checkbox',
	            	'class' =>'switch'
	           ),
	          array(
		            'name' => 'Do you keep track of parent products for variations in QuickBooks?',
		            'desc' => 'Choose whether to use Sales Receipts or Sales Orders.',
		            'id' => 'track_parents',
		            'type' => 'checkbox',
		            'class' =>''
		      ),
		      array(
		            'name' 	=> 'Do you keep track of variantions for variable products in QuickBooks?',
		            'desc' 	=> 'Choose whether to use Sales Receipts or Sales Orders.',
		            'id' 	=> 'track_variations',
		            'type' 	=> 'checkbox',
		            'std'	=> 'on',
		            'class' =>''
		      ),
	         array(
	            'name' => 'Unique Product Identifer Mapping for Quickbooks',
	            'desc' => 'If not creating individual accounts, please select the default QuickBooks account to post orders to.',
	            'id' => 'product_identifier',
	            'type' => 'select',
	            'options'=> $identifiers,
	            'class' =>'chzn-select wide',
	            
	        ),
	        	array(
	            'name' => 'Change WooCommerce stock status when QuickBooks quantity goes above or below zero?',
	            'desc' => 'If not creating individual accounts, please select the default QuickBooks account to post orders to.',
	            'id' => 'change_stock_status',
	            'type' => 'checkbox',
	        ), 
	       	array(
	            'name' => 'Auto-create <strong>website</strong> products in QuickBooks if they can\'t be found?',
	            'desc' => 'If not creating individual accounts, please select the default QuickBooks account to post orders to.',
	            'id' => 'auto_create',
	            'type' => 'checkbox',
	        ),
	        array(
	            	'name' => 'Are Variations Subitems of the Parent product in QB?',
	            	'desc' => 'Choose whether to automatically create individual customer accounts in QuickBooks. If you choose no, you\'ll need to specify an default account to post to.',
	            	'id' => 'create_subitems',
	            	'type' => 'checkbox',
	            	'class' =>'switch'
	           ), 
	        array(
	            'name' => 'Default QuickBooks Item Type for auto-created products?',
	            'desc' => 'If not creating individual accounts, please select the default QuickBooks account to post orders to.',
	            'id' => 'default_itemtype',
	             'type' => 'select',
	            'options'=> $item_types,
	            'class' =>'chzn-select wide',
	        ),  
	        array(
	            'name' => 'How Often Should the Inventory Sync run?',
	            'desc' => 'If not creating individual accounts, please select the default QuickBooks account to post orders to.',
	            'id' => 'inv_sync_frequency',
	            'type' => 'text',
	            'std'=>'5',
	            'suffix'=>'minutes',
	            //'options'=> $identifiers
	            'class' =>'wide',
	       	),  
			)
		);
		$fields = apply_filters('sod_qb_inventory_settings', $fields);
	   	sod_form_builder($fields);
	}

function sod_quickbooks_qwc_settings(){
	global $quickbooks;
	$options = get_option('sod_quickbooks_webconnector');
	$key = get_option('_quickbooks_connector_key');
	$dir = plugin_dir_path( __FILE__ );
	$webconnector = new SOD_Quickbooks_Data;
	?>

	<form method="post" action="<?php echo plugins_url('qwc.php', __FILE__);?>">
		<table id="qwc">
						<tr>
							<td class="label">
								<label for="sod_quickbooks_webconnector[username]">Webconnector Username?</label>
							</td>
							<td>
								<input name="sod_quickbooks_webconnector[username]" id="sod_quickbooks_webconnector[username]" value="<?php echo $options['username'];?>" type="input"/> 
							</td>
						</tr>
							<tr>
							<td class="label">
								<label for="sod_quickbooks_webconnector[password]">Webconnector Password?</label>
							</td>
							<td>
								<input name="sod_quickbooks_webconnector[password]" id="sod_quickbooks_webconnector[password]" value="<?php echo $webconnector->decode($options['password']);?>" type="password"/> 
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="sod_quickbooks_webconnector[ssl]">SSL Path</label>
							</td>
							<td>
								<input name="sod_quickbooks_webconnector[ssl]" id="sod_quickbooks_webconnector[ssl]" value="<?php echo $options['ssl'];?>" type="input"/> 
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<input type="hidden" name="sod_quickbooks_webconnector[key]" value="<?php echo $key;?>"/>
								<input type="hidden" name="sod_quickbooks_webconnector[plugin_dir]" value="<?php echo $dir;?>"/>
							</td>
						</tr>
					</table>
			<input type="submit" class="button-primary" id="create_qwc" value="<?php _e('Create QWC File') ?>" />
	</form>
<?php 
}