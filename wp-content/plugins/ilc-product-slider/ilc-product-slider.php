<?php
/**
 * Plugin Name: ILC Product Slider
 * Plugin URI: http://ilovecolors.com.ar/
 * Description: Creates a carousel to display products from WooCommerce.
 * Author: Elio Rivero
 * Author URI: http://ilovecolors.com.ar
 * Version: 1.0.0
 */

// Check if WooCommerce is active
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	
	class ILC_Product_Slider{
		
		static $version = '1.0.0';
		
		function __construct(){
			
			$basefile = plugin_dir_path(__FILE__) . '/' . basename(__FILE__);
			
			//Load localization file
			add_action('plugins_loaded', array(&$this, 'localization'));
			
			//Create Settings Page
			require_once ('ilc-admin.php');
			global $ilc_ps;
			$ilc_ps = new ILCPSAdmin( array(
				'basefile' => $basefile
			) );
			
			//Create shortcode
			require_once ('ilc-shortcode.php');
			new ILCPSShortcode(  array(
				'version' => self::$version
			));
			
		}
		
		function localization() {
			load_plugin_textdomain( 'ilc', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' ); 
		}
		
		function get_base_file(){
			return self::$basefile;
		}
	}
	
	// Initialize product slider
	new ILC_Product_Slider();
	
	// Include debugging utility.
	//require_once ('ilc-utils.php');
	
} else {
	add_action( 'admin_notices', create_function('', "echo '<div class=\"update-nag fade-out\">You need to have <strong>WooCommerce installed and activated</strong> for Product Slider to work.</div>';") );
}


?>