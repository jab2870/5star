<?php
ob_start();

Header('Cache-Control: no-cache');
Header('Pragma: no-cache');
require_once('../../../../wp-load.php');
$content = ob_get_contents();
ob_end_clean();

global $wpdb,$cgm_isotope_generator;

function send_error_die($msg){
	die(json_encode(array('R'=>'ERR','MSG'=>$msg)));
}

if(!isset($_POST['post_id'])){
	send_error_die(__('No post id are received','evt'));
}

if(empty($_POST['tmp_id'])){
	$_POST['tmp_id'] = 0;
}


$cgm_gallery_data = get_post_meta($_POST['post_id'], "cgm-gallery-data",true);	
$cgm_settings = get_post_meta($_POST['post_id'], "cgm_settings",true);

$return_data = $cgm_isotope_generator->load_images_gallery_new($_POST['tmp_id'],$cgm_gallery_data,$cgm_settings,$_POST['count']);

$response = array(
    'R'	=> 'OK',
    'DATA' => $return_data[1],
    'MOREDATA' => $return_data[0]
);

die(json_encode($response));
?>