<?php
ob_start();

Header('Cache-Control: no-cache');
Header('Pragma: no-cache');
require_once('../../../../wp-load.php');
$content = ob_get_contents();
ob_end_clean();

global $complete_gallery_manager_plugin;

function send_error_die($msg){
	die(json_encode(array('R'=>'ERR','MSG'=>$msg)));
}

if(!is_user_logged_in()){
	send_error_die(__('You are not logged in.','act'));
}

if(!isset($_POST['post_id']) && $_POST['post_id']>0){
	send_error_die(__('No post id pleas contact support.','act'));
}

$comments = get_post_meta($_POST['post_id'], 'cgm_comments', true);


if(empty($comments)){
	$comments = __('No comments','act');
}

$preview = '';

if ( has_post_thumbnail($_POST['post_id'],'thumbnail')) {
	$preview = wp_get_attachment_image_src( get_post_thumbnail_id( $_POST['post_id'] ), 'medium'  );
} else {
	$preview[0] = COMPLETE_GALLERY_URL.'images/no_photo.jpg';
}



$response = array(
    'R'	=> 'OK',
    'SHORTCODE' => $complete_gallery_manager_plugin->globalt_shorcode_generator(array('id'=>$_POST['post_id'])),
    'COMMENTS' =>$comments,
    'PREVIEW' => $preview
);

die(json_encode($response));
?>