<?php
ob_start();

Header('Cache-Control: no-cache');
Header('Pragma: no-cache');
require_once('../../../../wp-load.php');
require_once(COMPLETE_GALLERY_PATH.'inc/class_upload.php');
$content = ob_get_contents();
ob_end_clean();
  
$allowedExtensions = array();

$uploader = new cgm_qqFileUploader($allowedExtensions);
$result = $uploader->handleUpload();
echo json_encode($result);	 
?>