<?php
ob_start();

Header('Cache-Control: no-cache');
Header('Pragma: no-cache');
require_once('../../../../wp-load.php');
$content = ob_get_contents();
ob_end_clean();

global $complete_gallery_manager_plugin;
$create_scales = $complete_gallery_manager_plugin->cgm_get_image_scalse();

$args = array(
    'numberposts'     => $_POST['cgm_tooles_diff'],
    'offset'          => $_POST['cgm_tooles_current'],
    'orderby'         => $_POST['cgm_tooles_sort'],
    'order'           => 'DESC',
    'post_type'       => 'attachment');

$posts_array = get_posts( $args );

$content = '';

foreach($posts_array as $post_array){
	$content .= '<div title="'.$post_array->post_name.'" alt="'.$post_array->post_name.'" onClick="cgm_activate_click(this);" class="element imagesize'.$_POST['cgm_tooles_size'].'" data-ID="'.$post_array->ID.'"';
 	$image=wp_get_attachment_image_src($post_array->ID);
	$content .= ' style="background-image:url(\''.$image[0].'\')">';	
 	$content .= '</div>';	
}

if(count($posts_array)>0){
	$response = array(
	    'R'	=> 'OK',
	    'DATA' => $content,
	    'MSG' => 'New Images ('.count($posts_array).' Items)',
	    'CURRENT' => ($_POST['cgm_tooles_current']+count($posts_array))
	);
} else {
	$response = array(
	    'R'	=> 'NODATA',
	    'MSG' => 'No More Images',
	    'CURRENT' => $_POST['cgm_tooles_current']
	);
}

die(json_encode($response));
?>







