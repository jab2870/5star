<?php
/**
Plugin Name: Complete Gallery Manager for Wordpress
Plugin URI: http://plugins.righthere.com/complete-gallery-manager/
Description: Complete Gallery Manager for WordPress is an exquisite jQuery plugin for creating magical galleries with Images, Posts, Pages and Videos. This is an incredible versatile plugin, which lets you create amazing looking galleries very easily. From simple fully responsive galleries or with infinite scroll, with this plugin it is very easy.
Version: 2.0.3 rev28734
Author: Rasmus R. Sorensen (RightHere LLC)
Author URI: http://plugins.righthere.com
**/


define("CGM_VERSION",'2.0.3');

define("COMPLETE_GALLERY_PATH", ABSPATH . 'wp-content/plugins/' . basename(dirname(__FILE__)).'/' ); 
define("COMPLETE_GALLERY_URL", trailingslashit(get_option('siteurl')) . 'wp-content/plugins/' . basename(dirname(__FILE__)) . '/' ); 

global $complete_gallery_display;
	   $complete_gallery_display = '';

require_once COMPLETE_GALLERY_PATH.'inc/isotope_generator.php';
require_once(COMPLETE_GALLERY_PATH.'inc/install.php'); 

class complete_gallery_manager_plugin { 
	var $id = 'cgm';
	var $plugin_page; 
	var $menu_name;
	var $gallery_count_id = 1;
	var $gallery_script_load = array();
	var $gallery_css_load = array();
	var $short_code_name = 'complete_gallery';

	function complete_gallery_manager_plugin(){
		$this->menu_name = __('CGM Settings','cgm');
		require_once(COMPLETE_GALLERY_PATH.'inc/isotope_settings.php');

		add_action("plugins_loaded", array(&$this,"plugins_loaded") );	
		add_action("admin_menu", array(&$this,"admin_menu") );
		
		if(is_admin()){
			require_once COMPLETE_GALLERY_PATH.'options-panel/load.pop.php';
			rh_register_php('options-panel',COMPLETE_GALLERY_PATH.'options-panel/class.PluginOptionsPanelModule.php', '2.0.2');
		}
	}
	
	function admin_menu(){
		if(is_admin() && (current_user_can('manage_options') || current_user_can('cgm_create_gallery'))){         
            add_menu_page( $this->menu_name, 
            			   $this->menu_name, 
            			   'cgm_create_gallery', 
            			   ($this->id.'-start'), 
            			   array(&$this,'get_started_options'), 
            			   COMPLETE_GALLERY_URL.'images/cgm.png' );
            			   
            $this->plugin_page = add_submenu_page(($this->id.'-start'),
            									  __("Get Started",'cgm'), 
            									  __("Get Started",'cgm'), 
            									  'cgm_create_gallery',
            									  ($this->id.'-start'), 
            									  array(&$this,'get_started_options') );
            
            
            add_action( 'admin_head-'. $this->plugin_page, 
            		    array(&$this,'get_started_options_head') );
            		    
        	do_action(($this->id.'-options-menu'));
        	
        	
			if(current_user_can('manage_options') || current_user_can('cgm_insert_gallery')){         	
            	require_once COMPLETE_GALLERY_PATH.'admin/TinyMCE-extra-button.php';
            	new cgm_tinymce_extra_button();
            }
		}
	}
	
	
    function plugins_loaded(){
    
        $check_install = get_option('cgm_version');
    	if($check_install < CGM_VERSION){
    		cgm_install_capabilities();
    	} 
    	

    	add_action('wp_head', array(&$this,'wpheader'));
    
        $this->create_sub_menu();
        add_shortcode($this->short_code_name, array(&$this,'do_shortcode'));
    }	
    
    
	function wpheader(){
		echo '<link rel="stylesheet" type="text/css" href="'.COMPLETE_GALLERY_URL.'css/prettyPhoto.css" />';
		echo '<link rel="stylesheet" type="text/css" href="" />';
	}
    
	function create_sub_menu(){ 	
		global $cgm_admin_post_list;
		require_once COMPLETE_GALLERY_PATH.'admin/admin_post_list.php';		 	
		$cgm_admin_post_list = new cgm_admin_post_list($this->id);
		
		if(is_admin() && (current_user_can('cgm_create_gallery') || current_user_can('manage_options'))){

			$settings = array(
				'id'					=> $this->id.'-opt',
				'plugin_id'				=> $this->id,
				'menu_id'				=> $this->id.'-opt',
				'capability'			=> 'manage_options',
				'options_varname'		=> 'cgm_options',
				'page_title'			=> __('Options','cgm'),
				'menu_text'				=> __('Options','cgm'),
				'option_menu_parent'	=> ($this->id.'-start'),
				'options_panel_version'	=> '2.0.3',
				'notification'			=> (object)array(
					'plugin_version'=>  CGM_VERSION,
					'plugin_code' 	=> 'CGM',
					'message'		=> __('Complete gallery manager update %s is available!','cgm').' <a href="%s">'.__('Please update now','cgm')
				),
				'registration' 		=> true,
				'theme'					=> false,
				'import_export'  		=> false,
				'import_export_options' => false
				);	
			
			do_action('rh-php-commons');	
			//no longer loaded like this, instead the do_action takes care of loading the one from the rh plugin with the latest version of pop.
			//require_once COMPLETE_GALLERY_PATH.'options-panel/class.PluginOptionsPanelModule.php';	
		 					
			new PluginOptionsPanelModule($settings);
		 	
            require_once COMPLETE_GALLERY_PATH.'admin/option.panel.php';
            new cgm_options($this->id);
            
            
		}
	}	
	
	
	function do_shortcode($atts) {
		global $complete_gallery_display;

		$post_id = '';
		
		if(!empty($atts['id'])){
			$post_id = $atts['id'];
		}

		
		$return_content = '';
		
		if(!empty($post_id)){
		
			$cgm_flag = get_post_meta($post_id, "cgm_flag",true);	
			$cgm_settings = get_post_meta($post_id, "cgm_settings",true);
		
			$cgm_width  = get_post_meta($post_id, "cgm_width",true);	
			$cgm_height = get_post_meta($post_id, "cgm_height",true);	


			$return_content .= '<div id="completegallery'.$this->gallery_count_id.'" ';
			$return_content .='style="overflow:auto;';
			
			if(!empty($atts['style'])){
				$return_content .= $atts['style'];
			}
			
			if(!empty($cgm_width) and $cgm_width > 0){
				$return_content .= ' width:'.$cgm_width.'px ';
			}
			
			if(!empty($cgm_height) and $cgm_height > 0){
				$return_content .= ' height:'.$cgm_height.'px ';
			}
									
			$return_content .='"class="completegallery ';
			if(!empty($atts['class'])){
				$return_content .= $atts['class'].' ';
			}
			$return_content .='"';
			
			$return_content .= '>';
			$return_content .= '<div class="loading" style="background-image: url(\''.COMPLETE_GALLERY_URL.'/images/loader.gif\');background-repeat: no-repeat;float: left;height: 30px;line-height: 17px;margin-bottom: 20px;padding-left: 25px;">Loading Gallery</div>';
			if(!empty($cgm_flag)) {		
	   			foreach($complete_gallery_display[$cgm_flag]['class_js'] as $tmp_js_key =>  $tmp_js ){ 
					if(empty($this->gallery_script_load[$tmp_js_key])){
		   				if(empty($tmp_js)){
		   					wp_enqueue_script( $tmp_js_key ); 
		   				} else {
		   					if(substr($tmp_js_key, 0, 1) == '*'){
		   						$return_content .= '<script src="'.$tmp_js.'" type="text/javascript"></script>';
		   					} else {
		    					wp_register_script( $tmp_js_key, $tmp_js, array(), '1.0',false);
		    					wp_enqueue_script( $tmp_js_key ); 
		    				}
		   				}
		   				$this->gallery_script_load[$tmp_js_key] = true;
		   			}
	  			 }
	  			 
	   			foreach($complete_gallery_display[$cgm_flag]['class_css'] as $tmp_css_key =>  $tmp_css ){ 
					if(empty($this->gallery_css_load[$tmp_css_key])){
	    				wp_register_style( $tmp_css_key, $tmp_css);
	    				wp_enqueue_style( $tmp_css_key ); 
	    				$this->gallery_css_load[$tmp_css_key] = true;		
	    			}	
	  			}
	  			 
			
				if(!empty($cgm_settings) and !empty($cgm_flag) and !empty($this->gallery_count_id) ){
					if(!empty($complete_gallery_display[$cgm_flag]['call_js_func'])){
					
						$return_content .= '<script>';
						$return_content .= 'function cgm_atload'.($this->gallery_count_id).'(){';
						$return_content .= $complete_gallery_display[$cgm_flag]['call_js_func'].'('.$this->gallery_count_id.',\''.$cgm_settings.'\','.$post_id.',\''.COMPLETE_GALLERY_URL.'\');';
						$return_content .=		'if(typeof (window.cgm_atload'.($this->gallery_count_id+1).') == \'function\') {';
						$return_content .=			'cgm_atload'.($this->gallery_count_id+1).'();';
						$return_content .=		'}';
						$return_content .=	'}</script>';
					}
				}
			}
		}
		
		$return_content .= '</div>';
		$return_content .= '<div class="clear" style="clear:both;"></div>';
		if($this->gallery_count_id == 1){
			$return_content .= '<script>';
			$return_content .= 'window.onload=cgm_atload;';
			$return_content .=	'function cgm_atload(){';
			$return_content .=		'if(typeof (window.cgm_atload1) == \'function\') {';
			$return_content .=			'cgm_atload1();';
			$return_content .=		'}';
			$return_content .=	'}';


			$return_content .='</script>';
		}
	
	
		$this->gallery_count_id += 1;
		//echo $return_content;
     	return $return_content;
	}
	
	function globalt_shorcode_generator($array_datas){
		$return_content = '['.$this->short_code_name.' ';
		
		if(!empty($array_datas)){
			foreach($array_datas as $key => $array_data){
				$return_content .= $key . '="'.$array_data.'" ';			
			}
		}
		
		$return_content .= ']';
		
		return $return_content;
	}
	
	
    function get_started_options_head(){
     	wp_register_style( 'cgm_get_started', COMPLETE_GALLERY_URL.'css/get_started.css');
		wp_enqueue_style( 'cgm_get_started');
    } 	
	
	function get_started_options(){
		include_once(COMPLETE_GALLERY_PATH.'admin/getstarted.php');
    }
    
	function cgm_get_image_scalse(){
		global $_wp_additional_image_sizes;
		
		$image_sizes = '';
		
		$image_sizes['thumbnail']['width'] = get_option('thumbnail_size_w');
		$image_sizes['thumbnail']['height'] = get_option('thumbnail_size_h');
		$image_sizes['thumbnail']['crop'] = get_option('thumbnail_crop');  
	    
		$image_sizes['medium']['width'] = get_option('medium_size_w');
		$image_sizes['medium']['height'] = get_option('medium_size_h');
		$image_sizes['medium']['crop'] = get_option('medium_crop');     
	    
		$image_sizes['large']['width'] = get_option('large_size_w');
		$image_sizes['large']['height'] = get_option('large_size_h');
		$image_sizes['large']['crop'] = get_option('large_crop');     
	        
		return $image_sizes;
	}	
}
$complete_gallery_manager_plugin = new complete_gallery_manager_plugin();