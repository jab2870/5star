var cgm_current_version = 0;
var cgm_count_down = 0;

function cgm_drawIsoTope_gallery(tmp_id,tmp_settings,tmp_post_id,tmp_COMPLETE_GALLERY_URL,debug){
	if(debug){
		cgm_drawIsoTope_gallery_v2(tmp_id,tmp_settings,tmp_post_id,tmp_COMPLETE_GALLERY_URL);
	} else {
		jQuery(document).ready(function($){
			var _url = tmp_COMPLETE_GALLERY_URL+'frames/frame.get_data.php';	
			jQuery.post(_url,{gallery_count_id:tmp_id,post_id:tmp_post_id},function(data){
				if(data.R == 'OK'){
					
					jQuery('#completegallery'+tmp_id).fadeTo('slow',0,function(){
						jQuery('#completegallery'+tmp_id).html(data.RETURN_DATA);
						
						cgm_loadcss(data.CSS, "css");
						
						setTimeout(function() {
							cgm_drawIsoTope_gallery_v2(tmp_id,tmp_settings,tmp_post_id,tmp_COMPLETE_GALLERY_URL);	
						},100);
					});
				}
			},'json');
		});
	}


}

function cgm_loadcss(filename, filetype) {
	cgm_replacejscssfile(filename+'?ver='+cgm_current_version, filename+'?ver='+(cgm_current_version+1), filetype)
}



function cgm_createjscssfile(filename, filetype){
	if (filetype=="js"){ //if filename is a external JavaScript file
		var fileref=document.createElement('script')
		fileref.setAttribute("type","text/javascript")
		fileref.setAttribute("src", filename)
	}
	else if (filetype=="css"){ //if filename is an external CSS file
		var fileref=document.createElement("link")
		fileref.setAttribute("rel", "stylesheet")
		fileref.setAttribute("type", "text/css")
		fileref.setAttribute("href", filename)
	}
	return fileref
}

function cgm_replacejscssfile(oldfilename, newfilename, filetype){
	var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none"
	var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" 
	var create_type = true; 
	
	var allsuspects=document.getElementsByTagName(targetelement)
	for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
		if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(oldfilename)!=-1){
			var newelement=cgm_createjscssfile(newfilename, filetype)
			allsuspects[i].parentNode.replaceChild(newelement, allsuspects[i]);
			create_type = false;
		}
	}

	if(create_type){
		var newelement=cgm_createjscssfile(oldfilename, filetype);
		if (typeof newelement!="undefined"){
			if(navigator.appName == 'Microsoft Internet Explorer'){

				if(cgm_count_down == 0){
					cgm_count_down = 10;
				}

				for (var j=cgm_count_down; j>=0; j--){
					if (allsuspects[j] && allsuspects[j].getAttribute('rel')!=null && allsuspects[j].getAttribute('rel') == 'stylesheet' ){
						cgm_count_down = j;
						break;
					}
				}
				allsuspects[0].parentNode.replaceChild(newelement, allsuspects[cgm_count_down]);
			} else {
				if (typeof newelement!="undefined"){
					document.getElementsByTagName("head")[0].appendChild(newelement);
				}  
			}
		}  
	} else {
		cgm_current_version++;
	}
}


function cgm_activate_link(tmp_this){
	jQuery(tmp_this).parent().find('a').click();
}


function cgm_drawIsoTope_gallery_v2(tmp_id,tmp_settings,tmp_post_id,tmp_COMPLETE_GALLERY_URL){
	jQuery(document).ready(function($){
		if(typeof(tmp_settings)=='string'){
			tmp_settings = JSON.parse(decodeURI(tmp_settings));

			if(tmp_settings.cgm_mouseEventClick == 'prettyPhoto'){
			
				var cgm_tmp_pretty_photo = new Object();
				
				if(typeof(tmp_settings.cgm_pretty.theme)=='string'){
					cgm_tmp_pretty_photo.theme = tmp_settings.cgm_pretty.theme;
				}
				
				if(typeof(tmp_settings.cgm_pretty.thumbnails) =='boolean' && tmp_settings.cgm_pretty.thumbnails == false){
					cgm_tmp_pretty_photo.gallery_markup = ' ';
				}
				
				if(typeof(tmp_settings.cgm_pretty.thumbnails) =='string' && tmp_settings.cgm_pretty.thumbnails == 'false'){
					cgm_tmp_pretty_photo.gallery_markup = ' ';
				}

				cgm_tmp_pretty_photo.show_title = tmp_settings.cgm_pretty.showtitle;	
				cgm_tmp_pretty_photo.autoplay_slideshow = tmp_settings.cgm_pretty.autoplayslideshow;
				cgm_tmp_pretty_photo.deeplinking = tmp_settings.cgm_pretty.deeplinking;			
			
				if(typeof(tmp_settings.cgm_pretty.opacity)=='number'){
					cgm_tmp_pretty_photo.opacity = tmp_settings.cgm_pretty.opacity;
				}	
			
				if(typeof(tmp_settings.cgm_pretty.slideshow)=='number'){
					cgm_tmp_pretty_photo.slideshow = tmp_settings.cgm_pretty.slideshow;
				}
				
				if(typeof(tmp_settings.cgm_pretty.animationspeed)=='string'){
					cgm_tmp_pretty_photo.animation_speed = tmp_settings.cgm_pretty.animationspeed;
				}	

				jQuery("#cgm_isotype_bg_"+tmp_id+" a[rel^='prettyPhoto']").prettyPhoto(cgm_tmp_pretty_photo);
			}

			var cgm_tmp_isotope = new Object();
			
			cgm_tmp_isotope.itemSelector = '.cgm_items';
			cgm_tmp_isotope.layoutMode = tmp_settings.cgm_layout.default;
			cgm_tmp_isotope.sortBy = tmp_settings.cgm_sort.default;			

			cgm_tmp_isotope.getSortData = {
						index : function( $elem ) {
							return parseInt( $elem.find('.index').text());
						},
						title : function( $elem ) {
							return $elem.find('.title').text();
						},
						desc : function( $elem ) {
							return $elem.find('.desc').text();
						},
						tag : function( $elem ) {
							return $elem.find('.tag').text();
						},
						imageSize : function ( $elem ) {
							return parseFloat( $elem.find('.size').text());
						}
					};	
					
			if(typeof(tmp_settings.cgm_animation.type)=='string'){
				cgm_tmp_isotope.animationEngine = tmp_settings.cgm_animation.type;	
			}

			if(typeof(tmp_settings.cgm_animation.duration)=='number'){
				cgm_tmp_isotope.animationOptions = {
     				duration: tmp_settings.cgm_animation.duration,
     				easing: tmp_settings.cgm_animation.easing,
     				queue: false
   				}
			}
			
			
			if(tmp_settings.cgm_layout.default == 'masonry' || tmp_settings.cgm_layout.default == 'masonryHorizontal' || tmp_settings.cgm_layout.default == 'cellsByRow' || tmp_settings.cgm_layout.default == 'cellsByColumn'){
				var cgm_height = 0;
				var cgm_width = 0;
				var cgm_lr = 0;
				var cgm_hb = 0;			
				jQuery('#cgm_isotype_bg_'+tmp_id).find('.cgm_items').each(function(){
				
					var cgm_lr = parseInt(jQuery(this).css("padding-left"))+parseInt(jQuery(this).css("padding-right"));
					var cgm_hb = parseInt(jQuery(this).css("padding-bottom"))+parseInt(jQuery(this).css("padding-top"));	
				
				
					if(cgm_width < (jQuery(this).width()+cgm_lr)){
						cgm_width = (jQuery(this).width()+cgm_lr);
					}
					
	
					if(cgm_height < (jQuery(this).height()+cgm_hb)){
						cgm_height = (jQuery(this).height()+cgm_hb);
					}		
				}); 
				
				
				cgm_tmp_isotope.masonry = {columnWidth : 1};
				cgm_tmp_isotope.masonryHorizontal = {rowHeight:1};
				cgm_tmp_isotope.cellsByRow = {columnWidth : cgm_width,rowHeight : cgm_height};
				cgm_tmp_isotope.cellsByColumn = {columnWidth : cgm_width,rowHeight : cgm_height};
				
			}

 				//jQuery('#cgm_isotype_bg_'+tmp_id).isotope(cgm_tmp_isotope);	
			
			jQuery('#completegallery'+tmp_id).css('opacity',1);
			jQuery('#completegallery'+tmp_id).fadeTo(0,1);			
			jQuery('#completegallery'+tmp_id).show();	
			jQuery('#cgm_isotype_bg_'+tmp_id).isotope(cgm_tmp_isotope);	

			if(typeof(tmp_settings.cgm_universallScroll.loadNumber)=='number' && tmp_settings.cgm_universallScroll.loadNumber>0 && tmp_settings.cgm_universallScroll.loadNumber != null){
				
				var cgm_setscollepos = jQuery('#cgm_isotype_bg_'+tmp_id).parent();
				if(typeof(tmp_settings.cgm_height) == 'undefined'  || (typeof(tmp_settings.cgm_height) == 'number' && tmp_settings.cgm_height == 0)){
					 cgm_setscollepos = jQuery(window);
				} else {
					jQuery(cgm_setscollepos).scrollTop(0);
				}
				
				jQuery(cgm_setscollepos).scroll(function () {
				
				var cgm_universal_scroll_max = 0;
				
				if(typeof(tmp_settings.cgm_height) == 'undefined' || (typeof(tmp_settings.cgm_height) == 'number' && tmp_settings.cgm_height == 0)){
					var cgm_top_universal =  jQuery('#cgm_isotype_bg_'+tmp_id).parent().offset();
					cgm_top_universal = Math.round(cgm_top_universal.top);
					cgm_universal_scroll_max = jQuery('#cgm_isotype_bg_'+tmp_id).parent().height() + cgm_top_universal - jQuery(this).height()+100;
				} else {
					cgm_universal_scroll_max = jQuery(this)[0].scrollHeight - jQuery(this).height();
				}
				
					var tmp_universal = jQuery('#cgm_isotype_bg_'+tmp_id + ' .universall_scroll span').html();

					if(jQuery(this).scrollTop() > cgm_universal_scroll_max-50 && tmp_universal == 'true'){
						jQuery('#cgm_isotype_bg_'+tmp_id + ' .universall_scroll span').html('false');

						jQuery('#cgm_isotype_bg_'+tmp_id + ' .universall_scroll div').html('Loading');
						jQuery('#cgm_isotype_bg_'+tmp_id + ' .universall_scroll').fadeIn(500, function() {

							jQuery.post(tmp_COMPLETE_GALLERY_URL+'frames/select_next_images.php',{'steps':tmp_settings.cgm_universallScroll.loadNumber,'post_id':tmp_post_id,count:(jQuery('#cgm_isotype_bg_'+tmp_id).children("div").length-1),'tmp_id':tmp_id},function(data){
								if(data.R == 'OK'){
									jQuery('#cgm_isotype_bg_'+tmp_id).isotope( 'insert', jQuery(data.DATA) );
									jQuery("#cgm_isotype_bg_"+tmp_id+" a[rel^='prettyPhoto']").prettyPhoto(cgm_tmp_pretty_photo);
								}
									
								jQuery('#cgm_isotype_bg_'+tmp_id + ' .universall_scroll').fadeOut(500, function() {
									if(data.DATA == ''){
										jQuery('#cgm_isotype_bg_'+tmp_id + ' .universall_scroll div').html('No more pictures');
										jQuery('#cgm_isotype_bg_'+tmp_id + ' .universall_scroll').fadeIn(500, function() {	
											jQuery('#cgm_isotype_bg_'+tmp_id + ' .universall_scroll').delay(1000).fadeOut(500, function() {
												jQuery(this).hide();
											});	
										})	
									} else {
										setTimeout(function(){
											jQuery('#cgm_isotype_bg_'+tmp_id + ' .universall_scroll span').html('true');
										}, 500 );
									}
								});
							},'json');
						})
					}
				});
			}
		}	
			
	});
}


function cgm_sort_order_system(tmp_id,tmp_action,tmp_this){
	var cgm_tmp_isotope = new Object();

	cgm_tmp_isotope.sortAscending = tmp_action;
	
	jQuery(document).ready(function($){
		jQuery('#cgm_isotype_menu_'+tmp_id+' .cgm_sort_order a').removeClass('selected');
		jQuery(tmp_this).addClass('selected');
	
		jQuery('#cgm_isotype_bg_'+tmp_id).isotope(cgm_tmp_isotope);
	});	
	
}

function cgm_sort_system(tmp_id,tmp_action,tmp_this){
	var cgm_tmp_isotope = new Object();

	cgm_tmp_isotope.sortBy = tmp_action;
	
	jQuery(document).ready(function($){
		jQuery('#cgm_isotype_menu_'+tmp_id+' .cgm_sort a').removeClass('selected');
		jQuery(tmp_this).addClass('selected');
	
		jQuery('#cgm_isotype_bg_'+tmp_id).isotope(cgm_tmp_isotope);
	});	
	
}

function cgm_filter_system(tmp_id,tmp_action,tmp_this){
	var cgm_tmp_isotope = new Object();
	if(tmp_action != '*' && tmp_action != ''){
		tmp_action = '.'+tmp_action
	}
	cgm_tmp_isotope.filter = tmp_action;
	
	jQuery(document).ready(function($){
		jQuery('#cgm_isotype_menu_'+tmp_id+' .cgm_filter a').removeClass('selected');
		jQuery(tmp_this).addClass('selected');
	
		jQuery('#cgm_isotype_bg_'+tmp_id).isotope(cgm_tmp_isotope);
	});	
	
	setTimeout(function(){
		jQuery('#cgm_isotype_bg_'+tmp_id+' .cgm_items').each(function(){
			if(jQuery(this).css('opacity') != 0){
				jQuery(this).find('a').attr('rel','prettyPhoto[pp_gal]');
			} else {
				jQuery(this).find('a').attr('rel','');
			}
		})
	},1000);
}

function cgm_changeImages(tmp_id,tmp_this,tmp_action){
	jQuery(document).ready(function($){
		var defaultdiv = jQuery(tmp_this).children('.default_image').html();
		
		if(defaultdiv=='thumbnail'){
			defaultdiv = 0;
		} else if(defaultdiv=='medium'){
			defaultdiv = 1;
		} else if(defaultdiv=='large'){
			defaultdiv = 2;
		} else if(defaultdiv=='full'){
			defaultdiv = 3;
		} else if(defaultdiv=='custom'){
			defaultdiv = 4;
		}
		
		var lastdiv = jQuery(tmp_this).children('.imageSize').children('div:last-child').index();
		var current = jQuery(tmp_this).children('.imageSize').find('.current').index();
		var newselect = 0;
		
		jQuery('#cgm_isotype_bg_'+tmp_id).isotope()
		jQuery(tmp_this).children('.imageSize').children('div:eq('+current+')').removeClass('current');

		if(tmp_action == 'exAll'){
			if(current == lastdiv){
				newselect = 0;
			} else {
				newselect = current + 1;
				if(current > 2){
					newselect = lastdiv;
				}
			}
		} else if(tmp_action == 'ex0'){
			if(current == defaultdiv){
				newselect = 0;
			} else {
				newselect = defaultdiv;
			}
		} else if(tmp_action == 'ex1'){
			if(current == defaultdiv){
				newselect = 1;
			} else {
				newselect = defaultdiv;
			}
		} else if(tmp_action == 'ex2'){
			if(current == defaultdiv){
				newselect = 2;
			} else {
				newselect = defaultdiv;
			}
		} else if(tmp_action == 'ex3'){
			if(current == defaultdiv){
				newselect = 3;
			} else {
				newselect = defaultdiv;
			}
		} else if(tmp_action == 'ex4'){
			if(current == defaultdiv){
				newselect = lastdiv;
			} else {
				newselect = defaultdiv;
			}
		}
		

		
		jQuery(tmp_this).children('.imageSize').children('div:eq('+newselect+')').addClass('current');	
		
		var newHeight = jQuery(tmp_this).children('.imageSize').children('div:eq('+newselect+')').find('.height').html();
		var newWidth = jQuery(tmp_this).children('.imageSize').children('div:eq('+newselect+')').find('.width').html();		
		var newUrl = jQuery(tmp_this).children('.imageSize').children('div:eq('+newselect+')').find('.url').html(); 	

		if(jQuery(tmp_this).parent().attr('id') == 'cgm_items'){
			//jQuery(tmp_this).parent().width(newWidth).height(newHeight);
			jQuery(tmp_this).parent().css({ width: newWidth,height:newHeight });

		} else if(jQuery(tmp_this).parent().parent().attr('id') =='cgm_items'){
			//jQuery(tmp_this).parent().parent().width(newWidth).height(newHeight);
			jQuery(tmp_this).parent().parent().css({ width: newWidth,height:newHeight });
		}	
		
		
		jQuery(tmp_this).children('.size').text(parseFloat(newWidth*newHeight));
		jQuery(tmp_this).children('img:first-child').attr('src',newUrl).css({ width: newWidth,height:newHeight });
	
		if(jQuery(tmp_this).parent().attr('id') == 'cgm_items'){
			jQuery('#cgm_isotype_bg_'+tmp_id).isotope('updateSortData',jQuery(tmp_this).parent());
		} else if(jQuery(tmp_this).parent().parent().attr('id') =='cgm_items'){
			jQuery('#cgm_isotype_bg_'+tmp_id).isotope('updateSortData',jQuery(tmp_this).parent().parent());
		}	
		
		var cgm_tmp_isotope = new Object();
		var cgm_height = 0;
		var cgm_width = 0;
		var cgm_lr = 0;
		var cgm_hb = 0;			
		jQuery('#cgm_isotype_bg_'+tmp_id).find('.cgm_items').each(function(){
			var cgm_lr = parseInt(jQuery(this).css("padding-left"))+parseInt(jQuery(this).css("padding-right"));
			var cgm_hb = parseInt(jQuery(this).css("padding-bottom"))+parseInt(jQuery(this).css("padding-top"));	
			
			if(cgm_width < (jQuery(this).width()+cgm_lr)){
				cgm_width = (jQuery(this).width()+cgm_lr);
			}
					
	
			if(cgm_height < (jQuery(this).height()+cgm_hb)){
				cgm_height = (jQuery(this).height()+cgm_hb);
			}		
		}); 

		cgm_tmp_isotope.masonry = {columnWidth : 1};
		cgm_tmp_isotope.masonryHorizontal = {rowHeight:1};
		cgm_tmp_isotope.cellsByRow = {columnWidth : cgm_width,rowHeight : cgm_height};
		cgm_tmp_isotope.cellsByColumn = {columnWidth : cgm_width,rowHeight : cgm_height};
				
				jQuery('#cgm_isotype_bg_'+tmp_id).isotope(cgm_tmp_isotope);
			setTimeout(function(){
				jQuery('#cgm_isotype_bg_'+tmp_id).isotope(cgm_tmp_isotope);
			}, 100 )

		setTimeout(function(){
				jQuery('#cgm_isotype_bg_'+tmp_id).isotope(cgm_tmp_isotope);
			}, 500 )

		setTimeout(function(){
				jQuery('#cgm_isotype_bg_'+tmp_id).isotope(cgm_tmp_isotope);
			}, 1000 )

		setTimeout(function(){
				jQuery('#cgm_isotype_bg_'+tmp_id).isotope(cgm_tmp_isotope);
			}, 1500 )
	});
}




function cgm_layout_system(tmp_id,tmp_action,tmp_width,tmp_height,tmp_this){
	var cgm_tmp_isotope = new Object();
	cgm_tmp_isotope.layoutMode = tmp_action;

	jQuery(document).ready(function($){
		var last_Str = jQuery('#cgm_isotype_menu_'+tmp_id+' .cgm_layout .selected').attr('title');
		var last_bool = false;
		var current_bool = false;
		if(last_Str== 'straightAcross' || last_Str== 'masonryHorizontal' || last_Str== 'cellsByColumn' || last_Str== 'fitColumns'){
			last_bool = true;
		}

		if(tmp_action== 'straightAcross' || tmp_action== 'masonryHorizontal' || tmp_action== 'cellsByColumn' || tmp_action== 'fitColumns'){
			current_bool = true;
		}	
		
		if(tmp_height == '100%'){
			tmp_height = '80%';
		}

		if(tmp_action == 'masonry' || tmp_action == 'masonryHorizontal' || tmp_action == 'cellsByRow' || tmp_action == 'cellsByColumn'){
			var cgm_height = 0;
			var cgm_width = 0;
			var cgm_lr = 0;
			var cgm_hb = 0;			
			jQuery('#cgm_isotype_bg_'+tmp_id).find('.cgm_items').each(function(){
			
				var cgm_lr = parseInt(jQuery(this).css("padding-left"))+parseInt(jQuery(this).css("padding-right"));
				var cgm_hb = parseInt(jQuery(this).css("padding-bottom"))+parseInt(jQuery(this).css("padding-top"));	
			
				if(cgm_width < (jQuery(this).width()+cgm_lr)){
					cgm_width = (jQuery(this).width()+cgm_lr);
				}

				if(cgm_height < (jQuery(this).height()+cgm_hb)){
					cgm_height = (jQuery(this).height()+cgm_hb);
				}		
			}); 
			
			cgm_tmp_isotope.masonry = {columnWidth : 1};
			cgm_tmp_isotope.masonryHorizontal = {rowHeight:1};
			cgm_tmp_isotope.cellsByRow = {columnWidth : cgm_width,rowHeight : cgm_height};
			cgm_tmp_isotope.cellsByColumn = {columnWidth : cgm_width,rowHeight : cgm_height};
			
		}
			
			
		jQuery('#cgm_isotype_menu_'+tmp_id+' .cgm_layout a').removeClass('selected');
		jQuery(tmp_this).addClass('selected');
			
		if (last_bool !== current_bool) {

				var style = current_bool ? { height: '80%', width: jQuery('#cgm_isotype_bg_'+tmp_id).width() } : { width: 'auto',height:'auto'};
			jQuery('#cgm_isotype_bg_'+tmp_id).addClass('no-transition').css( style );
			
			setTimeout(function(){
				jQuery('#cgm_isotype_bg_'+tmp_id).removeClass('no-transition').isotope( cgm_tmp_isotope );
			}, 100 )
			
		} else {
			jQuery('#cgm_isotype_bg_'+tmp_id).isotope(cgm_tmp_isotope);
		}
	});	
			
	 
}