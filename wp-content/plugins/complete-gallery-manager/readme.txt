=== Complete Gallery Manager for WordPress ===
Author: RightHere LLC (Rasmus S. Sørensen)
Author URL: http://plugins.righthere.com/complete-gallery-manager/
Tags: WordPress, Isotype, Masonry, Responsive, Infinite Scroll, Gallery
Requires at least: 3.0
Tested up to: 3.4.1
Stable tag: 2.0.3 rev28734

======== CHANGELOG ========
Version 2.0.3 rev28734 - August 10, 2012
* Bug Fixed: Width x Height was switched, and caused wrong resizing of thumbnails (according to the size set in Settings > Media). 

Version 2.0.2 rev28693 - August 8, 2012
* Bug Fixed: Problem with IE9 not being able to load the CSS has been fixed
* Bug Fixed: Problem with Pinterest not "pinning" the individual picture, but the entire gallery. This has been fixed so that the individual picture is pinned, and when you click the picture on Pinterest it will take you directly back to the image and show it in prettyPhoto (lightbox).
* Bug Fixed: Shadow effect on menu text can now be removed

Version 2.0.1 rev27876 - July 20, 2012
* Bug Fixed: Gallery Settings not loading when the server is Windows based
* Bug Fixed: CSS name misspelled

Version 2.0.0 rev27524 - July 12, 2012
* Update: The entire core of the plugin has been updated so that it works with the ID number of the media added. This has significantly improved the speed of the gallery. This does unfortunately require you to remove ALL images from your current galleries and reload them. However ALL settings are saved. Simply use the "Remove All Images" button and then add the images again from the media library.
* Update: Cleaned up the Thumbnail sizes. Only showing the four standard sizes from WordPress: Thumbnail, Medium, Large and Full Size
* New Feature: Added new Custom Thumbnail size option
* New Feature: Added Link Overwrite field. This is a local select link action for each type of media, which will overwrite any global settings.
* Update: Removed tag field as its not used by Isotype
* New Feature: Added icon to show type of media (Images, Posts, Pages, Videos)
* New Feature: Available Categories for each media type will only show categories that are selected from the Category Metabox.
* New Feature: Added Remove All Images button to "Selected Images". This makes it possible to quickly remove all inserted media.
* New Feature: Automatically retrieve Title, Description and Permalink when images are selected
* Update: Images are reloaded when the sort type is changed in the pop up window for selecting images to make sure that all data comes in the right flow.
* New Feature: Support for adding Post(s) with attachment image(s)
* New Feature: Select Category and add all Posts with attachment images. Automatically retrieve title, content, link (auto update if post changes). Title link description is locked (auto reload data)
* New Feature: Support for adding Page(s) with attachment image(s)
* New Feature: Select Patent Page with attachment image in order to add all sub-pages. Automatically retrieve title, content, link (auto update if page changes). Title link description is locked (auto reload data)
* New Feature: Support for adding YouTube or Vimeo Videos. Automatically retrieve title and saves preview images to media library (only one time even though you might add the video multiple times)
* New Feature: Support for selecting alternate preview image for video (from Media Library)
* New Feature: Added player button icon to preview image in order to show that it is a Video
* New Feature: Added option to set maximum number of words to captions that will be displayed to make sure Post(s) and Page(s) description data is not too long
* New Feature: Added support for "max word indicator" (More, Read More etc.)
* New Feature: Added 4 new types of captions that will always be visible (doesn't require mouse over action to activate)
* New Feature: Set inside shadow color, offset and blur for menu
* Bug Fixed: You can now click on captions 


Version 1.0.3 rev26366 - June 22, 2012
* Bug Fixed: Facebook like button div fixed.

Version 1.0.2 rev25760 - June 6, 2012
* Bug Fixed: Remove warnings on debug mode (causing image upload and select images to fail)
* Update: Updated Options Panel

Version 1.0.1 rev25421 - May 30, 2012
* New Feature: Added Support for 4 Custom Capabilities:
	cgm_create_gallery
	cgm_upload_images
	cgm_select_images
	cgm_insert_gallery
* Bug Fixed: Animation on click to next size
* Bug Fixed: Universal Scroll load

Version 1.0.0 rev25273 - May 25, 2012
* First release.


======== DESCRIPTION ========
Complete Gallery Manager for WordPress is an exquisite jQuery plugin for creating magical galleries with Pictures, Posts, Pages and Videos. This is an incredible versatile plugin, which lets you create amazing looking galleries very easily. From simple fully responsive galleries or with infinite scroll, with this plugin it is very easy. The galleries support prettyPhoto lightbox with major social sharing icons; Facebook Like, Twitter, Google+ and Pin It. You can also enable captions on each picture in the gallery.

The plugin is ready for internationalization. 

== INSTALLATION ==

1. Upload the 'complete-gallery-manager' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. In the menu you will find CGM Settings (Complete Gallery Manager Settings)

== FREQUENTLY ASKED QUESTIONS ==
If you have any questions or trouble using this plugin you are welcome to contact us through our profile on Codecanyon (http://codecanyon.net/user/RightHere)

Or visit our HelpDesk at http://support.righthere.com


== SOURCES - CREDITS & LICENSES ==

We have used the following open source projects, graphics, fonts, API's or other files as listed. Thanks to the author for the creative work they made.

1) Isotope Commercial License
   Item # 13620 
   David DeSandro   
   http://isotope.metafizzy.co/

2) prettyPhoto
   http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/

3) Captions
   Licensor's Author Username: DADU
   Licensee: RightHere LLC
   License: One Extended License
   For the item: Captions
   http://codecanyon.net/item/captions/159760
   Item # 159760
   Item Purchase Code: 08e1a444-80a5-4d30-97e4-2258c67f5017