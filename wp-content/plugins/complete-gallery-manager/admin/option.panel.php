<?php

class cgm_options {
	var $screen_title = 'Options';
	var $screen_menu = 'Options';    
	var $plugin_id;
	var $tdom = 'cgm';
	
	function cgm_options($parent_id){
		$this->plugin_id = $parent_id.'-opt';
		add_filter("pop-options_{$this->plugin_id}",array(&$this,'options'),10,1);		
	} 

    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;        
        }
        return $val;
    }
    
	function options($t){
        global $wpdb;

		$i = count($t);
		//-------------------------	Generelle Settings	
		$i++;
		$t[$i]->id 			= 'Generalte-settings'; 
		$t[$i]->label 		= __('General Settings','cgm');//title on tab
		$t[$i]->right_label	= __('Input data','cgm');//title on tab
		$t[$i]->page_title	= __('General Settings','cgm');//title on content
		$t[$i]->theme_option = true;
		$t[$i]->plugin_option = true;
		
		
		$postSize = $this->toBytes(ini_get('post_max_size'));
		$uploadSize = $this->toBytes(ini_get('upload_max_filesize'));
		
		if(empty($uploadSize)){
			$uploadSize = 100000;
		}
		if(empty($postSize)){
			$postSize = 100000;
		}
		
		 
		
		$temp = array();
		$temp[] = (object)array(
				'id'	=> 'uploadsize',
				'type'	=> 'text',
				'label'	=> __('Upload size','cgm'),
				'default' => $postSize/1024/102,
				'description' => __('The allowed upload file size is set to your servers default. You can decrease the allowed upload file size for Complete Gallery Manager, but if you want to increase it you need to change the file upload size allowed in your servers php.ini.','cgm') . '<br><br>'.'Post max size:' .$postSize/1024/1024 .'mb<br>Upload max filesize: '.$uploadSize/1024/1024,
				'el_properties'=>array('style'=>'width:250px;margin-bottom: 50px;'),
				'save_option'=>true,
				'load_option'=>true
		);
		
		$temp[] = (object)array(
				'type'	=> 'submit',
				'label'	=> __('Save','cgm'),
				'class' => 'button-primary',
				'save_option'=>false,
				'el_properties'=>array('style'=>'width:100%;'),
				'load_option'=>false
			);			
			
		$t[$i]->options = $temp;
		return $t;
	}
}
?>