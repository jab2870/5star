<?php 
function cgm_install_capabilities(){
	$caps = array();
	$caps[] = 'cgm_create_gallery';
	$caps[] = 'cgm_upload_images';
	$caps[] = 'cgm_select_images';
	$caps[] = 'cgm_insert_gallery';
 	

	$WP_Roles = new WP_Roles();	
	foreach($caps as $cap){
		$WP_Roles->add_cap( 'administrator', $cap );
	}
	
	update_option('cgm_version',CGM_VERSION);
}
?>