<?php 
class cgm_isotope_generator_class {
	var $tmp_id = '';
	var $tmp_post_id = '';
	var $tmp_images = '';
	var $tmp_settings = '';
	var $tmp_type = '';
	var $tmp_folder  = 'cgm';
	var $tmp_file = 'css.css';
	var $tmp_categoryes = array();
	var $tmp_prewiev = false;
	var $tmp_current_count = 0;
	
	var $imageGalleryCurrent = 0;
	var $imageGalleryTotal = 0;	

	function load_images_gallery_new($id,$images,$settings,$current_count){
		$this->tmp_id = $id;
		$this->tmp_images = json_decode(urldecode($images));
		$this->tmp_settings = json_decode(urldecode($settings));
	
		$this->tmp_current_count = $current_count;
		$images = $this->images_generate();
		$more_images = 'true';
		
		if($this->imageGalleryCurrent<=$this->imageGalleryTotal){
			$more_images = 'false';
		}
		return array($more_images,$images);	
	}

	function cgm_drawIsoTope_gallery($id,$images,$settings,$type,$prewiev = false,$post_id=0){
		$upload_dir = wp_upload_dir();
		
		$this->tmp_id = $id;
		$this->tmp_post_id = $post_id; 
		
		$this->tmp_images = json_decode(urldecode($images));
		$this->tmp_settings = json_decode(urldecode($settings));
		$this->tmp_type = $type;
		
		$this->tmp_prewiev = $prewiev;
			
		$images = $this->images_generate();	



		if($prewiev || !file_exists($upload_dir['basedir'].'/'.$this->tmp_folder.'/'.$this->tmp_post_id.$this->tmp_file)){
			$container  = $this->css_generate();		
		}
		
    	/*$container .=  '<link rel="stylesheet" type="text/css" href="'.$upload_dir['baseurl'].'/'.$this->tmp_folder.'/'.$this->tmp_post_id.$this->tmp_file.'" />';*/
    	
    	if($prewiev){
    		$container .= '<script>cgm_loadcss(\''.$upload_dir['baseurl'].'/'.$this->tmp_folder.'/'.$this->tmp_post_id.$this->tmp_file.'\', "css");</script>';
    	}

		if(!empty($this->tmp_settings->cgm_menu->pos->type) && ($this->tmp_settings->cgm_menu->pos->type == 'ldt' || $this->tmp_settings->cgm_menu->pos->type == 'sbst' || $this->tmp_settings->cgm_menu->pos->type == 'sbstb' || $this->tmp_settings->cgm_menu->pos->type == 'ldtb')){
			$container .= '<div class="cgm_isotype_menu_'.$this->tmp_post_id.'" id="cgm_isotype_menu_'.$this->tmp_id.'">';
				$container .= $this->sort_generate();
				$container .= $this->sort_order_generate();
				$container .= $this->layout_generate();
				$container .= $this->filter_generate();
			$container .= '</div>';
		$container .= '<div class="clear" style="clear:both"></div>';
		}
		
		$container .= '<div class="cgm_isotype_bg_'.$this->tmp_post_id.'" id="cgm_isotype_bg_'.$this->tmp_id.'">';
		$container .= '<div id="universall_scroll" class="universall_scroll"><div></div><span style="display:none">true</span></div>';
		
		$container .= $images;
		$container .= '</div>';
		
		if(!empty($this->tmp_settings->cgm_menu->pos->type) && ($this->tmp_settings->cgm_menu->pos->type == 'ldb' || $this->tmp_settings->cgm_menu->pos->type == 'sbsb'  || $this->tmp_settings->cgm_menu->pos->type == 'sbstb' || $this->tmp_settings->cgm_menu->pos->type == 'ldtb')){
			$container .= '<div class="clear" style="clear:both"></div>';
			$container .= '<div class="cgm_isotype_menu_'.$this->tmp_post_id.'" id="cgm_isotype_menu_'.$this->tmp_id.'">';
				$container .= $this->sort_generate();
				$container .= $this->sort_order_generate();
				$container .= $this->layout_generate();
				$container .= $this->filter_generate();
			$container .= '</div>';
		}
		
		$container .= '<div class="clear" style="clear:both"></div>';
		return $container;

	}

	function sort_order_generate(){
		$return_sort = '';
		if(!empty($this->tmp_settings->cgm_sort)){
			if(!empty($this->tmp_settings->cgm_sort->order)){
				$return_sort = '<div class="cgm_sort_order">';
				$return_sort .= '<ul>';
				$return_sort .= '<li><a href="#" class="selected" onclick="cgm_sort_order_system('.$this->tmp_id.',true,this);return false;">Ascending</a></li>';
				$return_sort .= '<li><a href="#" onclick="cgm_sort_order_system('.$this->tmp_id.',false,this);return false">Descending</a></li>';
				$return_sort .= '</ul></div>';		
			}		
		}
		
		return $return_sort;
	}
	function sort_generate(){
		$return_sort = '';
		if(!empty($this->tmp_settings->cgm_sort)){
			$sortdata = $this->tmp_settings->cgm_sort;
			$default = $sortdata->default;
			
			if(!empty($this->tmp_settings->cgm_menu->pos->showtitle) && $this->tmp_settings->cgm_menu->pos->showtitle){
				$return_sort .=	'<lable>'.__('Sort','cgm').':</lable> ';
			}
			if(!empty($sortdata->index)){
				$return_sort .= '<li><a href="#" '.($default == 'index' ? 'class="selected"' : '').' onclick="cgm_sort_system('.$this->tmp_id.',\'index\',this);return false">Index</a></li>';
			}
			if(!empty($sortdata->title)){
				$return_sort .= '<li><a href="#" '.($default == 'title' ? 'class="selected"' : '').' onclick="cgm_sort_system('.$this->tmp_id.',\'title\',this);return false">Title</li>';
			}
			if(!empty($sortdata->desc)){
				$return_sort .= '<li><a href="#" '.($default == 'desc' ? 'class="selected"' : '').' onclick="cgm_sort_system('.$this->tmp_id.',\'desc\',this);return false">Description</li>';
			}

			if(!empty($sortdata->tag)){
				$return_sort .= '<li><a href="#" '.($default == 'tag' ? 'class="selected"' : '').' onclick="cgm_sort_system('.$this->tmp_id.',\'tag\',this);return false">Link</li>';
			}
			if(!empty($sortdata->imageSize)){
				$return_sort .= '<li><a href="#" '.($default == 'imageSize' ? 'class="selected"' : '').' onclick="cgm_sort_system('.$this->tmp_id.',\'imageSize\',this);return false">Image Size</li>';
			}
			
			if(!empty($return_sort)){
				$return_sort = '<div class="cgm_sort"><ul>'.$return_sort.'</ul></div>';	
			}
		}
		
		return $return_sort;
	}
	
	function layout_generate(){
		$return_layout = '';
		if(!empty($this->tmp_settings->cgm_layout)){
			$tmp = $this->tmp_settings->cgm_layout;
			$default = $tmp->default;
			
			$default_height = '';
			if(!empty($this->tmp_settings->cgm_height)){
				$default_height = $this->tmp_settings->cgm_height.'px';
			} else {
				$default_height = '100%';
			}
			
			$default_width = '';
			if(!empty($this->tmp_settings->cgm_width)){
				$default_width = $this->tmp_settings->cgm_width.'px';
			} else {
				$default_width = '100%';
			}
			
			if(!empty($this->tmp_settings->cgm_menu->pos->showtitle) && $this->tmp_settings->cgm_menu->pos->showtitle){
				$return_layout .=	'<lable>'.__('Layout','cgm').':</lable> ';
			}
			
			if(!empty($tmp->masonry)){
				$return_layout .= '<li><a href="#" title="masonry" '.($default == 'masonry' ? 'class="selected"' : '').' onclick="cgm_layout_system('.$this->tmp_id.',\'masonry\',\''.$default_width.'\',\''.$default_height.'\',this);return false;">Masonry</a></li>';
			}
			if(!empty($tmp->fitRows)){
				$return_layout .= '<li><a href="#" title="fitRows" '.($default == 'fitRows' ? 'class="selected"' : '').' onclick="cgm_layout_system('.$this->tmp_id.',\'fitRows\',\''.$default_width.'\',\''.$default_height.'\',this);return false;">Fit Rows</a></li>';
			}
			if(!empty($tmp->cellsByRow)){
				$return_layout .= '<li><a href="#" title="cellsByRow" '.($default == 'cellsByRow' ? 'class="selected"' : '').' onclick="cgm_layout_system('.$this->tmp_id.',\'cellsByRow\',\''.$default_width.'\',\''.$default_height.'\',this);return false;">Cells By Row</a></li>';
			}

			if(!empty($tmp->straightDown)){
				$return_layout .= '<li><a href="#" title="straightDown" '.($default == 'straightDown' ? 'class="selected"' : '').' onclick="cgm_layout_system('.$this->tmp_id.',\'straightDown\',\''.$default_width.'\',\''.$default_height.'\',this);return false;">Straight Down</a></li>';
			}
			if(!empty($tmp->masonryHorizontal)){
				$return_layout .= '<li><a href="#" title="masonryHorizontal" '.($default == 'masonryHorizontal' ? 'class="selected"' : '').' onclick="cgm_layout_system('.$this->tmp_id.',\'masonryHorizontal\',\''.$default_width.'\',\''.$default_height.'\',this);return false;">Masonry Horizontal</a></li>';
			}

			if(!empty($tmp->fitColumns)){
				$return_layout .= '<li><a href="#" title="fitColumns" '.($default == 'fitColumns' ? 'class="selected"' : '').' onclick="cgm_layout_system('.$this->tmp_id.',\'fitColumns\',\''.$default_width.'\',\''.$default_height.'\',this);return false;">Fit Columns</a></li>';
			}			
			
			if(!empty($tmp->cellsByColumn)){
				$return_layout .= '<li><a href="#" title="cellsByColumn" '.($default == 'cellsByColumn' ? 'class="selected"' : '').' onclick="cgm_layout_system('.$this->tmp_id.',\'cellsByColumn\',\''.$default_width.'\',\''.$default_height.'\',this);return false;">Cells By Column</a></li>';
			}
			
			if(!empty($tmp->straightAcross)){
				$return_layout .= '<li><a href="#" title="straightAcross" '.($default == 'straightAcross' ? 'class="selected"' : '').' onclick="cgm_layout_system('.$this->tmp_id.',\'straightAcross\',\''.$default_width.'\',\''.$default_height.'\',this);return false;">Straight Across</a></li>';
			}			

			if(!empty($return_layout)){
				$return_layout = '<div class="cgm_layout"><ul>'.$return_layout.'</ul></div>';		
			}
		}
		
		return $return_layout;
	}
	
	function filter_generate(){

		$return_filter = '';
		if(!empty($this->tmp_settings->cgm_filters) and count($this->tmp_categoryes)>0){
			$return_filter = '<div class="cgm_filter">';
			
			if(!empty($this->tmp_settings->cgm_menu->pos->showtitle) && $this->tmp_settings->cgm_menu->pos->showtitle){
				$return_filter .=	'<lable>'.__('Filter','cgm').':</lable> ';
			}
			
			$return_filter .=	'<ul>';
			
			$return_filter .= '<li><a href="#" class="selected" onclick="cgm_filter_system('.$this->tmp_id.',\'*\',this);return false">All</a></li>';

			$taxonomies=get_categories(array('hide_empty' => 0,'taxonomy' => 'cgm-category','orderby' => 'name','order'=> $this->tmp_settings->cgm_filtersDirrection));

			foreach($taxonomies as $taxonomie){
				$tmp_name = '';
				$tmp_catid = '';
				foreach($this->tmp_categoryes as $tmp_cat){
					if($taxonomie->term_id == $tmp_cat){
						$tmp_name = $taxonomie->name;
					}
				}
				if(!empty($tmp_name)){
					$return_filter .= '<li><a href="#" onclick="cgm_filter_system('.$this->tmp_id.',\'categoryid'.$taxonomie->term_id.'\',this);return false">'.$tmp_name.'</a></li>';	
				}
			}

			$return_filter .= '</ul></div>';		
		}
		
		return $return_filter;
		
	}
	
	function images_generate(){
		global $wpdb;
		$return_images = '';
		
		$upload_dir = wp_upload_dir();
		
	
		$click_functions = '';
		$total_number = 0;
		$show_decs = '';
		if(!empty($this->tmp_settings->cgm_mouseEventClick) and $this->tmp_settings->cgm_mouseEventClick != 1){
			$click_functions = $this->tmp_settings->cgm_mouseEventClick;
		}
		
		if(!empty($this->tmp_settings->cgm_universallScroll) and !empty($this->tmp_settings->cgm_universallScroll->loadNumber) and $this->tmp_settings->cgm_universallScroll->loadNumber > 0){
			$total_number = $this->tmp_settings->cgm_universallScroll->loadNumber;
		}
		
		if(!empty($this->tmp_settings->cgm_pretty) and !empty($this->tmp_settings->cgm_pretty->showdecs) and $this->tmp_settings->cgm_pretty->showdecs){
			$show_decs = $this->tmp_settings->cgm_pretty->showdecs;
		}
		
		$tmp_pm_css = '';
		
		if(!empty($this->tmp_settings->cgm_item)){
			if(!empty($this->tmp_settings->cgm_item->margin)){
				$tmp_pm_css .= 'margin:'.$this->tmp_settings->cgm_item->margin.';';
			}
			
			if(!empty($this->tmp_settings->cgm_item->padding)){
				$tmp_pm_css .= 'padding:'.$this->tmp_settings->cgm_item->padding.';';
			}	
		}

		
		$count_image = 1;
		$count_image_total = 1;
		
		if(!empty($this->tmp_images)){	
			foreach($this->tmp_images as $tmp_image_key => $tmp_image){
				if($count_image > $this->tmp_current_count || $this->tmp_current_count == 0){
					if($count_image > ($total_number+$this->tmp_current_count) &&  $total_number > 0){
						$count_image_total++;
					} else {
						$imageselected = 0;
				
						$click_function = $click_functions;
						
						
						if(!empty($tmp_image->linkoverwrite) and $tmp_image->linkoverwrite != 'default' ){
							$click_function = $tmp_image->linkoverwrite;
						}
				
				
						$post_exists = $wpdb->get_row("SELECT * FROM $wpdb->posts WHERE id = '" . $tmp_image->postid . "'", 'ARRAY_A');
				
						if(!empty($tmp_image->show) and $tmp_image->show == 'true' && $post_exists){
							if(!empty($tmp_image->imageselected)){
								$imageselected = $tmp_image->imageselected;
							}
							$imageselected_w = '';
							$imageselected_h = '';
							$imageselected_url = '';
							
							$tmp_cats = '';
							if(!empty($tmp_image->category)){
								foreach($tmp_image->category as $tmp_cat){
									if(!empty($tmp_cats)){
										$tmp_cats .= ' '; 
									}
									
									$this->tmp_categoryes[$tmp_cat] = $tmp_cat;
									$tmp_cats .= 'categoryid'.$tmp_cat;
								}
							}
		
							if(!empty($tmp_image->attactedid) and is_numeric($tmp_image->attactedid)){
							
								$post_tmp = get_post($tmp_image->attactedid); 
								$tmp_image->description = strip_tags($post_tmp->post_content);
								$tmp_image->title = $post_tmp->post_title;
								$tmp_image->link = get_permalink($tmp_image->attactedid);
							}
							
							if(!empty($this->tmp_settings->cgm_captions->maxNumberWord)){
								$tmp_image->description = $this->wordlimit($tmp_image->description,$this->tmp_settings->cgm_captions->maxNumberWord,$this->tmp_settings->cgm_captions->maxNumberWordIndicator);
							}
				
							$tmp_imgs = '';
							$tmp_type_sizes = array('thumbnail','medium','large','full','custom');
							
							foreach($tmp_type_sizes as $tmp_type_size){
								if($tmp_type_size == 'custom'){
									$tmp_infoff = wp_get_attachment_image_src($tmp_image->postid,'full');
									$tmp_infos = get_post_meta($tmp_image->postid, '_wp_attached_file', true); 

									if(substr($tmp_infos, 0, 1) != '/'){
										$tmp_infos = '/'.$tmp_infos;
									}

									$tmp_info = pathinfo($tmp_infos);
									
									if(!empty($tmp_image->customwidth) and !empty($tmp_image->customheight)){
										$tmp_name = $upload_dir['basedir'].'/'.$tmp_info['filename'].'-'.$tmp_image->customwidth.'x'.$tmp_image->customheight.'.'.$tmp_info['extension'];
										
										$tmp_standart = image_resize( ($upload_dir['basedir'].$tmp_infos), $tmp_image->customwidth,$tmp_image->customheight,1);

										if(is_object($tmp_standart)){
											$tmp_standart = image_resize( ($upload_dir['basedir'].$tmp_infos), $tmp_image->customwidth,$tmp_image->customheight,1);
										}

										if(is_object($tmp_standart)){
											$tmp_standart = image_resize( ($upload_dir['basedir'].$tmp_info['dirname'].'/'. $tmp_info['filename'].'-'.$tmp_infoff[1].'x'.$tmp_infoff[2].'.'. $tmp_info['extension']), $tmp_image->customwidth,$tmp_image->customheight,1);
										}

										if(is_object($tmp_standart)){
											$tmp_loaded = $tmp_infoff;
										} else {
											$tmp_tmp_name = basename($tmp_standart);
											$tmp_tmp_size = explode("-", $tmp_tmp_name);
											$tmp_tmp_size = explode(".", $tmp_tmp_size[(count($tmp_tmp_size)-1)]);
											$tmp_tmp_size = explode("x", $tmp_tmp_size[(count($tmp_tmp_size)-2)]);
	
											$tmp_loaded[0] = $upload_dir['baseurl'].$tmp_info['dirname'].'/'.$tmp_info['filename'].'-'.$tmp_tmp_size[0].'x'.$tmp_tmp_size[1].'.'.$tmp_info['extension'];
	
											$tmp_loaded[1] = $tmp_tmp_size[0];
											$tmp_loaded[2] = $tmp_tmp_size[1];	
										};
										

									}
								} else {
									$tmp_loaded = wp_get_attachment_image_src($tmp_image->postid,$tmp_type_size);
								}
							
								if($tmp_type_size != 'custom' || ($tmp_type_size == 'custom' && !empty($tmp_image->customwidth) && !empty($tmp_image->customheight))){
								$tmp_imgs .= '<div style="display:none" class="dif_img';
								
								
								if($imageselected == $tmp_type_size){
									$tmp_imgs .= ' current';
									$imageselected_w = $tmp_loaded[1];
									$imageselected_h = $tmp_loaded[2];
									$imageselected_url = $tmp_loaded[0];
								}
		
								if($tmp_type_size == 'full'){
									$full_screen_tmp = $tmp_loaded[0];
									
								}
		
		
								$tmp_imgs .= '">';
								$tmp_imgs .= '<span class="height">'.$tmp_loaded[2].'</span>';
								$tmp_imgs .= '<span class="width">'.$tmp_loaded[1].'</span>';
								$tmp_imgs .= '<span class="url">'.$tmp_loaded[0].'</span>';
								$tmp_imgs .= '</div>';
								}
							}


							$return_images .= '<div id="cgm_items" class="cgm_items '.$tmp_cats.'" style="';
							$return_images .= 'width:'.$imageselected_w.'px;';
							$return_images .= $tmp_pm_css;
							$return_images .= 'height:'.$imageselected_h.'px;">';
		
		
							if((!empty($this->tmp_settings->cgm_captions->showtitle) and $this->tmp_settings->cgm_captions->showtitle and !empty($tmp_image->title)) or (!empty($this->tmp_settings->cgm_captions->showdecs) and $this->tmp_settings->cgm_captions->showdecs and $tmp_image->description ) and !empty($tmp_image->title)){
								$return_images .= '<figure class="captions '.$this->tmp_settings->cgm_captions->type.'">';
							}
		
		
								if(!empty($tmp_image->typeobject) and ($tmp_image->typeobject=='youtube' or $tmp_image->typeobject=='vimeo') and ($click_function == '1' || $click_function =='prettyPhoto')){
									$return_images .= '<a style="display: block;width:100%;height:100%" href="'.$tmp_image->link.'" rel="prettyPhoto[pp_gal]" title="'.($show_decs == 'true' ? $tmp_image->description : '').'">';
								} else if(!empty($click_function) and $click_function=='prettyPhoto'){
									$return_images .= '<a style="display: block;width:100%;height:100%" href="'.$full_screen_tmp.'" rel="prettyPhoto[pp_gal]" title="'.($show_decs == 'true' ? $tmp_image->description : '').'">';
								} else if(!empty($click_function) and $click_function=='click'){
									$return_images .= '<a style="display: block;width:100%;height:100%" href="'.$tmp_image->link.'">';
								} else if(!empty($click_function) and $click_function=='clickNew'){
									$return_images .= '<a target="_blank" style="display: block;width:100%;height:100%" href="'.$tmp_image->link.'">';
								} else if(!empty($click_function) and $click_function !='1'){
									$return_images .= '<a style="display: block;width:100%;height:100%" href="#" onClick="cgm_changeImages('.$this->tmp_id.',this,\''.$click_function.'\');return false;" >';
								} else {
									$return_images .= '<a style="display: block;width:100%;height:100%;cursor: default;" href="#" onClick="return false" >';
								}
		
								$return_images .= '<img src="'.$imageselected_url.'" style="width:'.$imageselected_w.'px;height:'.$imageselected_h.'px;';
								$return_images .= '" alt="'.$tmp_image->title.'" />';
								if($tmp_image->typeobject=='youtube' or $tmp_image->typeobject=='vimeo'){
									$return_images .= '<img id="cgm_video_play_icon" src="'.COMPLETE_GALLERY_URL.'images/Play1Pressed.png" alt="'.$tmp_image->title.'" />';									
								
								}
								

		
								$return_images .= '<div class="default_image" style="display:none">'.$imageselected.'</div>';
								$return_images .= '<div class="index" style="display:none">'.$tmp_image_key.'</div>';
								$return_images .= '<div class="title" style="display:none">'.$tmp_image->title.'</div>';					
								$return_images .= '<div class="tag" style="display:none">'.$tmp_image->link.'</div>';						
								$return_images .= '<div class="desc" style="display:none">'.$tmp_image->description.'</div>';
								$return_images .= '<div class="size" style="display:none">'.($imageselected_w*$imageselected_h).'</div>';		
								$return_images .= '<div class="imageSize" style="display:none"> '. $tmp_imgs . '</div>';
								
									$return_images .= '</a>';
		
		
								if((!empty($this->tmp_settings->cgm_captions->showtitle) and $this->tmp_settings->cgm_captions->showtitle and !empty($tmp_image->title)) or (!empty($this->tmp_settings->cgm_captions->showdecs) and $this->tmp_settings->cgm_captions->showdecs and $tmp_image->description ) and !empty($tmp_image->title)){
									$return_images .= '<figcaption ';
									
									if(!empty($click_function) and $click_function !='1'){
										$return_images .= ' onclick="cgm_activate_link(this);" style="cursor:pointer" ';	
									}

									$return_images .= '>';
									if($this->tmp_settings->cgm_captions->showtitle and !empty($tmp_image->title)){
										$return_images .= 	'<h1>'.$tmp_image->title.'</h1>';				
									}
									if($this->tmp_settings->cgm_captions->showdecs and !empty($tmp_image->description)){
										$return_images .= 	'<p>'.$tmp_image->description.'</p>';				
									}
									$return_images .= '</figcaption>';
									$return_images .= '</figure>';
								}
		
		
									$return_images .= '</div>';
								
							$count_image_total++;
							$count_image++;					
						}
					}
				}	else {
					$count_image_total++;
					$count_image++;
				}
			if(!empty($tmp_image->category)){
				foreach($tmp_image->category as $tmp_cat){
					$this->tmp_categoryes[$tmp_cat] = $tmp_cat;
				}
			}
	
			}
		}
		
		$this->imageGalleryCurrent = $count_image;
		$this->imageGalleryTotal = $count_image_total;
		
		return $return_images;
	}
	
	function css_generate(){
		$tmp_css = '';
			$tmp_css .= '.isotope .isotope-item {';
			$tmp_css .= '-webkit-transition-duration: 0.8s !important;';
			$tmp_css .= '-moz-transition-duration: 0.8s !important;';
			$tmp_css .= '-ms-transition-duration: 0.8s !important;';
			$tmp_css .= '-o-transition-duration: 0.8s !important;';
			$tmp_css .= 'transition-duration: 0.8s !important;';
			$tmp_css .= '}';
		
		if(!empty($this->tmp_settings->cgm_animation->duration)){
			$dub = $this->tmp_settings->cgm_animation->duration;
			$dub = number_format(($dub/1000), 2, '.', '');
			$tmp_css .= '.cgm_isotype_bg_'.$this->tmp_post_id.' .isotope .isotope-item {';
			$tmp_css .= '-webkit-transition-duration: '.$dub.'s !important;';
			$tmp_css .= '-moz-transition-duration: '.$dub.'s !important;';
			$tmp_css .= '-ms-transition-duration: '.$dub.'s !important;';
			$tmp_css .= '-o-transition-duration: '.$dub.'s !important;';
			$tmp_css .= 'transition-duration: '.$dub.'s !important;';
			$tmp_css .= '}';
		}
		
		$tmp_css .= '.cgm_isotype_bg_'.$this->tmp_post_id.' .isotope {-webkit-transition-property: height, width;-moz-transition-property: height, width;-ms-transition-property: height, width;-o-transition-property: height, width;transition-property: height, width;}';

		$tmp_css .= '.cgm_isotype_bg_'.$this->tmp_post_id.' .isotope .isotope-item {-webkit-transition-property: -webkit-transform, opacity;-moz-transition-property:    -moz-transform, opacity;-ms-transition-property:-ms-transform, opacity;-o-transition-property:top, left, opacity;transition-property:transform, opacity;}';
		
		
		
		
		
		
		// --------------- background start -------------------------
		
		$tmp_css .= '.cgm_isotype_bg_'.$this->tmp_post_id.' {';
		if(!empty($this->tmp_settings->cgm_height)){
			$tmp_css .= 'height:'.$this->tmp_settings->cgm_height.'px;';
		} else {
			$tmp_css .= 'height:100%;';
		}
		
		if($this->tmp_prewiev){
			$tmp_css .= 'min-height:350px;';
		} else {
			$tmp_css .= 'min-height:150px;';
		}
			
				
		$tmp_css .= 'overflow:auto;';
		if(!empty($this->tmp_settings->cgm_width)){
			$tmp_css .= 'width:'.$this->tmp_settings->cgm_width.'px;';
		} else {
			$tmp_css .= 'width:100%;';
		}
		
		if(!empty($this->tmp_settings->cgm_background)){
			$bgdata = $this->tmp_settings->cgm_background;
				
			if(!empty($bgdata->backgroundColor)){
				$tmp_css .= 'background-color:'.$bgdata->backgroundColor.';';
			}
			
			if(!empty($bgdata->borderColor)){
				$tmp_css .= 'border-color:'.$bgdata->borderColor.';';
			}
			
			if(!empty($bgdata->borderWidth)){
				$tmp_css .= 'border-width:'.$bgdata->borderWidth.'px;';
			}
			
			if(!empty($bgdata->borderRadius)){
				$tmp_css .= 'border-radius:'.$bgdata->borderRadius.'px;';
			}		
			
			if(!empty($bgdata->borderStyle)){
				$tmp_css .= 'border-style:'.$bgdata->borderStyle.';';
			}
			
		}
		$tmp_css .= '}';	
		// --------------- background end -------------------------

		// --------------- Captions start -------------------------
		$tmp_css .='#cgm_video_play_icon {left: 50%;margin-left: -48px;margin-top: -48px;opacity: 0.9;position: absolute;top: 50%;height: 96px;width: 96px;}';
					
					
					
					

		$tmp_css .= '.cgm_isotype_bg_'.$this->tmp_post_id.' .cgm_items figcaption h1 {margin-bottom: 0;padding-bottom: 0;';
		if(!empty($this->tmp_settings->cgm_captions->h1)){
			$tmp = $this->tmp_settings->cgm_captions->h1;
			
			if(!empty($tmp->fontSize)){
				$tmp_css .= 'font-size:'.$tmp->fontSize.'px;';
			}

			if(!empty($tmp->textColor)){
				$tmp_css .= 'color:'.$tmp->textColor.';';
			}	

			if(!empty($tmp->family)){
				$tmp_css .= 'font-family:'.$tmp->family.';';
			}			
						
			if(!empty($tmp->underline)){
				$tmp_css .= 'text-decoration: underline;';
			} else {
				$tmp_css .= 'text-decoration: none;';
			}	
			
			if(!empty($tmp->bold)){
				$tmp_css .= 'font-weight: bold;';
			} else {
				$tmp_css .= 'font-weight: normal;';
			}	
			
			if(!empty($tmp->italic)){
				$tmp_css .= 'font-style:italic;';
			} else {
				$tmp_css .= 'font-style:none;';
			}	
			
			$textshadowopacity = 0;
			$textshadowcolor = '#';
		
			if(!empty($tmp->textShadowOpacity)){
				$textshadowopacity = $tmp->textShadowOpacity;
			}

			if(!empty($tmp->textShadowColor)){
				$textshadowcolor = $tmp->textShadowColor;
			}
		
			if(!empty($textshadowopacity) and !empty($textshadowcolor)  and $textshadowcolor != '#' ){
				$shadowrbg = $this->HexToRGB(str_replace('#','',$textshadowcolor));
				
				$tmp_css .= 'text-shadow: rgba('.$shadowrbg['r'].','.$shadowrbg['g'].','.$shadowrbg['b'].','.$textshadowopacity.') '; 
				
				
				$textShadowX = 0;
				$textShadowY = 0;
				$textShadowBlue = 0;
				
				if(!empty($tmp->textShadowX)){
					$textShadowX = $tmp->textShadowX;
				}
				
				if(!empty($tmp->textShadowY)){
					$textShadowY = $tmp->textShadowY;
				}
				
				if(!empty($tmp->textShadowBlue)){
					$textShadowBlue = $tmp->textShadowBlue;
				}
				
				$tmp_css .= $textShadowX.'px ';
				$tmp_css .= $textShadowY.'px ';
				$tmp_css .= $textShadowBlue.'px ';
				
				$tmp_css .= ';';
			}
			
			
			
		}
		$tmp_css .= '}';
		
		$tmp_css .= '.cgm_isotype_bg_'.$this->tmp_post_id.' .cgm_items figcaption p {';
		if(!empty($this->tmp_settings->cgm_captions->p)){
			$tmp = $this->tmp_settings->cgm_captions->p;
			
			if(!empty($tmp->fontSize)){
				$tmp_css .= 'font-size:'.$tmp->fontSize.'px;';
			}

			if(!empty($tmp->textColor)){
				$tmp_css .= 'color:'.$tmp->textColor.';';
			}	

			if(!empty($tmp->family)){
				$tmp_css .= 'font-family:'.$tmp->family.';';
			}			
						
			if(!empty($tmp->underline)){
				$tmp_css .= 'text-decoration: underline;';
			} else {
				$tmp_css .= 'text-decoration: none;';
			}	
			
			if(!empty($tmp->bold)){
				$tmp_css .= 'font-weight: bold;';
			} else {
				$tmp_css .= 'font-weight: normal;';
			}	
			
			if(!empty($tmp->italic)){
				$tmp_css .= 'font-style:italic;';
			} else {
				$tmp_css .= 'font-style:none;';
			}	
			
			$textshadowopacity = 0;
			$textshadowcolor = '#';
		
			if(!empty($tmp->textShadowOpacity)){
				$textshadowopacity = $tmp->textShadowOpacity;
			}

			if(!empty($tmp->textShadowColor)){
				$textshadowcolor = $tmp->textShadowColor;
			}
		
			if(!empty($textshadowopacity) and !empty($textshadowcolor) and $textshadowcolor != '#' ){
				$shadowrbg = $this->HexToRGB(str_replace('#','',$textshadowcolor));
				
				$tmp_css .= 'text-shadow: rgba('.$shadowrbg['r'].','.$shadowrbg['g'].','.$shadowrbg['b'].','.$textshadowopacity.') '; 
				
				
				$textShadowX = 0;
				$textShadowY = 0;
				$textShadowBlue = 0;
				
				if(!empty($tmp->textShadowX)){
					$textShadowX = $tmp->textShadowX;
				}
				
				if(!empty($tmp->textShadowY)){
					$textShadowY = $tmp->textShadowY;
				}
				
				if(!empty($tmp->textShadowBlue)){
					$textShadowBlue = $tmp->textShadowBlue;
				}
				
				$tmp_css .= $textShadowX.'px ';
				$tmp_css .= $textShadowY.'px ';
				$tmp_css .= $textShadowBlue.'px ';
				
				$tmp_css .= ';';
			}		

		}
		$tmp_css .= '}';
		
		
		
		$tmp_css .= '.cgm_isotype_bg_'.$this->tmp_post_id.' .cgm_items figure {';
		
		if(!empty($this->tmp_settings->cgm_captions->type) && ($this->tmp_settings->cgm_captions->type == 'flip horizontal' || $this->tmp_settings->cgm_captions->type == 'flip vertical')){
		} else {
			$tmp_css .= 'overflow: hidden;';
		}		
		if(!empty($this->tmp_settings->cgm_background->borderRadius)){
			$tmp_css .= 'border-radius:'.$this->tmp_settings->cgm_background->borderRadius.'px;';
		} else if(!empty($this->tmp_settings->cgm_item->borderRadius)){
			$tmp_css .= 'border-radius:'.$this->tmp_settings->cgm_item->borderRadius.'px;';
		}
		$tmp_css .= '}';
				

		$tmp_css .= '.cgm_isotype_bg_'.$this->tmp_post_id.' .cgm_items figcaption {';
		if(!empty($this->tmp_settings->cgm_captions)){
			$tmp = $this->tmp_settings->cgm_captions;
			
			
			$opacity = 0;
			$backgroundColor = '#ffffff';
			
			if(!empty($tmp->opacity)){
				$opacity = $tmp->opacity;
			}
			
			
			if(!empty($tmp->backgroundColor)){
				$backgroundColor = $tmp->backgroundColor;
			}
			
			if(!empty($opacity) and !empty($backgroundColor) ){
			
				$rbg = $this->HexToRGB(str_replace('#','',$backgroundColor));
				$tmp_css .= 'background-color: rgba('.$rbg['r'].','.$rbg['g'].','.$rbg['b'].','.$opacity.');';
			}		
			
			if(!empty($tmp->padding)){
				$tmp_css .= 'padding:'.$tmp->padding.';';
			}	
			
			if(!empty($tmp->align)){
				$tmp_css .= 'text-align:'.$tmp->align.';';
			}	
		}
		$tmp_css .= '}';
		
		// --------------- Captions end ---------------------------

		// --------------- Items start -------------------------
		
		$tmp_css .= '.cgm_isotype_bg_'.$this->tmp_post_id.' .cgm_items .dif_img {display:none;}';
		
		$tmp_css .= '.cgm_isotype_bg_'.$this->tmp_post_id.' .cgm_items img {';
		$tmp_css .= 'max-width: none !important;';
		$tmp_css .= 'min-width: none !important;';
		if(!empty($this->tmp_settings->cgm_item)){
			$itemdata = $this->tmp_settings->cgm_item;
			if(!empty($itemdata->imageRadius)){
				$tmp_css .= 'border-radius:'.$itemdata->imageRadius.'px;';
			}
		}
		$tmp_css .= '}';	

		$tmp_css .= '.cgm_isotype_bg_'.$this->tmp_post_id.' .cgm_items {';

		$tmp_css .= 'max-width: none !important;';
		$tmp_css .= 'min-width: none !important;';
		if(!empty($this->tmp_settings->cgm_item)){
			$itemdata = $this->tmp_settings->cgm_item;
				
			if(!empty($itemdata->margin)){
				$tmp_css .= 'margin:'.$itemdata->margin.';';
			}
			
			if(!empty($itemdata->padding)){
				$tmp_css .= 'padding:'.$itemdata->padding.';';
			}	

			if(!empty($itemdata->backgroundColor)){
				$tmp_css .= 'background-color:'.$itemdata->backgroundColor.';';
			}
			
			if(!empty($itemdata->borderColor)){
				$tmp_css .= 'border-color:'.$itemdata->borderColor.';';
			}
			
			if(!empty($itemdata->borderWidth)){
				$tmp_css .= 'border-width:'.$itemdata->borderWidth.'px;';
			}
			
			if(!empty($itemdata->borderRadius)){
				$tmp_css .= 'border-radius:'.$itemdata->borderRadius.'px;';
			}		
			
			if(!empty($itemdata->borderStyle)){
				$tmp_css .= 'border-style:'.$itemdata->borderStyle.';';
			}
			
		}
		$tmp_css .= '}';	
		// --------------- background end -------------------------
	
		// --------------- li ul start ----------------------------
		
		$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' ul {';
		$tmp_css .= 'list-style: none outside none;';
		$tmp_css .= 'margin: 0;';
		$tmp_css .= '}';

		$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' ul ul {';
		$tmp_css .= 'margin-left: 1.5em;';
		$tmp_css .= '}';
		
		$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li {';
		$tmp_css .= 'float: left;';
		$tmp_css .= 'margin-bottom: 0.2em;';
		$tmp_css .= '}';
		
		$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li a {';
		$tmp_css .= 'line-height: 24px;';
		$tmp_css .= '}';
		
		$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li a:active {';

			if(!empty($this->tmp_settings->cgm_menu) and !empty($this->tmp_settings->cgm_menu->pushed)){
				$tmp = $this->tmp_settings->cgm_menu->pushed;
				$textshadowopacity = 0;
				$textshadowcolor = '#';
			
				if(!empty($tmp->textShadowOpacity)){
					$textshadowopacity = $tmp->textShadowOpacity;
				}
	
				if(!empty($tmp->textShadowColor)){
					$textshadowcolor = $tmp->textShadowColor;
				}
				
				if(!empty($textshadowopacity) and !empty($textshadowcolor)  and $textshadowcolor != '#'){
					$shadowrbg = $this->HexToRGB(str_replace('#','',$textshadowcolor));
					
					$tmp_css .= 'box-shadow: ';
					
					$textShadowX = 0;
					$textShadowY = 0;
					$textShadowBlue = 0;
					
					if(!empty($tmp->textShadowX)){
						$textShadowX = $tmp->textShadowX;
					}
					
					if(!empty($tmp->textShadowY)){
						$textShadowY = $tmp->textShadowY;
					}
					
					if(!empty($tmp->textShadowBlue)){
						$textShadowBlue = $tmp->textShadowBlue;
					}
					
					$tmp_css .= $textShadowX.'px ';
					$tmp_css .= $textShadowY.'px ';
					$tmp_css .= $textShadowBlue.'px ';
					
					
					$tmp_css .= ' rgba('.$shadowrbg['r'].','.$shadowrbg['g'].','.$shadowrbg['b'].','.$textshadowopacity.') ';
					$tmp_css .= ' inset;';
				}	
			} else {
				$tmp_css .= 'box-shadow: 0 2px 8px rgba(0, 0, 0, 0.6) inset;';
			}

		$tmp_css .= '}';

		if(!empty($this->tmp_settings->cgm_menu) and !empty($this->tmp_settings->cgm_menu->border) and !empty($this->tmp_settings->cgm_menu->border->borderSeperator)){
			
			$tmp_color['r'] = '255';
			$tmp_color['g'] = '255';
			$tmp_color['b'] ='255';
			
			
			if(!empty($this->tmp_settings->cgm_menu->border->borderColor)){
				$tmp_color = $this->HexToRGB($this->tmp_settings->cgm_menu->border->borderColor);
			}
		
			$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li a {';
			$tmp_css .= 'border-left: 1px solid rgba('.$tmp_color['r'].', '.$tmp_color['g'].', '.$tmp_color['b'].', 0.3);';
			$tmp_css .= 'border-right: 1px solid rgba(0, 0, 0, 0.2);';
			$tmp_css .= '}';
		} else {
			$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li a {';
			$tmp_css .= 'border-left: 0px solid rgba(255, 255, 255, 0.3);';
			$tmp_css .= 'border-right: 0px solid rgba(0, 0, 0, 0.2);';
			$tmp_css .= '}';
		}
		

		if(!empty($this->tmp_settings->cgm_menu) and !empty($this->tmp_settings->cgm_menu->border) and !empty($this->tmp_settings->cgm_menu->border->borderRadius)){

			$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li:first-child a {';
			$tmp_css .= 'border-left: medium none;';
			$tmp_css .= 'border-radius: '.$this->tmp_settings->cgm_menu->border->borderRadius.'px 0 0 '.$this->tmp_settings->cgm_menu->border->borderRadius.'px;';
			$tmp_css .= '}';

			$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li:last-child a {';
			$tmp_css .= 'border-radius: 0 '.$this->tmp_settings->cgm_menu->border->borderRadius.'px '.$this->tmp_settings->cgm_menu->border->borderRadius.'px 0;';
			$tmp_css .= '}';
		} else {
			$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li:first-child a {';
			$tmp_css .= 'border-left: medium none;';
			$tmp_css .= 'border-radius: 0;';
			$tmp_css .= '}';

			$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li:last-child a {';
			$tmp_css .= 'border-radius: 0;';
			$tmp_css .= '}';
		}
		
		
		if(!empty($this->tmp_settings->cgm_menu) and !empty($this->tmp_settings->cgm_menu->border)){
			$tmp = $this->tmp_settings->cgm_menu->border;
			
			if(empty($tmp->borderStyle) or $tmp->borderStyle == 'none'){
				$tmp_s = '';
			} else {
				$tmp_s = $tmp->borderStyle;
			}
			
			if(empty($tmp->borderColor)){
				$tmp_c = '#ffffff';
			} else {
				$tmp_c = $tmp->borderColor;
			}	
			
			if(empty($tmp->borderWidth)){
				$tmp_w =  0;
			} else {
				$tmp_w = $tmp->borderWidth;
			}			
			
			if(!empty($tmp_s)){
				$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li:first-child a {';
				$tmp_css .= 'border-left: '.$tmp_w.'px '.$tmp_s.' '.$tmp_c.' !important;';
				$tmp_css .= 'border-left-color:'.$tmp_c.' !important;';
				$tmp_css .= '}';

				$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li:last-child a {';
				$tmp_css .= 'border-right:'.$tmp_w.'px '.$tmp_s.' '.$tmp_c.' !important;';
				$tmp_css .= 'border-right-color:'.$tmp_c.' !important;';
				$tmp_css .= '}';
			
				$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li a {';
				$tmp_css .= 'border-top-color:'.$tmp_c.' !important; ';
				$tmp_css .= 'border-bottom-color:'.$tmp_c.' !important; ';
				$tmp_css .= 'border-top: '.$tmp_w.'px '.$tmp_s.' '.$tmp_c .' !important;';
				$tmp_css .= 'border-bottom: '.$tmp_w.'px '.$tmp_s.' '.$tmp_c .' !important;';
				$tmp_css .= '}';	
			} 
		}
		


		// --------------- li ul end ----------------------------


		// --------------- menu pos start -------------------------



		$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' div:first-child {';
		$tmp_css .=	'margin-left: 0px !important;';	
		$tmp_css .= '}';
		
		
		$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' div:last-child {';
		$tmp_css .=	'margin-rigth: 0px !important;';	
		$tmp_css .= '}';
		
		
		
		$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' div {';

		if(!empty($this->tmp_settings->cgm_menu)){
			if(!empty($this->tmp_settings->cgm_menu->pos)){
			$tmp = $this->tmp_settings->cgm_menu->pos;
				if(!empty($tmp->align)){
					$tmp_css .= 'float:'.$tmp->align.';';
				}	
				
				if(!empty($tmp->margin)){
					$tmp_css .= 'margin: '.$tmp->margin.';';
				}
				if(!empty($tmp->type) and ($tmp->type == 'ldt' or $tmp->type == 'ldb' )){
					$tmp_css .= 'clear:both;';
				}	
				
				
					
			}
		}
		$tmp_css .= '}';

		$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' lable {';
		$tmp_css .= 'padding-right: 5px;';
		$tmp_css .= 'float: left;';
				
		if(!empty($this->tmp_settings->cgm_menu)){
			if(!empty($this->tmp_settings->cgm_menu->pos)){
			$tmp = $this->tmp_settings->cgm_menu->pos;

				if(!empty($tmp->fontSize)){
					$tmp_css .= 'font-size: '.$tmp->fontSize.'px;';
				}

				if(!empty($tmp->textColor)){
					$tmp_css .= 'color: '.$tmp->textColor.';';
				}	

				if(!empty($tmp->family)){
					$tmp_css .= 'font-family: '.$tmp->family.';';
				}	
				
				if(!empty($tmp->lineHeigh)){
					$tmp_css .= 'line-heigh: '.$tmp->lineHeigh.'px;';
				}	
							
				if(!empty($tmp->underline)){
					$tmp_css .= 'text-decoration: underline;';
				} else {
					$tmp_css .= 'text-decoration: none;';
				}	
				
				if(!empty($tmp->bold)){
					$tmp_css .= 'font-weight: bold;';
				} else {
					$tmp_css .= 'font-weight: normal;';
				}	
				
				if(!empty($tmp->italic)){
					$tmp_css .= 'font-style: italic;';
				} else {
					$tmp_css .= 'font-style: none;';
				}	
				
			}
		}
		$tmp_css .= '}';



		// --------------- menu pos end ---------------------------
		




		// --------------- menu default start -------------------------
		$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li a {';
		$tmp_css .= 'background-image: -moz-linear-gradient(center top , rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0));';
		$tmp_css .= 'padding: 0.4em 0.5em;';
		$tmp_css .= 'display: block;';
		
		if(!empty($this->tmp_settings->cgm_menu)){
			if(!empty($this->tmp_settings->cgm_menu->normal)){
			$tmp = $this->tmp_settings->cgm_menu->normal;

				if(!empty($tmp->fontSize)){
					$tmp_css .= 'font-size:'.$tmp->fontSize.'px;';
				}

				if(!empty($tmp->textColor)){
					$tmp_css .= 'color:'.$tmp->textColor.';';
				}	

				if(!empty($tmp->family)){
					$tmp_css .= 'font-family:'.$tmp->family.';';
				}			
							
				if(!empty($tmp->underline)){
					$tmp_css .= 'text-decoration: underline;';
				} else {
					$tmp_css .= 'text-decoration: none;';
				}	
				
				if(!empty($tmp->bold)){
					$tmp_css .= 'font-weight: bold;';
				} else {
					$tmp_css .= 'font-weight: normal;';
				}	
				
				if(!empty($tmp->italic)){
					$tmp_css .= 'font-style:italic;';
				} else {
					$tmp_css .= 'font-style:none;';
				}	
				
				$textshadowopacity = 0;
				$textshadowcolor = '#';
			
				if(!empty($tmp->textShadowOpacity)){
					$textshadowopacity = $tmp->textShadowOpacity;
				}

				if(!empty($tmp->textShadowColor)){
					$textshadowcolor = $tmp->textShadowColor;
				}
			
			if(!empty($textshadowopacity) and !empty($textshadowcolor)  and $textshadowcolor != '#'){
				$shadowrbg = $this->HexToRGB(str_replace('#','',$textshadowcolor));
				
				$tmp_css .= 'text-shadow: rgba('.$shadowrbg['r'].','.$shadowrbg['g'].','.$shadowrbg['b'].','.$textshadowopacity.') '; 
				
				
				$textShadowX = 0;
				$textShadowY = 0;
				$textShadowBlue = 0;
				
				if(!empty($tmp->textShadowX)){
					$textShadowX = $tmp->textShadowX;
				}
				
				if(!empty($tmp->textShadowY)){
					$textShadowY = $tmp->textShadowY;
				}
				
				if(!empty($tmp->textShadowBlue)){
					$textShadowBlue = $tmp->textShadowBlue;
				}
				
				$tmp_css .= $textShadowX.'px ';
				$tmp_css .= $textShadowY.'px ';
				$tmp_css .= $textShadowBlue.'px ';
				
				$tmp_css .= ';';
			}		

				
				
				if(!empty($tmp->backgroundColor)){
					$tmp_css .= 'background-color:'.$tmp->backgroundColor.';';
				}
			}
		}
		$tmp_css .= '}';	
		// --------------- menu default start-------------------------

		// --------------- menu hover start -------------------------
		$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li a:hover {';
		if(!empty($this->tmp_settings->cgm_menu)){
			if(!empty($this->tmp_settings->cgm_menu->hover)){
			$tmp = $this->tmp_settings->cgm_menu->hover;

				if(!empty($tmp->fontSize)){
					$tmp_css .= 'font-size:'.$tmp->fontSize.'px;';
				}

				if(!empty($tmp->textColor)){
					$tmp_css .= 'color:'.$tmp->textColor.';';
				}	

				if(!empty($tmp->family)){
					$tmp_css .= 'font-family:'.$tmp->family.';';
				}			
							
				if(!empty($tmp->underline)){
					$tmp_css .= 'text-decoration: underline;';
				} else {
					$tmp_css .= 'text-decoration: none;';
				}	
				
				if(!empty($tmp->bold)){
					$tmp_css .= 'font-weight: bold;';
				} else {
					$tmp_css .= 'font-weight: normal;';
				}	
				
				if(!empty($tmp->italic)){
					$tmp_css .= 'font-style:italic;';
				} else {
					$tmp_css .= 'font-style:none;';
				}	
				
				$textshadowopacity = 0;
				$textshadowcolor = '#';
			
				if(!empty($tmp->textShadowOpacity)){
					$textshadowopacity = $tmp->textShadowOpacity;
				}

				if(!empty($tmp->textShadowColor)){
					$textshadowcolor = $tmp->textShadowColor;
				}
			
			if(!empty($textshadowopacity) and !empty($textshadowcolor)  and $textshadowcolor != '#'){
				$shadowrbg = $this->HexToRGB(str_replace('#','',$textshadowcolor));
				
				$tmp_css .= 'text-shadow: rgba('.$shadowrbg['r'].','.$shadowrbg['g'].','.$shadowrbg['b'].','.$textshadowopacity.') '; 
				
				
				$textShadowX = 0;
				$textShadowY = 0;
				$textShadowBlue = 0;
				
				if(!empty($tmp->textShadowX)){
					$textShadowX = $tmp->textShadowX;
				}
				
				if(!empty($tmp->textShadowY)){
					$textShadowY = $tmp->textShadowY;
				}
				
				if(!empty($tmp->textShadowBlue)){
					$textShadowBlue = $tmp->textShadowBlue;
				}
				
				$tmp_css .= $textShadowX.'px ';
				$tmp_css .= $textShadowY.'px ';
				$tmp_css .= $textShadowBlue.'px ';
				
				$tmp_css .= ';';
			}	

				
				
				if(!empty($tmp->backgroundColor)){
					$tmp_css .= 'background-color:'.$tmp->backgroundColor.';';
				}
			}
		}
		$tmp_css .= '}';	
		// --------------- menu hover start-------------------------


		// --------------- menu select start -------------------------
		$tmp_css .= '.cgm_isotype_menu_'.$this->tmp_post_id.' li a.selected {';
		$tmp_css .= 'text-shadow: none;';
		
		if(!empty($this->tmp_settings->cgm_menu)){
			if(!empty($this->tmp_settings->cgm_menu->selected)){
			$tmp = $this->tmp_settings->cgm_menu->selected;

				if(!empty($tmp->fontSize)){
					$tmp_css .= 'font-size:'.$tmp->fontSize.'px;';
				}

				if(!empty($tmp->textColor)){
					$tmp_css .= 'color:'.$tmp->textColor.';';
				}	

				if(!empty($tmp->family)){
					$tmp_css .= 'font-family:'.$tmp->family.';';
				}			
							
				if(!empty($tmp->underline)){
					$tmp_css .= 'text-decoration: underline;';
				} else {
					$tmp_css .= 'text-decoration: none;';
				}	
				
				if(!empty($tmp->bold)){
					$tmp_css .= 'font-weight: bold;';
				} else {
					$tmp_css .= 'font-weight: normal;';
				}	
				
				if(!empty($tmp->italic)){
					$tmp_css .= 'font-style:italic;';
				} else {
					$tmp_css .= 'font-style:none;';
				}	
				
				
				$textshadowopacity = 0;
				$textshadowcolor = '#';
			
				if(!empty($tmp->textShadowOpacity)){
					$textshadowopacity = $tmp->textShadowOpacity;
				}

				if(!empty($tmp->textShadowColor)){
					$textshadowcolor = $tmp->textShadowColor;
				}
			
			if(!empty($textshadowopacity) and !empty($textshadowcolor)  and $textshadowcolor != '#'){
				$shadowrbg = $this->HexToRGB(str_replace('#','',$textshadowcolor));
				
				$tmp_css .= 'text-shadow: rgba('.$shadowrbg['r'].','.$shadowrbg['g'].','.$shadowrbg['b'].','.$textshadowopacity.') '; 
				
				
				$textShadowX = 0;
				$textShadowY = 0;
				$textShadowBlue = 0;
				
				if(!empty($tmp->textShadowX)){
					$textShadowX = $tmp->textShadowX;
				}
				
				if(!empty($tmp->textShadowY)){
					$textShadowY = $tmp->textShadowY;
				}
				
				if(!empty($tmp->textShadowBlue)){
					$textShadowBlue = $tmp->textShadowBlue;
				}
				
				$tmp_css .= $textShadowX.'px ';
				$tmp_css .= $textShadowY.'px ';
				$tmp_css .= $textShadowBlue.'px ';
				
				$tmp_css .= ';';
			}	

				if(!empty($tmp->backgroundColor)){
					$tmp_css .= 'background-color:'.$tmp->backgroundColor.';';
				}
			}
		}
		$tmp_css .= '}';	
		// --------------- menu default start-------------------------

		$tmp_css .= '.pp_social .facebook {';
		if(!empty($this->tmp_settings->cgm_pretty)){
			if(!empty($this->tmp_settings->cgm_pretty->facebook)){
				$tmp_css .=	'display:block !important;';
				$tmp_css .=	'float:left !important;';
			} else {
				$tmp_css .=	'display:none !important;';
			}
		}
		$tmp_css .= '}';
		
		
		$tmp_css .= '.pp_social .pinterest {';
		$tmp_css .= 'float: left;';
		$tmp_css .= 'margin-left: 5px;';
		if(!empty($this->tmp_settings->cgm_pretty)){
			if(!empty($this->tmp_settings->cgm_pretty->pinterest)){
				$tmp_css .=	'display:block !important;';
				$tmp_css .=	'float:left !important;';
			} else {
				$tmp_css .=	'display:none !important;';
			}
		}
		$tmp_css .= '}';
			
		$tmp_css .= '.pp_social .twitter {';
		$tmp_css .= 'float: left;';
		$tmp_css .= 'margin-left: 5px;';
		if(!empty($this->tmp_settings->cgm_pretty)){
			if(!empty($this->tmp_settings->cgm_pretty->tweet)){
				$tmp_css .=	'display:block !important;';
				$tmp_css .=	'float:left !important;';
			} else {
				$tmp_css .=	'display:none !important;';
			}
		}	
		$tmp_css .= '}';


		$tmp_css .= '.pp_social .google {';
		$tmp_css .= 'float: left;';
		$tmp_css .= 'margin-left: 5px;';
		
		if(!empty($this->tmp_settings->cgm_pretty)){
			if(!empty($this->tmp_settings->cgm_pretty->google)){
				$tmp_css .=	'display:block !important;';
				$tmp_css .=	'float:left !important;';
			} else {
				$tmp_css .=	'display:none !important;';
			}
		}	
		$tmp_css .= '}';



		// universal scroll
		
		$tmp_css .= '.cgm_isotype_bg_'.$this->tmp_post_id.' .universall_scroll {';

		$tmp_css .= 'bottom: 50px;';
		$tmp_css .= 'margin-left: 50%;';
		$tmp_css .= 'display: none;';
		$tmp_css .= 'position: absolute;';
		$tmp_css .= 'text-align: center;';
		$tmp_css .= 'width: 250px;';
		$tmp_css .= 'z-index:800;';		
		$tmp_css .= '}';
		
		
		
		
		$tmp_css .= '.cgm_isotype_bg_'.$this->tmp_post_id.' .universall_scroll div {';
		$tmp_css .= 	'margin-left: -125px;';
		$tmp_css .= 	'width: 250px;';

		if(!empty($this->tmp_settings->cgm_universallScroll)){
			$tmp = $this->tmp_settings->cgm_universallScroll;

			if(!empty($tmp->fontSize)){
				$tmp_css .= 'font-size:'.$tmp->fontSize.'px;';
			}

			if(!empty($tmp->textColor)){
				$tmp_css .= 'color:'.$tmp->textColor.';';
			}	

			if(!empty($tmp->family)){
				$tmp_css .= 'font-family:'.$tmp->family.';';
			}			
						
			if(!empty($tmp->underline)){
				$tmp_css .= 'text-decoration: underline;';
			} else {
				$tmp_css .= 'text-decoration: none;';
			}	
			
			if(!empty($tmp->bold)){
				$tmp_css .= 'font-weight: bold;';
			} else {
				$tmp_css .= 'font-weight: normal;';
			}	
			
			if(!empty($tmp->italic)){
				$tmp_css .= 'font-style:italic;';
			} else {
				$tmp_css .= 'font-style:none;';
			}	
			
			
			if(!empty($tmp->padding)){
				$tmp_css .= 'padding:'.$tmp->padding.';';
			}	
			
			if(!empty($tmp->borderColor)){
				$tmp_css .= 'border-color:'.$tmp->borderColor.';';
			}
			
			if(!empty($tmp->borderWidth)){
				$tmp_css .= 'border-width:'.$tmp->borderWidth.'px;';
			}
			
			if(!empty($tmp->borderRadius)){
				$tmp_css .= 'border-radius:'.$tmp->borderRadius.'px;';
			}		
			
			if(!empty($tmp->borderStyle)){
				$tmp_css .= 'border-style:'.$tmp->borderStyle.';';
			}
			
			if(!empty($tmp->backgroundColor)){
					$tmp_css .= 'background-color:'.$tmp->backgroundColor.';';
			}
			
			
			$textshadowopacity = 0;
			$textshadowcolor = '#';
		
			if(!empty($tmp->textShadowOpacity)){
				$textshadowopacity = $tmp->textShadowOpacity;
			}

			if(!empty($tmp->textShadowColor)){
				$textshadowcolor = $tmp->textShadowColor;
			}
			
			if(!empty($textshadowopacity) and !empty($textshadowcolor) and $textshadowcolor != '#'){
				$shadowrbg = $this->HexToRGB(str_replace('#','',$textshadowcolor));
				
				
				
				$tmp_css .= 'text-shadow: rgba('.$shadowrbg['r'].','.$shadowrbg['g'].','.$shadowrbg['b'].','.$textshadowopacity.') '; 
				
				
				$textShadowX = 0;
				$textShadowY = 0;
				$textShadowBlue = 0;
				
				if(!empty($tmp->textShadowX)){
					$textShadowX = $tmp->textShadowX;
				}
				
				if(!empty($tmp->textShadowY)){
					$textShadowY = $tmp->textShadowY;
				}
				
				if(!empty($tmp->textShadowBlue)){
					$textShadowBlue = $tmp->textShadowBlue;
				}
				
				$tmp_css .= $textShadowX.'px ';
				$tmp_css .= $textShadowY.'px ';
				$tmp_css .= $textShadowBlue.'px ';
				
				$tmp_css .= ';';
			}	
		}
		$tmp_css .= '}';
	
	
		$upload_dir = wp_upload_dir();
		
		if(!is_dir($upload_dir['basedir'].'/'.$this->tmp_folder)){
			mkdir($upload_dir['basedir'].'/'.$this->tmp_folder, 0777);
			
		}
		
		
		
		$cssFile = COMPLETE_GALLERY_PATH.'css/captions.css';
		$fh = fopen($cssFile, 'r');
		$tmp_css .= fread($fh,filesize($cssFile));
		fclose($fh);	

		$myFile = $upload_dir['basedir'].'/'.$this->tmp_folder.'/'.$this->tmp_post_id.$this->tmp_file;
		$fh = fopen($myFile, 'w') or die("can't open file");

		fwrite($fh,  $tmp_css);
		fclose($fh);

		return 	'';	
		
	}
	
	function HexToRGB($hex) {
		$hex = ereg_replace("#", "", $hex);
		$color = array();
 
		if(strlen($hex) == 3) {
			$color['r'] = hexdec(substr($hex, 0, 1) . $r);
			$color['g'] = hexdec(substr($hex, 1, 1) . $g);
			$color['b'] = hexdec(substr($hex, 2, 1) . $b);
		} else if(strlen($hex) == 6) {
			$color['r'] = hexdec(substr($hex, 0, 2));
			$color['g'] = hexdec(substr($hex, 2, 2));
			$color['b'] = hexdec(substr($hex, 4, 2));
		}
 
		return $color;
	}
	
	function wordlimit($string, $length = 50, $ellipsis = "...")
	{
	   $words = explode(' ', $string);
	   if (count($words) > $length)
	       return implode(' ', array_slice($words, 0, $length)) . $ellipsis;
	   else
	       return $string;
	}
}

$cgm_isotope_generator = new cgm_isotope_generator_class();
?>