// aktivste color wheel
function uh_activate_farbtastic(){
	jQuery(document).ready(function($){
		jQuery('.pop-farbtastic').each(function(i,o){
			$(this).farbtastic($(this).attr('rel')).hide();
		});	
		jQuery('.uh_post_color').click(function(e){
			var helper = $(this).parent().find('.pop-farbtastic');
			if(helper.is(':visible')){
				helper.slideUp();
				jQuery(this).addClass('show-colorpicker').removeClass('hide-colorpicker');
				if(uh_call_preview_function != ''){
					window[uh_call_preview_function]();
				}

			}else{
				helper.slideDown();
				jQuery(this).addClass('hide-colorpicker').removeClass('show-colorpicker');
			}
		});
		
		jQuery('.uh_post_color').mousedown(function(e){jQuery(this).parent().find('input').trigger('focus');});
	});
}

function uh_check_color(tmp_this){

var strings = jQuery(tmp_this).val();
var string = '';
var i = 0;
while (i <= strings.length){
    character = strings.charAt(i);
	if(isNaN(character * 1) && character != 'a' && character != 'b' && character != 'c' && character != 'd' && character != 'e' && character != 'f'){
	
		if(character == '#' && i == 0 ) {
			character = '#';
		} else {
			character = 'f';
		}
	}
	
	if(i < 7){
		string += character;
	}
    i++;
}



	if(string == '' || string == '#'){
		jQuery(tmp_this).val('#');
	
		jQuery(tmp_this).css('backgroundColor','#ffffff')
	} else {
		jQuery(tmp_this).val(string);
	}
}



function uh_show_hide_group(tmp_this,tmp_id){
	jQuery(document).ready(function($){
		if(tmp_id != ''){
			var cgw_group = jQuery('#group_'+tmp_id).is(":hidden");
			
			jQuery.post(uh_form_URL+'frames/save_group_visible.php',{tmp_id:tmp_id,cgw_group_hide:cgw_group});	
			
			if(jQuery(tmp_this).find('div').attr('class') == 'uh_arrow_up'){
				jQuery(tmp_this).find('div').attr('class','uh_arrow_down');
			} else {
				jQuery(tmp_this).find('div').attr('class','uh_arrow_up');
			}
			
			
			if(cgw_group){
				jQuery('#group_'+tmp_id).slideDown();
			} else {
				jQuery('#group_'+tmp_id).slideUp();
			}
		}
	});
}

//check numbers
function uh_numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
  		key = window.event.keyCode;
	else if (e)
   		key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || 
    	(key==9) || (key==13) || (key==27) )
   		return true;

	// numbers
	else if (((".-0123456789").indexOf(keychar) > -1))
   		return true;

	// decimal point jump
	else if (dec && (keychar == "."))
   	{
   		myfield.form.elements[dec].focus();
   		return false;
   	}
	else
   		return false;
}


//var uh_timer_chekker;

// fade ud help
/*function uh_hide_help(evt_object){
	jQuery(document).ready(function($){
		if(uh_timer_chekker){
			clearTimeout(uh_timer_chekker);
		}
		
		jQuery('#referent_pointer_'+jQuery(evt_object).attr('name')).parent().parent().fadeTo('slow', 0 , function(){
			jQuery(this).hide();
		});
	});
}*/

// fade in help
function uh_activate_help(evt_object,evt_title,evt_txt){
	jQuery(document).ready(function($){
		var content = '';
		if(evt_txt && evt_title){
			content += '<span class="uh_help_h3">'+evt_title+'</span>';
			content += '<p>'+evt_txt+'</p><div id="referent_pointer_'+jQuery(evt_object).attr('ref')+'" style="display:none"></div>';
			$('.wp-pointer').fadeTo('slow', 0).hide();
           	$(evt_object).pointer({
   			    content: content,
        		position: {
        			edge:'left',
					my: 'left top',
					at: 'right top',
					offset: '1 -55px'
        		}
      		}).pointer('open');
      			
			jQuery('#referent_pointer_'+jQuery(evt_object).attr('ref')).parent().parent().css({ opacity: 0 });
      		jQuery('.wp-pointer-arrow').hide();
      		jQuery('#referent_pointer_'+jQuery(evt_object).attr('ref')).parent().parent().fadeTo('slow', 1);
		}
	});
}