<?php

class uh_form_structure { 

	var $type = '';
	var $change_function = '';
	var $filelocation_url = '';
	var $group_show_hide = '';

	function uh_form_structure($type = 'default' , $change_function = ''){
		$filelocation_array = explode('wp-content',dirname(__FILE__));
		$filelocation_url = trailingslashit(get_option('siteurl')) . 'wp-content' . $filelocation_array[1].'/'; 
		$filelocation_url = str_replace("\\","/",$filelocation_url);//windows based server issue.
		
		$this->filelocation_url = $filelocation_url;
		
     	wp_register_style( 'uh_formsetting_css', $filelocation_url.'formsettings.css');
    	wp_enqueue_style( 'uh_formsetting_css' );
    	wp_register_script( 'uh_formsettings_js',  $filelocation_url.'uh_formsettings.js');
		wp_enqueue_script( 'uh_formsettings_js' );

		$current_user = wp_get_current_user(); 
		$this->group_show_hide = get_user_meta($current_user->ID,'cgw_show_hide_settings',true);

		$this->type = $type;
		$this->change_function = $change_function;
	}

	function create_form($data,$key,$saved_data = '',$saved_data2 = ''){
		if(!empty($data['help'])){
			$data['help_action']['onMouseOver'] = 'uh_activate_help(this,\''.$data['title'].'\',\''.$data['help'].'\')';
			//$data['help_action']['onMouseOut'] = 'uh_hide_help(this)';
		}	
	
		if(empty($data['extra']['onChange']) && !empty($this->change_function)){
			$data['extra']['onChange'] = $this->change_function.'();';
		}	

		switch ($data['type']) {
    		case 'title':
        		return $this->create_form_title($data,$key,$saved_data);
        		break;
    		case 'number':
       			return $this->create_form_start($data).$this->create_form_number($data,$key,$saved_data).$this->create_help($data).$this->create_form_end();
       			break;
    		case 'dropdown':
       			return $this->create_form_start($data).$this->create_form_dropdown($data,$key,$saved_data).$this->create_help($data).$this->create_form_end();
       			break;
    		case 'string':
       			return $this->create_form_start($data).$this->create_form_string($data,$key,$saved_data).$this->create_help($data).$this->create_form_end();
       			break;
    		case 'checkbox':
       			return $this->create_form_start($data).$this->create_form_checkbox($data,$key,$saved_data).$this->create_help($data).$this->create_form_end();
       			break;
     		case 'textarea':
       			return $this->create_form_start($data).$this->create_form_textarea($data,$key,$saved_data).$this->create_help($data).$this->create_form_end();
       			break;
    		case 'boolean':
       			return $this->create_form_start($data).$this->create_form_boolean($data,$key,$saved_data).$this->create_help($data).$this->create_form_end();
       			break;	
    		case 'color':
       			return $this->create_form_start($data).$this->create_form_color($data,$key,$saved_data).$this->create_help($data).$this->create_form_end();
       			break;	
    		case 'div_start':
       			return $this->create_form_div_start($data,$key,$saved_data);
       			break;	
    		case 'div_break':
       			return $this->create_form_div_break($data,$key,$saved_data);
       			break;	
    		case 'div_end':
       			return $this->create_form_div_end($data,$key,$saved_data);
       			break;	
    		case 'groupStart':
       			return $this->create_groupStart($data,$key,$saved_data);
       			break;	
    		case 'groupEnd':
       			return $this->create_groupEnd($data,$key,$saved_data);
       			break;
       		default:
      			return '';
		}
	}
	
	function create_form_title($data,$key,$saved_data){
		$tmp = '<div class="uh_post_div" onClick="uh_show_hide_group(this,\''.$data['ID'].'\');" style="cursor:pointer" >';
		$tmp .= '<h4 ';
		
		if(!empty($data['ID'])){
			$tmp .= 'id="'.$data['ID'].'" ';
		}
		
		$tmp .='>'.$data['title'].'</h4>';

		
		$tmp .= '<div ';
		
		if(empty($this->group_show_hide[$data['ID']]) || $this->group_show_hide[$data['ID']]== 'false'){
			$tmp .= 'class="uh_arrow_down" ';
		} else {
			$tmp .= 'class="uh_arrow_up" ';
		}
		$tmp .= '></div>';
		$tmp .= '</div>';
		return $tmp;
	}
	
	function create_groupStart($data,$key,$saved_data){
		$tmp = '<div class="uh_groupClass" ';
		
		if(empty($this->group_show_hide[$data['ID']]) || $this->group_show_hide[$data['ID']]== 'false'){
			$tmp .= 'style="display:none;" ';
		}
		
		if(!empty($data['ID'])){
			$tmp .= 'id="group_'.$data['ID'].'"';
		}		
		$tmp .= ' >';

		return $tmp;
	}
	function create_groupEnd($data,$key,$saved_data){
		$tmp = '</div>';
		return $tmp;
	}

	function create_form_string($data,$key,$saved_data){
	
		if(empty($saved_data)){
			if(!empty($data['default'])){
				$saved_data = $data['default'];
			}			
		}
	
	
		$tmp = '<input type="text" class="uh_post_input" name="'.$this->type.'_'.$data['name'].'" value="'.$saved_data.'" ';
		if(!empty($data['extra'])){		
			$tmp .= $this->create_form_extra($data['extra']);
		}
		$tmp .=' >';
		return $tmp;
	}
	
	function create_form_checkbox($data,$key,$saved_data){
	
		if(empty($saved_data)){
			if(!empty($data['default'])){
				$saved_data = $data['default'];
			}			
		}
	
		$tmp = '<input type="checkbox" class="uh_post_checkbox" name="'.$this->type.'_'.$data['name'].'" value="true" ';
		
		if($saved_data == 'true'){
			$tmp .= ' checked = "checked" '; 
		}
		if(!empty($data['extra'])){
			$tmp .= $this->create_form_extra($data['extra']);
		}
		$tmp .=' >';
		return $tmp;
	}	
	
	function create_form_textarea($data,$key,$saved_data){
		if(empty($saved_data)){
			if(!empty($data['default'])){
				$saved_data = $data['default'];
			}			
		}
	
		$tmp = '<textarea class="uh_post_textarea" name="'.$this->type.'_'.$data['name'].'" ';
		if(!empty($data['extra'])){
			$tmp .= $this->create_form_extra($data['extra']);
		}
		$tmp .=' >'.$saved_data.'</textarea>';
		return $tmp;
	}
	
	

	function create_form_number($data,$key,$saved_data){
		if(empty($saved_data)){
			if(!empty($data['default'])){
				$saved_data = $data['default'];
			}			
		}
	
		$tmp = '<input type="text" onKeyPress="return uh_numbersonly(this, event)" class="uh_post_input_numbers" name="'.$this->type.'_'.$data['name'].'" value="'.$saved_data.'" ';
		if(!empty($data['extra'])){
			$tmp .= $this->create_form_extra($data['extra']);
		}
		$tmp .=' >';		
		
		return $tmp;
	}

	function create_form_dropdown($data,$key,$saved_data){
		$tmp = '';
		if(empty($saved_data)){
			if(!empty($data['default'])){
				$saved_data = $data['default'];
			}			
		}
		$tmp .= '<select  id="'.$this->type.'_'.$data['name'].'" name="'.$this->type.'_'.$data['name'].'" class="uh_post_dropdown" ';
		if(!empty($data['extra'])){
			$tmp .= $this->create_form_extra($data['extra']);
		}
		$tmp .= ' >';
		
		foreach($data['list'] as  $key => $list_tmp){
			$tmp 	.='<option value="'.$key.'" ';
			
			if($key == $saved_data && $key != '---' && $key != '----'){
				$tmp 	.= ' selected="selected" ';
			}
			
			if( ($key == '---' || $key == '----') && !empty($key)){
			$tmp 	.= ' disabled="disabled" ';
			}
			$tmp 	.='>'.__($list_tmp,$this->type).'</option>';
		}
		
		$tmp .='</select>';
		return $tmp;
		
	}
	
	function create_form_boolean($data,$key,$saved_data){
		if(empty($saved_data)){
			if(!empty($data['default'])){
				$saved_data = $data['default'];
			}			
		}
	
		if(!empty($data['extra']['onChange'])){
			$data['extra2']['onChange'] = $data['extra']['onChange'];
		}

		$data['extra']['onChange'] = '';	
	
		$tmp = '<div style="margin-top: 4px;"';
		$tmp .= '>';
		$tmp .= '<input type="radio" class="uh_post_radio" name="'.$this->type.'_'.$data['name'].'" value="true" ';
		if(!empty($data['extra2'])){
			$tmp .= $this->create_form_extra($data['extra2']);
		}
		
		if($saved_data == 'true'){
			$tmp .= ' checked="checked" ';
		}
		$tmp .= ' > <lable>'.__('Yes',$this->type ).'</lable> ';
		$tmp .= '<input type="radio" class="uh_post_radio" name="'.$this->type.'_'.$data['name'].'" value="false" ';
		if(!empty($data['extra2'])){
			$tmp .= $this->create_form_extra($data['extra2']);
		}
		
		if($saved_data == 'false' || empty($saved_data)){
			$tmp .= ' checked="checked" ';
		}
		$tmp .= ' > <lable>'.__('No',$this->type ).'</lable></div>';
		return $tmp;
	}
	
	function create_form_color($data,$key,$saved_data){
		if(empty($saved_data)){
			if(!empty($data['default'])){
				$saved_data = $data['default'];
			} else {
				$saved_data = "#";
			}
		}
	
		$tmp = 	 '<div class="farbtastic-holder">';
		$tmp .= 		'<input type="text" onKeyUp="uh_check_color(this);" value="'.$saved_data.'" class="uh_post_color show-colorpicker" name="'.$this->type.'_'.$data['name'].'" id="cs_menu_current_font_color'.$key.'" ';
		if(!empty($data['extra'])){
			$tmp .= $this->create_form_extra($data['extra']);
		}
		$tmp .=' >';		
		$tmp .= 		'<div class="pop-farbtastic" rel="#cs_menu_current_font_color'.$key.'" id="pop-farbtastic-'.$key.'" style="float:left;display: block;">';
		$tmp .= 			'<div class="farbtastic">';
		$tmp .=					'<div class="color" style="background-color: rgb(255, 0, 0);"></div>';
		$tmp .=					'<div class="wheel"></div>';
		$tmp .=					'<div class="overlay"></div>';
		$tmp .=					'<div class="h-marker marker" style="left: 97px; top: 13px;"></div>';
		$tmp .=					'<div class="sl-marker marker" style="left: 147px; top: 147px;"></div>';
		$tmp .=				'</div>';
		$tmp .=			'</div>';
		$tmp .=		'</div>';
		return $tmp;
	}

	function create_form_start($data){
		$tmp = '<div class="uh_post_div">';
		$tmp .= '<table width=100%><tr>';
		

		$tmp .= '<td valign="top">';
		$tmp .= '<lable class="uh_post_lable" ';
		if(!empty($data['extra_lable'])){
			$tmp .= $this->create_form_extra($data['extra_lable']);
		}
		$tmp .= ' >'.$data['title'].' :</lable>';
		$tmp .= '</td><td valign="top">';
		if(!empty($data['help_action']['onMouseOver'])){
			$tmp .= '<img ref="'.$this->type.'_'.$data['name'].'" onClick="'.$data['help_action']['onMouseOver'].'" src="'.$this->filelocation_url.'images/question.png" style="margin-right: 2px;margin-top: 3px;cursor:pointer">';
		}
		
		
		$tmp .= '</td><td valign="top" width="100%">';
		return $tmp;
	}
	function create_form_end(){
		$tmp = '</td></tr></table>';
		$tmp .='</div>';
		return $tmp;
	}

	function create_form_extra($extra){
		$tmp = '';
		if(!empty($extra)){
			foreach($extra as $key_tmp => $data_tmp){
				$tmp .= ' '.$key_tmp.'="'.$data_tmp.'" ';
			}
		}
		return $tmp;
	}
	
	function create_form_div_start($data,$key,$saved_data){
		$tmp = '<script type="text/javascript"> var uh_form_URL = \''.$this->filelocation_url .'\';;var uh_call_preview_function = \''.$this->change_function .'\'</script>';
		$tmp .= '<div class="uh_post_div_col" ';
		if(!empty($data['extra'])){
			$tmp .= $this->create_form_extra($data['extra']);
		}
		$tmp .= ' >';
		return $tmp;
	}
	
	function create_form_div_break($data,$key,$saved_data){
		$tmp = '</div><div class="uh_post_div_col" ';
		if(!empty($data['extra'])){
			$tmp .= $this->create_form_extra($data['extra']);
		}
		$tmp .= ' >';
		return $tmp;
	}
	
	function create_form_div_end($data,$key,$saved_data){

		$tmp = '</div><div class="uh_post_div_clear"></div>';
		return $tmp;
	}
	
	function create_help($data){
		$tmp = '';
		return $tmp;
	}		
}
?>
