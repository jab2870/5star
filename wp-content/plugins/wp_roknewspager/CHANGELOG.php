<?php
/**
 * @package		RocketTheme
 * @subpackage	roknewspager
 * @version   1.18 November 13, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 */
?>

1. Copyright and disclaimer
----------------


2. Changelog
------------
This is a non-exhaustive changelog for RokNewsPager, inclusive of any alpha, beta, release candidate and final versions.

Legend:

* -> Security Fix
# -> Bug Fix
+ -> Addition
^ -> Change
- -> Removed
! -> Note

----------- 1.15 Release [] -----------
# Fixed plugin path on certain Multi Site installations

----------- 1.14 Release [] -----------
^ RokNewsPager should be now working fine with the WordPress Multi Site installations