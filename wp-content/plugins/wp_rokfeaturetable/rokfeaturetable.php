<?php

/**
 * @version   1.8 November 13, 2012
 * @author    RocketTheme, LLC http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
/*
Plugin Name: RokFeatureTable
Plugin URI: http://www.rockettheme.com
Description: RokFeatureTable offers a stylistic and user friendly solution for displaying tabular based data. It is perfect for price or product comparisons, or essentially, any type of data display that suits a matrix format.
Author: RocketTheme, LLC
Version: 1.8
Author URI: http://www.rockettheme.com
License: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// Globals

global $rokfeaturetable_plugin_path, $rokfeaturetable_plugin_url, $platform;
if(!is_multisite()) {
	$rokfeaturetable_plugin_path = dirname($plugin);
} else {
	if(!empty($network_plugin)) {
		$rokfeaturetable_plugin_path = dirname($network_plugin);
	} else {
		$rokfeaturetable_plugin_path = dirname($plugin);
	}
}
$rokfeaturetable_plugin_url = WP_PLUGIN_URL.'/'.basename($rokfeaturetable_plugin_path);

require(dirname(__FILE__).'/rokbrowser_check.php');

$browser_check = new RokBrowserCheck;
$platform = $browser_check->platform;
$widgetID = 0;

// Widget

class RokFeatureTable extends WP_Widget {

	// RocketTheme RokFeatureTable Widget
	// Port by Jakub Baran

	static $plugin_path;
	static $plugin_url;
	var $short_name = 'rokfeaturetable';
	var $long_name = 'RokFeatureTable';
	var $description = 'RocketTheme RokFeatureTable Widget';
	var $css_classname = 'widget_rokfeaturetable';
	var $width = 200;
	var $height = 400;
	var $counter = 0;
	var $_defaults = array(
		'title' => '',
		'builtin_css' => '1',
		'highlight-col' => '1',
		'data-col1' => 'row-1::row-1|Row 1 row-1-sub::row-1 row-1-link::row-1',
		'data-col2' => '',
		'data-col3' => '',
		'data-col4' => '',
		'data-col5' => '',
		'data-col6' => ''
	);

	function init() {
		global $platform;    	
		if($platform != 'iphone') :
			register_widget("RokFeatureTable");
		endif;
	}
	
	function rokfeaturetable_admin_scripts() {
		global $rokfeaturetable_plugin_url;
		
		wp_register_style('rokfeaturetable_admincss', $rokfeaturetable_plugin_url.'/admin/css/rokfeaturetable.css');
		wp_register_script('rokfeaturetable_adminjs', $rokfeaturetable_plugin_url.'/admin/js/rokfeaturetable.js');
		
		if(is_admin() && substr($_SERVER['PHP_SELF'], '-11', '11') == 'widgets.php') :
			wp_enqueue_style('rokfeaturetable_admincss');
			wp_enqueue_script('mootools.js');	
			wp_enqueue_script('rokfeaturetable_adminjs');
		endif;
	}
	
	function rokfeaturetable_frontend_scripts($instance) {
		global $rokfeaturetable_plugin_url;
		
		wp_register_style('rokfeaturetable_frontcss', $rokfeaturetable_plugin_url.'/tmpl/css/rokfeaturetable.css');
		wp_register_script('rokfeaturetable_frontjs', $rokfeaturetable_plugin_url.'/tmpl/js/rokfeaturetable.js');
		
		if(!is_admin()) :
			if($instance['builtin_css'] == '1') :
				wp_enqueue_style('rokfeaturetable_frontcss');
			endif;
			wp_enqueue_script('mootools.js');	
			wp_enqueue_script('rokfeaturetable_frontjs');
		endif;
	}
	
	function render($args, $instance) {
		global $gantry, $more, $post, $rokfeaturetable_plugin_path, $rokfeaturetable_plugin_url;
		
		add_action('wp_head', $this->rokfeaturetable_frontend_scripts($instance));
		
		ob_start();
		echo $args['before_widget'];
		if($instance['title'] != '')
		echo $args['before_title'] . $instance['title'] . $args['after_title']; 		
		
		if(file_exists(TEMPLATEPATH.'/html/plugins/wp_rokfeaturetable/default.php')) :	
			require(TEMPLATEPATH.'/html/plugins/wp_rokfeaturetable/default.php');
		else :
			require($rokfeaturetable_plugin_path.'/tmpl/default.php'); 
		endif;	
		
		echo $args['after_widget'];
		echo ob_get_clean();
	}
	
	function displayData($instance) {

		$data = array();
	
		for ($x=1;$x<=6;$x++) {
			$col = $instance['data-col'.$x];
			if ($col) {
				$lines = explode("\n",$col);
				foreach ($lines as $line) {
					$previous = isset($row) ? $row[0] : null;
					$row = explode("::",$line);
					if (isset($row[1])) {
						if (preg_match("/\-sub/", $row[0])) {
							$previous = str_replace("-sub", '', str_replace("-link", '', str_replace("-classes", '', $previous)));
							$prev = $data[$x][$previous];
							$cur_data = $this->getLineData($row[1]);
							$prev->sub = $cur_data->data;
						} elseif (preg_match("/\-link/", $row[0])) {
							$previous = str_replace("-sub", '', str_replace("-link", '', str_replace("-classes", '', $previous)));
							$prev = $data[$x][$previous];
							$cur_data = $this->getLineData($row[1]);
							$prev->link = $cur_data->data;
						} elseif (preg_match("/\-classes/", $row[0])) {
							$previous = str_replace("-sub", '', str_replace("-link", '', str_replace("-classes", '', $previous)));
							$prev = $data[$x][$previous];
							$cur_data = $this->getLineData($row[1]);
							$prev->classes = $cur_data->data;
						}
						else {
							$data[$x][$row[0]] = $this->getLineData($row[1]);							
						}
						
						$previous = str_replace("-sub", '', str_replace("-link", '', str_replace("-classes", '', $previous)));
					}
				}
			
			}
		}
		
		return ($data);
	}
	
	function getLineData($line) {
		$linebits = new stdclass;
		if (strpos($line,"|")!==false) {
			$bits = explode("|",$line);
			$linebits->style = $bits[0];
			$linebits->data = $bits[1];
		} else {
			$linebits->style = "";
			$linebits->data = $line;
		}
		return $linebits;
	}
	
	function form($instance) {
		global $rokfeaturetable_plugin_path, $rokfeaturetable_plugin_url;
		$defaults = $this->_defaults;
		$widgetID = $this->id;
		$instance = wp_parse_args((array) $instance, $defaults);
		foreach ($instance as $variable => $value)
		{
			$$variable = self::_cleanOutputVariable($variable, $value);
			$instance[$variable] = $$variable;
		}
		$this->_values = $instance;
		
		$this->data = $this->getData($instance);	
		$this->actions = array("class" => "classname", "sub" => "subline text", "link" => 'cell link', "style" => "");
		
		ob_start(); ?>

		<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'rokfeaturetable'); ?>:&nbsp;
		<input style="width: 165px;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $instance['title']; ?>" /></label>
		<br /><br />
		<label for="<?php echo $this->get_field_id('highlight-col'); ?>"><?php _e('Highlight Column', 'rokfeaturetable'); ?></label>&nbsp;
		<select id="<?php echo $this->get_field_id('highlight-col'); ?>" name="<?php echo $this->get_field_name('highlight-col'); ?>">
			<option value="0"<?php selected( $instance['highlight-col'], '0' ); ?>><?php _e('None', 'rokfeaturetable'); ?></option>
			<option value="1"<?php selected( $instance['highlight-col'], '1' ); ?>><?php _e('Column 1', 'rokfeaturetable'); ?></option>
			<option value="2"<?php selected( $instance['highlight-col'], '2' ); ?>><?php _e('Column 2', 'rokfeaturetable'); ?></option>
			<option value="3"<?php selected( $instance['highlight-col'], '3' ); ?>><?php _e('Column 3', 'rokfeaturetable'); ?></option>
			<option value="4"<?php selected( $instance['highlight-col'], '4' ); ?>><?php _e('Column 4', 'rokfeaturetable'); ?></option>
			<option value="5"<?php selected( $instance['highlight-col'], '5' ); ?>><?php _e('Column 5', 'rokfeaturetable'); ?></option>
			<option value="6"<?php selected( $instance['highlight-col'], '6' ); ?>><?php _e('Column 6', 'rokfeaturetable'); ?></option>
		</select>
		<br /><br />
		<?php _e('Built-in CSS', 'rokfeaturetable'); ?>
		<input id="<?php echo $this->get_field_id('builtin_css'); ?>1" type="radio" value="1" name="<?php echo $this->get_field_name('builtin_css'); ?>" <?php if($instance['builtin_css'] == 1) echo 'checked="checked"'; ?>/>
		<label for="<?php echo $this->get_field_id('builtin_css'); ?>1"><?php _e('Yes', 'rokfeaturetable'); ?></label>&nbsp;&nbsp;
		<input id="<?php echo $this->get_field_id('builtin_css'); ?>0" type="radio" value="0" name="<?php echo $this->get_field_name('builtin_css'); ?>" <?php if($instance['builtin_css'] == 0) echo 'checked="checked"'; ?>/>
		<label for="<?php echo $this->get_field_id('builtin_css'); ?>0"><?php _e('No', 'rokfeaturetable'); ?></label>
		<br /><br />
		
		<?php echo $this->getTemplates();
		
		$render = "";
		$render .= "<br /><br />";
		$render .= "<div class='rft-wrapper'>\n";
		$render .= "	<div class='status'>\n";
		$render .= "		Current Table Layout: <span id='no_of_rows-".$this->id."'>1</span> x <span id='no_of_columns-".$this->id."'>1</span>\n";
		$render .= "	</div>\n";
		$render .= "	<div id='rft-settings-".$this->id."' class='settings'></div>\n";
		$render .= " 	<div class='clear'></div>\n";
		$render .= "</div>\n";
		
		$render .= "<div id='rft-layout-".$this->id."' class='rft-layout'>\n";
		
		// tabs
		$render .= "	<div id='tabs-".$this->id."' class='rft-tabs'>\n";
	for ($i = 0; $i < $this->counter; $i++)
		$render .= "		<div class='tab tab-".($i+1)."".(!$i ? ' active' : '')."'><div class='inner-tab'><span class='column-title'>COL ".($i+1)."</span><span class='delete-tab'></span></div><span class='remove-column'>remove</span></div>\n";
		$render .= "		<div class='add-tab' ".($this->counter == 6 ? 'style=\'display: none;\'' : '')."><span> + </span></div>\n";
		$render .= "	</div>\n";

		// panels
		$render .= "	<div id='panels-".$this->id."' class='rft-panels'>\n";
	for ($i = 0; $i < $this->counter; $i++) {
		$render .= "		<div class='panel panel-".($i+1)."".(!$i ? ' active': '')."'>\n";
		// rows
		$data = $this->data[$i]['parsed'];
		for ($j = 0; $j < count($data); $j++) {
			$render .= $this->createRow($data, $j);
		}
		
		if (!count($data)) $render .= $this->createRow($data, 0);
		
		$render .= "		</div>\n";
	}
		$render .= "	</div>\n";
		$render .= "</div>";

		// real fields for storing data
		$render .= "<div id='rft-data-".$this->id."' class='rft-data'>\n";
		foreach($this->data as $data) {
			$render .= "	<textarea id='".$this->get_field_id($data['name'])."' name='".$this->get_field_name($data['name'])."' class='text_area' rows='10' cols='50'>".$data['value']."</textarea>\n";
		}
		$render .= "</div>\n";
		$render .= "<script type='text/javascript'>window.addEvent('domready', function(){ 
			if (!'".$this->id."'.contains('__i__')) new RFT({id: '-".$this->id."'}); 
		});</script>";
		
		echo $render;
	
		echo ob_get_clean();
	}
	
	function getData($instance) {
		$data = array();
		$this->counter = 0;
		
		foreach($instance as $name => $val) {
			if($name == 'data-col1' || $name == 'data-col2' || $name == 'data-col3' || $name == 'data-col4' || $name == 'data-col5' || $name == 'data-col6') :
				$value = (empty($val)) ? null : $val;
				array_push($data, array('name' => $name, 'value' => $value, 'parsed' => $this->parse($value)));
				if ($value != null) $this->counter += 1;
			endif;
		}
		
		if (!$this->counter) $this->counter = 1;
		
		return $data;
	}
	
	function createRow($data, $j) {			
			$key = $this->getCurrentKey($data, $j);
			$values = $this->getCurrent($data, $j);
			
			$value = $this->getValue($values);
			if ($value == $this->actions['class']) $value = 'Row ' . ($j+1);

			$render = "		<div class='row row-".($j+1)."'>\n";
			$render .= "			<span class='row-title'>ROW ".($j+1)."</span>\n";
			$render .= "			<div class='input-wrapper'>\n";
			$render .= "				<input type='text' value='".$value."' />\n";
		// actions
		foreach($this->actions as $action => $default) {
			if (isset($data[$key]))	$action_value = $this->getAction($action, $key, $data[$key]);
			if (empty($action_value) || !isset($data[$key])) $action_value = $default;
			if ($action_value == $default && $key == 'class') $action_value = 'row-1';
			if (isset($data[$key]['classes']) && $action == 'class') $action_value .= " ".$data[$key]['classes'];
			
			$render .= "				<div class='action-input-wrapper action-".$action."'><input type='text' name='".$action."' class='action-input' rel='".$default."' value='".$action_value."' /></div>\n";
		}
			$render .= "			</div>\n";
			$render .= "			<span class='row-action add'><span>+</span></span>\n";
			$render .= "			<span class='row-action remove'><span>-</span></span>\n";
			$render .= "			<span class='row-action action-button class'><span>class</span></span>\n";
			$render .= "			<span class='row-action action-button sub'><span>subline</span></span>\n";
			$render .= "			<span class='row-action action-button link'><span>link</span></span>\n";
			$render .= "			<span class='row-action action-button style'><span>style</span></span>\n";
			$render .= "		</div>\n";
			
			return $render;
	}
	
	function parse($value) {
		$rows = array();
		$previous = '';
		$lines = explode("\n", $value);

		foreach($lines as $line) {
			$cell = explode("::", $line);
			$previous = (isset($prefix)) ? $prefix : false;
			$previous = str_replace('-sub', '', str_replace('-link', '', str_replace('-classes', '', $previous)));
			
			$prefix = $cell[0];
			$text = (!empty($cell[1]) ? $this->parseLine($cell[1]) : '');
			$value = (is_array($text) ? $text['value'] : $text);
			
			if (!empty($value)) {
				if (substr($prefix, -4) == '-sub') {
					$previous_cell = $rows[$previous];
					if (is_array($previous_cell)) $rows[$previous]['sub'] = $value;
					else if (empty($previous_cell)) $rows[$previous] = array('value' => '', 'sub' => $value);
					else $rows[$previous] = array('value' => $previous_cell, 'sub' => $value);
				}
				else if (substr($prefix, -5) == '-link') {
					$previous_cell = $rows[$previous];
					if (is_array($previous_cell)) $rows[$previous]['link'] = $value;
					else if (empty($previous_cell)) $rows[$previous] = array('value' => '', 'link' => $value);
					else $rows[$previous] = array('value' => $previous_cell, 'link' => $value);
				}
				else if (substr($prefix, -8) == '-classes') {
					$previous_cell = $rows[$previous];
					if (is_array($previous_cell)) $rows[$previous]['classes'] = $value;
					else if (empty($previous_cell)) $rows[$previous] = array('value' => '', 'classes' => $value);
					else $rows[$previous] = array('value' => $previous_cell, 'classes' => $value);
				}
				else {
					$rows[$prefix] = $value;
					if (is_array($text) && isset($text['style'])) {
						if (!is_array($rows[$prefix])) {
							$rows[$prefix] = array('value' => $rows[$prefix], 'style' => $text['style']);
						} else {
							$rows[$prefix]['style'] = $text['style'];
						}
					}
				}
			}
		}

		return $rows;
	}
	
	function parseLine($text) {
		if (strpos($text, "|") !== false) {
			$bits = explode("|", $text);
			$style = $bits[0];
			$value = $bits[1];
			
			$text = array('value' => $value, 'style' => $style);
		}
		
		return $text;
	}
	
	function getCurrent($data, $index) {
		if (!count($data)) $data = $this->actions;
		$keys = array_keys($data);
		return $data[$keys[$index]];
	}
	
	function getCurrentKey($data, $index) {
		if (!count($data)) $data = $this->actions;
		$keys = array_keys($data);
		return $keys[$index];
	}
	
	function getValue($value) {
		return (is_array($value)) ? $value['value'] : $value;
	}
	
	function getAction($action, $key, $data) {
		switch($action) { 
			case 'sub':
				if (is_array($data) && (array_key_exists('sub', $data))) return $data['sub'];
				else return '';
				break;
			case 'style': 
				if (is_array($data) && (array_key_exists('style', $data))) return $data['style'];
				else return '';
				break;
			case 'link':
				if (is_array($data) && (array_key_exists('link', $data))) return $data['link'];
				else return '';
				break;
			default:
				return $key;
		}
	}
	
	function isEmpty($index) {
		$key = $this->data[$index];
		
		if ($key && !empty($key['value'])) return false;
		return true;
	}
	
	function getTemplates() {
		global $rokfeaturetable_plugin_path, $rokfeaturetable_plugin_url;
		
		$this->_templates = $rokfeaturetable_plugin_path . DS . 'templates';
		$this->_jtemplate = TEMPLATEPATH . DS . 'admin' . DS . 'rft-templates';
		$output = "";
		
		if (file_exists($this->_templates)) {
			$files = $this->list_files($this->_templates, "\.txt");
			if (file_exists($this->_jtemplate)) {
				$jfiles = $this->list_files($this->_jtemplate, "\.txt");
				if (count($jfiles)) $this->merge($files, $jfiles);
			}
			
			if (count($files)) {
				$output = "<select id='templates-".$this->id."'>\n";
				$output .= "<option value='_select_' class='disabled' selected='selected'>Select a Template</option>";
				foreach($files as $file) {
					$handle = fopen($file, 'r');
					$title = $this->stripExt($this->getName($file));
					$title = str_replace("-", " ", str_replace("_", " ", $title));
					$title = ucwords($title);
					$output .= "<option value='".fread($handle, 8192)."'>".$title."</option>";
					fclose($handle);
				}
				$output .= "</select>\n";
				
				$output .= "<span id='import-button-".$this->id."' class='action-import'><span>import</span></span>\n";
			}
		} else {
			$output = "Templates folder was not found.";
		}
		
		return $output;
	}
	
	function list_files($path, $filter = '.', $exclude = array('.svn', 'CVS')) {
	
		$arr = array();

		if (!is_dir($path)) {
			_e('Path is not a folder. Path: ' . $path);
			return false;
		}
		
		$handle = opendir($path);
		while (($file = readdir($handle)) !== false) {
			if (($file != '.') && ($file != '..') && (!in_array($file, $exclude))) {
				$dir = $path . DS . $file;
				if (preg_match("/$filter/", $file)) {
					$arr[] = $path . DS . $file;   	
				}
			}
		}
		
		closedir($handle);
 
		asort($arr);
		return $arr;
	}
	
	function stripExt($file) {
		return preg_replace('#\.[^.]*$#', '', $file);
	}
	
	function getName($file) {
		$slash = strrpos($file, DS);
		if ($slash !== false) {
			return substr($file, $slash + 1);
		} else {
			return $file;
		}
	}
	
	function merge(&$files, $jfiles) {
		$clean_files = $this->getCleanArray($files);
		$clean_jfiles = $this->getCleanArray($jfiles);

		foreach($clean_jfiles as $index => $jfile) {
			if (in_array($jfile, $clean_files)) $files[array_search($jfile, $clean_files)] = $jfiles[$index];
			else array_push($files, $jfiles[$index]);
		}
		
		sort($files);
		return $files;
	}
	
	function getCleanArray($array) {
		$newArray = array();

		foreach($array as $value) array_push($newArray, $this->stripExt($this->getName($value)));
		
		return $newArray;
	}
	
	
	/********** Bellow here should not need to be changed ***********/

	function __construct() {
		if (empty($this->short_name) || empty($this->long_name)) {
			die("A widget must have a valid type and classname defined");
		}
		$widget_options = array('classname' => $this->css_classname, 'description' => __($this->description));
		$control_options = array('width' => $this->width, 'height' => $this->height);
		parent::__construct($this->short_name, $this->long_name, $widget_options, $control_options);
	}

	function _cleanOutputVariable($variable, $value) {
		if (is_string($value)) {
			return $value;
		}
		elseif (is_array($value)) {
			foreach ($value as $subvariable => $subvalue) {
				$value[$subvariable] = GantryWidgetRokMenu::_cleanOutputVariable($subvariable, $subvalue);
			}
			return $value;
		}
		return $value;
	}

	function _cleanInputVariable($variable, $value) {
		if (is_string($value)) {
			return stripslashes($value);
		}
		elseif (is_array($value)) {
			foreach ($value as $subvariable => $subvalue) {
				$value[$subvariable] = GantryWidgetRokMenu::_cleanInputVariable($subvariable, $subvalue);
			}
			return $value;
		}
		return $value;
	}

	function widget($args, $instance){
		global $gantry;
		extract($args);
		$defaults = $this->_defaults;
		$instance = wp_parse_args((array) $instance, $defaults);
		foreach ($instance as $variable => $value)
		{
			$$variable = self::_cleanOutputVariable($variable, $value);
			$instance[$variable] = $$variable;
		}
		$this->render($args, $instance);
	}

}

RokFeatureTable::$plugin_path = dirname($plugin);
RokFeatureTable::$plugin_url = WP_PLUGIN_URL.'/'.basename(RokFeatureTable::$plugin_path);

add_action('widgets_init', array('RokFeatureTable', 'init'));
add_action('admin_print_styles', array('RokFeatureTable', 'rokfeaturetable_admin_scripts'), 5);

// Load Language

load_plugin_textdomain('rokfeaturetable', false, basename($rokfeaturetable_plugin_path).'/languages/');

// MooTools Enqueue Script

add_action('init','rokfeaturetable_mootools_init',-50);

function rokfeaturetable_mootools_init(){
	global $rokfeaturetable_plugin_path, $rokfeaturetable_plugin_url;
	wp_register_script('mootools.js', $rokfeaturetable_plugin_url.'/admin/js/mootools.js');
	wp_enqueue_script('mootools.js');
}

if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

?>