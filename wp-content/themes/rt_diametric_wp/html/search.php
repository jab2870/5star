<?php
/**
 * @version   1.0 November 17, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
// no direct access
defined('ABSPATH') or die('Restricted access');
?>

<?php global $post, $posts, $query_string, $s, $wp_query; ?>

	<div class="rt-search">

		<?php /** Begin Query Setup **/ ?>
		
		<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		
		$query = $wp_query->query;
		if (!is_array($query)) parse_str($query, $query); 
		
		$custom_query = new WP_Query(array_merge($query, array('posts_per_page' => $gantry->get('search-count'), 'paged' => $paged))); ?>

		<?php /** End Query Setup **/ ?>

		<?php if($custom_query->have_posts()) : ?>
		
		<?php if($gantry->get('search-page-title')) : ?>
						
		<h1 class="rt-pagetitle">
			<?php _re('Search Results for'); ?>&nbsp;&#8216;<?php the_search_query(); ?>&#8217;
		</h1>
		
		<?php endif; ?>
														
		<?php while ($custom_query->have_posts()) : $custom_query->the_post(); ?>
		
		<?php
																														
		//$title 	= strip_tags(get_the_title());
		$content 	= strip_tags(strip_shortcodes(get_the_content(false)));
		$excerpt 	= strip_tags(strip_shortcodes(get_the_excerpt()));
		$keys 		= explode(" ",$s);
		//$title	= preg_replace('/('.implode('|', $keys) .')/iu', '<span class="search-excerpt">\0</span>', $title);
		$content 	= preg_replace('/('.implode('|', $keys) .')/iu', '<span class="search-excerpt">\0</span>', $content);
		$excerpt 	= preg_replace('/('.implode('|', $keys) .')/iu', '<span class="search-excerpt">\0</span>', $excerpt);

		// Create a shortcut for params.
		$category = get_the_category();

		?>

		<?php /** Begin Post **/ ?>
				
		<div class="rt-article">
			<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
				<div class="rt-article-bg">
					<div class="article-header">

						<?php /** Begin Article Title **/ ?>

						<?php if ($gantry->get('search-title')) : ?>

							<div class="article-title">
								<h1 class="title">
									<?php if ($gantry->get('search-link-title')) : ?>
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
									<?php else : ?>
										<?php the_title(); ?>
									<?php endif; ?>
								</h1>
							</div>

						<?php endif; ?>

						<?php /** End Article Title **/ ?>

						<div class="clear"></div>

						<?php /** Begin Extended Meta **/ ?>

						<?php if ($gantry->get('search-meta-date') || $gantry->get('search-meta-modified') || $gantry->get('search-meta-author') || $gantry->get('search-meta-comments')) : ?>
						
							<div class="rt-articleinfo">
								<div class="rt-articleinfo-text">
									<div class="rt-articleinfo-text2">

										<?php /** Begin Date & Time **/ ?>

										<?php if($gantry->get('search-meta-date')) : ?>

											<span class="rt-date-posted"><!--<?php _re('Created on'); ?> --><?php the_time('d F Y'); ?></span>

										<?php endif; ?>

										<?php /** End Date & Time **/ ?>

										<?php /** Begin Author **/ ?>
									
										<?php if ($gantry->get('search-meta-author')) : ?>
										
											<span class="rt-author">
												<?php the_author(); ?>
											</span>
										
										<?php endif; ?>
					
										<?php /** End Author **/ ?>

										<?php /** Begin Modified Date **/ ?>
					
										<?php if($gantry->get('search-meta-modified')) : ?>
					
											<span class="rt-date-modified"><?php _re('Last Updated on'); ?> <?php the_modified_date('d F Y'); ?></span>
					
										<?php endif; ?>
					
										<?php /** End Modified Date **/ ?>
					
										<?php /** Begin Comments Count **/ ?>
					
										<?php if($gantry->get('search-meta-comments')) : ?>
					
											<?php if($gantry->get('search-meta-link-comments')) : ?>
					
												<span class="rt-comments-count">
													<a href="<?php the_permalink(); ?>#comments">
														<?php comments_number(_r('0 Comments'), _r('1 Comment'), _r('% Comments')); ?>
													</a>
												</span>
					
											<?php else : ?>
					
												<span class="rt-comments-count"><?php comments_number(_r('0 Comments'), _r('1 Comment'), _r('% Comments')); ?></span>
					
											<?php endif; ?>
					
										<?php endif; ?>
					
										<?php /** End Comments Count **/ ?>
								
									</div>
								</div>
								<div class="clear"></div>
							</div>
						
						<?php endif; ?>

						<?php /** End Extended Meta **/ ?>

					</div>
				</div>

				<?php /** Begin Thumbnail **/ ?>
				
				<?php if(function_exists('the_post_thumbnail') && has_post_thumbnail()) : ?>

					<p>
						<?php the_post_thumbnail('gantryThumb', array('class' => 'rt-image '.$gantry->get('thumb-position'))); ?>			
					</p>
				
				<?php endif; ?>

				<?php /** End Thumbnail **/ ?>
				
				<?php /** Begin Post Content **/ ?>	
			
				<?php if($gantry->get('search-content') == 'content') : ?>
				
					<?php echo $content; ?>
									
				<?php else : ?>
									
					<?php echo $excerpt; ?>
										
				<?php endif; ?>
				
				<?php if(preg_match('/<!--more(.*?)?-->/', $post->post_content)) : ?>
				
					<p class="rt-readon-surround">																			
						<a href="<?php the_permalink(); ?>" class="readon"><span><?php echo $gantry->get('search-readmore'); ?></span></a>
					</p>
				
				<?php endif; ?>

				<?php /** Begin Parent Category **/ ?>
						
				<?php if ($gantry->get('search-meta-category-parent') && $category[0]->parent != '0') : ?>

					<div class="rt-parent-category">
						<?php
							$parent_category = get_category((int)$category[0]->parent);
							$title = $parent_category->cat_name;
							$link = get_category_link($parent_category);
							$url = '<a href="' . esc_url($link) . '">' . $title . '</a>'; 
						?>
	
						<?php if ($gantry->get('search-meta-link-category-parent')) : ?>
							<?php echo $url; ?>
						<?php else : ?>
							<?php echo $title; ?>
						<?php endif; ?>
	
						<?php if ($gantry->get('search-meta-category')) : ?>
							<?php echo ' - '; ?>
						<?php endif; ?>
					</div>

				<?php endif; ?>

				<?php /** End Parent Category **/ ?>

				<?php /** Begin Category **/ ?>

				<?php if ($gantry->get('search-meta-category')) : ?>

					<div class="rt-category">
						<?php 
							$title = $category[0]->cat_name;
							$link = get_category_link($category[0]->cat_ID);
							$url = '<a href="' . esc_url($link) . '">' . $title . '</a>';
						?>
	
						<?php if ($gantry->get('search-meta-link-category')) : ?>
							<?php echo $url; ?>
						<?php else : ?>
							<?php echo $title; ?>
						<?php endif; ?>
					</div>

				<?php endif; ?>

				<?php /** End Category **/ ?>
				
				<?php /** End Post Content **/ ?>

			</div>
		</div>
		
		<?php /** End Post **/ ?>

		<div class="item-separator"></div>
		
		<?php endwhile;?>
		
		<?php /** Begin Navigation **/ ?>
		
		<?php if($gantry->get('pagination-style') == 'full' && $custom_query->max_num_pages > 1) { ?>
	
			<?php if (!$current_page = get_query_var('paged')) $current_page = 1;
			
			$permalinks = get_option('permalink_structure');
			$format = empty($permalinks) ? '&paged=%#%' : 'page/%#%/';
			
			$pagination = paginate_links(array(
				'base' => get_pagenum_link(1) . '%_%',
				'format' => $format,
				'current' => $current_page,
				'total' => $custom_query->max_num_pages,
				'mid_size' => $gantry->get('pagination-count'),
				'type' => 'array',
				'next_text' => _r('Next').' &raquo;',
				'prev_text' => '&laquo; '._r('Previous')
			));
			
			$count = count($pagination);
			$last = $count-1;
			
			(preg_match('/class="prev/i', $pagination[0]) == 1) ? $pagination[0] .= '<div class="pages-nav">' : $pagination[0] = '<div class="pages-nav">' . $pagination[0];
			(preg_match('/class="next/i', $pagination[$last]) == 1) ? $pagination[$last] = '</div>' . $pagination[$last] : $pagination[$last] .= '</div>';
			
			?>
			
			<div class="rt-pagination nav">
				<div class="full-nav">
			
					<?php foreach($pagination as $page) {
					
						$page = preg_replace('/prev page-numbers/i', 'newer_posts', $page);
						$page = preg_replace('/next page-numbers/i', 'older_posts', $page);
						
						echo $page;
					} ?>
				
				</div>
				<div class="clear"></div>
			</div>
									
		<?php } else { ?>							
									
			<?php if($custom_query->max_num_pages > 1) : ?>
					
			<div class="rt-pagination nav">
				<div class="alignleft">
					<?php next_posts_link('&laquo; '._r('Previous'), $custom_query->max_num_pages); ?>
				</div>
				<div class="alignright">
					<?php previous_posts_link(_r('Next').' &raquo;', $custom_query->max_num_pages); ?>
				</div>
				<div class="clear"></div>
			</div>
						
			<?php endif; ?>

		<?php } ?>

		<?php /** End Navigation **/ ?>
		
		<?php else : ?>
																												
			<h1 class="rt-pagetitle">
				<?php _re("No posts found. Try a different search?"); ?>
			</h1>
														
		<?php endif; ?>
														
		<?php wp_reset_query(); ?>

	</div>