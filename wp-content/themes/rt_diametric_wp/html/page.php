<?php
/**
 * @version   1.0 November 17, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
// no direct access
defined('ABSPATH') or die('Restricted access');
?>

<?php global $post, $posts, $query_string; ?>

	<div class="rt-page">
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<?php /** Begin Post **/ ?>
				
		<div class="rt-article">
			<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
				<div class="article-header">

					<?php /** Begin Article Title **/ ?>

					<?php if ($gantry->get('page-title')) : ?>

						<div class="article-title">
							<h1 class="title">
								<?php the_title(); ?>
							</h1>
						</div>

					<?php endif; ?>

					<?php /** End Article Title **/ ?>

					<div class="clear"></div>

					<?php /** Begin Extended Meta **/ ?>

					<?php if ($gantry->get('page-meta-date') || $gantry->get('page-meta-modified') || $gantry->get('page-meta-author') || $gantry->get('page-meta-comments')) : ?>
					
						<div class="rt-articleinfo">
							<div class="rt-articleinfo-text">
								<div class="rt-articleinfo-text2">

									<?php /** Begin Date & Time **/ ?>

									<?php if($gantry->get('page-meta-date')) : ?>

										<span class="rt-date-posted"><!--<?php _re('Created on'); ?> --><?php the_time('d F Y'); ?></span>

									<?php endif; ?>

									<?php /** End Date & Time **/ ?>

									<?php /** Begin Author **/ ?>
								
									<?php if ($gantry->get('page-meta-author')) : ?>
									
										<span class="rt-author">
											<?php the_author(); ?>
										</span>
										
									<?php endif; ?>

									<?php /** End Author **/ ?>

									<?php /** Begin Modified Date **/ ?>

									<?php if($gantry->get('page-meta-modified')) : ?>

										<span class="rt-date-modified"><?php _re('Last Updated on'); ?> <?php the_modified_date('d F Y'); ?></span>

									<?php endif; ?>

									<?php /** End Modified Date **/ ?>

									<?php /** Begin Comments Count **/ ?>

									<?php if($gantry->get('page-meta-comments')) : ?>

										<span class="rt-comments-count"><?php comments_number(_r('0 Comments'), _r('1 Comment'), _r('% Comments')); ?></span>

									<?php endif; ?>

									<?php /** End Comments Count **/ ?>
								
								</div>
							</div>
							<div class="clear"></div>
						</div>
					
					<?php endif; ?>

					<?php /** End Extended Meta **/ ?>

				</div>

				<?php /** Begin Post Content **/ ?>		
						
				<?php the_content(); ?>
	
				<div class="clear"></div>
				
				<?php wp_link_pages('before=<div class="rt-pagination">'._r('Pages:').'&after=</div><br />'); ?>
																											
				<?php edit_post_link(_r('Edit this entry.'), '<div class="edit-entry">', '</div>'); ?>
					
				<?php if(comments_open() && $gantry->get('page-comments-form')) : ?>
															
					<?php echo $gantry->displayComments(true, 'standard', 'standard'); ?>
				
				<?php endif; ?>
				
				<div class="clear"></div>
				
				<?php /** End Post Content **/ ?>

			</div>
		</div>
		
		<?php /** End Post **/ ?>
		
		<?php endwhile; ?>
		
		<?php else : ?>
																	
			<h1 class="rt-pagetitle">
				<?php _re('Sorry, no pages matched your criteria.'); ?>
			</h1>
			
		<?php endif; ?>
		
		<?php wp_reset_query(); ?>

	</div>