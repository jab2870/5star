<?php
/**
 * @version   1.0 November 17, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
 
class DiametricFusionMenuTheme extends AbstractRokMenuTheme {

    protected $defaults = array(
        'fusion_load_css' => 0,
        'fusion_enable_js' => 1,
        'fusion_opacity' => 1,
        'fusion_effect' => 'slidefade',
        'fusion_hidedelay' => 500,
        'fusion_menu_animation' => 'Circ.easeOut',
        'fusion_menu_duration' => 300,
        'fusion_pill' => 0,
        'fusion_pill_animation' => 'Back.easeOut',
        'fusion_pill_duration' => 400,
        'fusion_centeredOffset' => 0,
        'fusion_tweakInitial_x' => -8,
        'fusion_tweakInitial_y' => -6,
        'fusion_tweakSubsequent_x' => -8,
        'fusion_tweakSubsequent_y' => -11,
        'fusion_tweak-width' => 18,
        'fusion_tweak-height' => 20,
        'fusion_enable_current_id' => 0
    );

    public function getFormatter($args){
        require_once(dirname(__FILE__).'/formatter.php');
        return new DiametricFusionMenuFormatter($args);
    }

    public function getLayout($args){
        require_once(dirname(__FILE__).'/layout.php');
        return new DiametricFusionMenuLayout($args);
    }
}
