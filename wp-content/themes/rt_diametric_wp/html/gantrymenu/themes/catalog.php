<?php
/**
 * @version   1.0 November 17, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

require_once(dirname(__FILE__).'/diametric_fusion/theme.php');
GantryWidgetMenu::registerTheme(dirname(__FILE__).'/diametric_fusion','diametric_fusion', __('Diametric Fusion'), 'DiametricFusionMenuTheme');

require_once(dirname(__FILE__).'/diametric_splitmenu/theme.php');
GantryWidgetMenu::registerTheme(dirname(__FILE__).'/diametric_splitmenu','diametric_splitmenu', __('Diametric SplitMenu'), 'DiametricSplitMenuTheme');

require_once(dirname(__FILE__).'/diametric_touch/theme.php');
GantryWidgetMenu::registerTheme(dirname(__FILE__).'/diametric_touch','diametric_touch', __('Diametric Touch'), 'DiametricTouchMenuTheme');