<?php
/**
 * @version   1.0 November 17, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
 // no direct access
defined('ABSPATH') or die('Restricted access');
?>

<h1 class="rt-pagetitle"><?php _re('404 Page Not Found'); ?></h1>