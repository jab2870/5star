<?php
/**
 * @version   1.0 November 17, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

defined('GANTRY_VERSION') or die();

gantry_import('core.gantrygizmo');

/**
 * @package     gantry
 * @subpackage  features
 */
class GantryGizmoCompositeOverrides extends GantryGizmo {

    var $_name = 'compositeoverrides';

	function isEnabled() {
        return true;
    }

    function init() {
    	global $gantry;
    	
		if (!defined('ROKCOMMON_LIB_PATH')) define('ROKCOMMON_LIB_PATH', ROKCOMMON_LIB_PATH);
        if (is_file(ROKCOMMON_LIB_PATH.'/include.php'))
        {
            include(ROKCOMMON_LIB_PATH.'/include.php');
        }
        if (defined('ROKCOMMON') && class_exists('RokCommon_Composite')){
            RokCommon_Composite::addPackagePath('rokgallery', $gantry->templatePath . '/overrides/rokgallery/templates',20);
        }

    }
	
}