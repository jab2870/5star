<?php
/**
 * @version   1.0 November 17, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
 
defined('GANTRY_VERSION') or die();

gantry_import('core.gantrywidget');

add_action('widgets_init', array("GantryWidgetSocial","init"));

class GantryWidgetSocial extends GantryWidget {
    var $short_name = 'social';
    var $wp_name = 'gantry_social';
    var $long_name = 'Gantry Social Buttons';
    var $description = 'Please place it in the Social position';
    var $css_classname = 'widget_gantry_social';
    var $width = 200;
    var $height = 400;

    function init() {
        register_widget("GantryWidgetSocial");
    }
    
    function render_widget_open($args, $instance){
	}
	
	function render_widget_close($args, $instance){
	}
	
	function pre_render($args, $instance) {
	}
	
	function post_render($args, $instance) {
	}

    function render($args, $instance){
        global $gantry;
	    ob_start();

	    ?>

	    <div class="rt-social-buttons">
		    	<?php if ($instance['facebook'] != '') : ?>
			<a id="rt-facebook-btn" href="<?php echo $instance['facebook']; ?>">
				<span></span>
			</a>
			<?php endif; ?>
			<?php if ($instance['twitter'] != '') : ?>
			<a id="rt-twitter-btn" href="<?php echo $instance['twitter']; ?>">
				<span></span>
			</a>
			<?php endif; ?>
			<?php if ($instance['buzz'] != '') : ?>
			<a id="rt-buzz-btn" href="<?php echo $instance['buzz']; ?>">
				<span></span>
			</a>
			<?php endif; ?>
			<?php if ($instance['rss'] != '') : ?>
			<a id="rt-rss-btn" href="<?php echo $instance['rss']; ?>">
				<span></span>
			</a>
			<?php endif; ?>
		</div>

		<?php

		echo ob_get_clean();
	    
	}
}