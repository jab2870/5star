<?php
/**
 * @version   1.0 November 17, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
// no direct access
defined('ABSPATH') or die('Restricted access');

global $gantry;
?>
							<?php /** Begin Main Body **/ ?>
						    <?php echo $gantry->displayMainbody('mainbody','sidebar','standard','standard','standard','standard','standard',null,ob_get_clean()); ?>
							<?php /** End Main Body **/ ?>
							<?php /** Begin Main Bottom **/ if ($gantry->countModules('mainbottom')) : ?>
							<div id="rt-mainbottom">
								<?php echo $gantry->displayModules('mainbottom','standard','standard'); ?>
								<div class="clear"></div>
							</div>
							<?php /** End Main Bottom **/ endif; ?>
							<?php /** Begin Extension **/ if ($gantry->countModules('extension')) : ?>
							<div id="rt-extension">
								<div class="rt-container">
									<?php echo $gantry->displayModules('extension','standard','standard'); ?>
									<div class="clear"></div>
								</div>
							</div>
							<?php /** End Extension **/ endif; ?>
						</div>
					</div>
				</div>
				<?php /** Begin Social **/ if ($gantry->countModules('social')) : ?>
				<?php echo $gantry->displayModules('social','basic','basic'); ?>
				<?php /** End Social **/ endif; ?>
			</div>
			<?php /** Begin Bottom **/ if ($gantry->countModules('bottom')) : ?>
			<div id="rt-bottom" <?php echo $gantry->displayClassesByTag('rt-showcaseblock'); ?>><div id="rt-bottom2" <?php echo $gantry->displayClassesByTag('rt-showcasepattern'); ?>><div id="rt-bottom3" class="rippedpaper style<?php echo $gantry->get('body-overlay'); ?> showcasepanel-accentoverlay-<?php echo $gantry->get('showcasepanel-accentoverlay'); ?>"><div id="rt-bottom4">
				<div class="rt-container">
					<?php echo $gantry->displayModules('bottom','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div></div></div></div>
			<?php /** End Bottom **/ endif; ?>
			<?php /** Begin Footer Section **/ if ($gantry->countModules('footer-position') or $gantry->countModules('copyright')) : ?>
			<div id="rt-footer-surround" <?php echo $gantry->displayClassesByTag('rt-footerblock'); ?>><div id="rt-footer-surround2" <?php echo $gantry->displayClassesByTag('footerpanel-pattern'); ?>><div id="rt-footer-surround3" class="footerpanel-accentoverlay-<?php echo $gantry->get('footerpanel-accentoverlay'); ?>">
				<?php /** Begin Footer **/ if ($gantry->countModules('footer-position')) : ?>
				<div id="rt-footer"><div id="rt-footer2">
					<div class="rt-container">
						<?php echo $gantry->displayModules('footer-position','standard','standard'); ?>
						<div class="clear"></div>
					</div>
				</div></div>
				<?php /** End Footer **/ endif; ?>
				<?php /** Begin Copyright **/ if ($gantry->countModules('copyright')) : ?>
				<div id="rt-copyright"><div id="rt-copyright2">
					<div class="rt-container">
						<?php echo $gantry->displayModules('copyright','standard','standard'); ?>
						<div class="clear"></div>
					</div>
				</div></div>
				<?php /** End Copyright **/ endif; ?>
			</div></div></div>
			<?php /** End Footer Section **/ endif; ?>
			<?php /** Begin Debug **/ if ($gantry->countModules('debug')) : ?>
			<div id="rt-debug">
				<div class="rt-container">
					<?php echo $gantry->displayModules('debug','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div>
			<?php /** End Debug **/ endif; ?>
			<?php /** Begin Popups **/
			echo $gantry->displayModules('popup','popup','popup');
			echo $gantry->displayModules('login','login','popup');
			/** End Popup s**/ ?>
			<?php /** Begin Analytics **/ if ($gantry->countModules('analytics')) : ?>
			<?php echo $gantry->displayModules('analytics','basic','basic'); ?>
			<?php /** End Analytics **/ endif; ?>
		</div>
		<?php $gantry->displayFooter(); ?>
	</body>
</html>
<?php
$gantry->finalize();
?>