<?php
/**
 * @version   1.0 November 17, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
// no direct access
defined('ABSPATH') or die('Restricted access');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $gantry->language; ?>" lang="<?php echo $gantry->language;?>" >
<head>

	<?php
		$gantry->displayHead();
		$gantry->addStyles(array('gantry-core.css','wordpress-core.css','menu-dark.css','body-light.css','overlays.css','fusionmenu.css','splitmenu.css','template.css','error.css','wp.css','custom.css'));

		if ($gantry->browser->platform != 'iphone')
			$gantry->addInlineScript('window.addEvent("domready", function(){ new SmoothScroll(); });');

	?>
	<style type="text/css">
		<?php 
			$accentColor = new Color($gantry->get('body-accentcolor'));
			echo '#rt-navigation, #rt-navigation .rt-menubar ul.menutop li.active {background-color:'.$accentColor->darken('10%').';}'."\n";
			echo '.menutop .rt-arrow-pointer {border-top-color:'.$gantry->get('body-accentcolor').';}'."\n";
			echo $css = '#rt-main-container a, #rt-main-container h1 span, #rt-main-container .module-title .title span,#rt-body-surround .sprocket-lists .sprocket-lists-container li.active .sprocket-lists-title, body #roksearch_results h3, body #roksearch_results a, .item-title {color:'.$gantry->get('body-accentcolor').';}'."\n";
			echo '#rt-main-container.body-overlay-light a:hover, #rt-main-container.body-overlay-light .title a:hover span {color:'.$accentColor->darken('15%').';}'."\n";
    		echo '#rt-main-container.body-overlay-dark a:hover, #rt-main-container.body-overlay-dark .title a:hover span {color:'.$accentColor->lighten('15%').';}'."\n";
    		echo '#rt-main-container .readon span, #rt-main-container .readon .button {background-color:'.$accentColor->darken('10%').';}'."\n";
		?>
	</style>

</head>
<body <?php echo $gantry->displayBodyTag(); ?>>
	<div id="rt-top-surround"><div id="rt-top-surround2">
		<?php /** Begin Header **/ if ($gantry->countModules('header')) : ?>
		<div id="rt-header">
			<div class="rt-container">
				<?php echo $gantry->displayModules('header','standard','standard'); ?>
				<div class="clear"></div>
			</div>
		</div>
		<?php /** End Header **/ endif; ?>
	</div></div>
	<div id="rt-navigation" <?php echo $gantry->displayClassesByTag('rt-menuoverlay'); ?>><div id="rt-navigation2"><div id="rt-navigation3" class="centered">
		<div class="rt-container">
			<div class="rt-block menu-block">
				<div class="rt-fusionmenu">
					<div class="nopill">
						<div class="rt-menubar">
	    					<ul class="menutop level1">
	                        	<li class="item1 root logo-holder">
		                            <a href="<?php echo $gantry->baseUrl; ?>" class="logo-module"></a>
	                    		</li>
	                    	</ul>
	                    </div>
	                </div>
	            </div>
        	</div>
			<div class="clear"></div>
		</div>
	</div></div></div>
	<div id="rt-error-body">
		<div id="rt-main-container">
			<div class="rt-container">
				<div id="rt-body-surround" class="component-block component-content">
					<div class="rt-error-box">
						<h2><?php _re('Error:'); ?></h2>
						<h1 class="error-title title"><span>404</span> - <?php _re('Page not found'); ?></h1>
						<p><strong><?php _re('You may not be able to visit this page because of:'); ?></strong></p>
						<ol>
							<li><?php _re('an out-of-date bookmark/favourite'); ?></li>
							<li><?php _re('a search engine that has an out-of-date listing for this site'); ?></li>
							<li><?php _re('a mistyped address'); ?></li>
							<li><?php _re('you have no access to this page'); ?></li>
							<li><?php _re('The requested resource was not found.'); ?></li>
							<li><?php _re('An error has occurred while processing your request.'); ?></li>
						</ol>
						<p></p>
						<p><a href="<?php echo $gantry->baseUrl; ?>" class="readon"><span>&larr; <?php _re('Home'); ?></span></a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $gantry->displayFooter(); ?>
</body>
</html>
<?php
$gantry->finalize();
?>