<?php
/**
 * @version   1.0 November 17, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
?>
<div class="sprocket-features layout-showcase <?php if ($parameters->get('features_show_arrows')!='hide') : ?>arrows-active<?php endif; ?> <?php if ($parameters->get('features_show_arrows')=='onhover') : ?>arrows-onhover<?php endif; ?> <?php if ($parameters->get('features_show_pagination')) : ?>pagination-active<?php endif; ?>" data-showcase="<?php echo $parameters->get('module_id'); ?>">
	<div class="sprocket-features-inner">
		<ul class="sprocket-features-list">
			<?php
			$index = 0;
				foreach($items as $item){
					$index++;
					echo $layout->getThemeContext()->load('item.php', array('item'=> $item,'parameters'=>$parameters,'index'=>$index,'layout'=>$layout));
				}
			?>
		</ul>
		<div class="sprocket-features-pagination<?php echo $parameters->get('features_show_pagination') ? '' : '-hidden'; ?>">
			<ul>
			<?php $i = 0; foreach ($items as $item): ?>
				<?php
					$class = (!$i) ? ' class="active"' : '';
					$i++;
				?>
		    	<li<?php echo $class; ?> data-showcase-pagination="<?php echo $i; ?>"><span><?php echo $i; ?></span></li>
			<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<?php if ($parameters->get('features_show_arrows')!='hide') : ?>
	<div class="sprocket-features-arrows">
		<span class="arrow next" data-showcase-next><span></span></span>
		<span class="arrow prev" data-showcase-previous><span></span></span>
	</div>
	<?php endif; ?>
</div>
