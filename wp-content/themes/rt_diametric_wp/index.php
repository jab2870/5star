<?php
/**
 * @version   1.0 November 17, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
// no direct access
defined('ABSPATH') or die('Restricted access');

global $gantry;

function isBrowserCapable(){
	global $gantry;
	
	$browser = $gantry->browser;
	
	// ie.
	if ($browser->name == 'ie' && $browser->version < 8) return false;
	
	return true;
}
// get the current preset
$gpreset = str_replace(' ','',strtolower($gantry->get('name')));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $gantry->language; ?>" lang="<?php echo $gantry->language;?>" >
<head>
	<?php
		$gantry->displayHead();
		$gantry->addStyles(array('template.css','wp.css','custom.css'));

		if ($gantry->browser->platform != 'iphone')
			$gantry->addInlineScript('window.addEvent("domready", function(){ new SmoothScroll(); });');

		if ($gantry->get('loadtransition') && isBrowserCapable()){
			$gantry->addScript('load-transition.js');
			$hidden = ' class="rt-hidden"';
		} else {
			$hidden = '';
		}

	?>
</head>
	<body <?php echo $gantry->displayBodyTag(); ?>>
		<div id="rt-page-surround">
			<div id="rt-headerblock" <?php echo $gantry->displayClassesByTag('rt-headerblock'); ?>><div id="rt-headerblock2" <?php echo $gantry->displayClassesByTag('rt-headerpattern'); ?>><div id="rt-headerblock3" class="headerpanel-accentoverlay-<?php echo $gantry->get('headerpanel-accentoverlay'); ?>">
				<div id="rt-top-surround" <?php if ($gantry->countModules('controls') or $gantry->countModules('top')) : ?>class="nopadding"<?php endif; ?>>
					<?php /** Begin Controls **/ if ($gantry->countModules('controls')) : ?>
					<div id="rt-controls">
						<div class="rt-container">
							<?php echo $gantry->displayModules('controls','basic','basic'); ?>
							<div class="clear"></div>
						</div>
					</div>
					<?php /** End Controls **/ endif; ?>
					<?php /** Begin Drawer **/ if ($gantry->countModules('drawer')) : ?>
					<div id="rt-drawer">
						<div class="rt-container">
							<?php echo $gantry->displayModules('drawer','standard','standard'); ?>
							<div class="clear"></div>
						</div>
					</div>
					<?php /** End Drawer **/ endif; ?>
					<?php /** Begin Top **/ if ($gantry->countModules('top')) : ?>
					<div id="rt-top">
						<div class="rt-container">
							<?php echo $gantry->displayModules('top','standard','standard'); ?>
							<div class="clear"></div>
						</div>
					</div>
					<?php /** End Top **/ endif; ?>
				</div>
				<?php /** Begin Navigation **/ if ($gantry->countModules('navigation')) : ?>
				<div id="rt-navigation" <?php echo $gantry->displayClassesByTag('rt-menuoverlay'); ?>><div id="rt-navigation2"><div id="rt-navigation3" <?php if ($gantry->get('menu-centering')) : ?>class="centered"<?php endif; ?>>
					<div class="rt-container">
						<?php echo $gantry->displayModules('navigation','standard','standard'); ?>
						<div class="clear"></div>
					</div>
				</div></div></div>
				<?php /** End Navigation **/ endif; ?>
				<?php /** Begin Header **/ if ($gantry->countModules('header')) : ?>
				<div id="rt-header">
					<div class="rt-container">
						<?php echo $gantry->displayModules('header','standard','standard'); ?>
						<div class="clear"></div>
					</div>
				</div>
				<?php /** End Header **/ endif; ?>
			</div></div></div>
			<div id="rt-transition"<?php echo $hidden; ?>>
				<?php /** Begin Showcase **/ if ($gantry->countModules('showcase')) : ?>
				<div id="rt-showcase" <?php echo $gantry->displayClassesByTag('rt-showcaseblock'); ?>><div id="rt-showcase2" <?php echo $gantry->displayClassesByTag('rt-showcasepattern'); ?>><div id="rt-showcase3" class="showcasepanel-accentoverlay-<?php echo $gantry->get('showcasepanel-accentoverlay'); ?>">
					<div class="rt-container">
						<?php echo $gantry->displayModules('showcase','standard','standard'); ?>
						<div class="clear"></div>
					</div>
				</div></div></div>
				<?php /** End Showcase **/ endif; ?>
				<?php /** Begin Feature **/ if ($gantry->countModules('feature')) : ?>
				<div id="rt-feature" <?php echo $gantry->displayClassesByTag('rt-featureblock'); ?>><div id="rt-feature2"><div id="rt-feature3" class="featurepanel-accentoverlay-<?php echo $gantry->get('featurepanel-accentoverlay'); ?>"><div id="rt-feature4" <?php echo $gantry->displayClassesByTag('rt-featurepattern'); ?>>
					<div class="rt-container">
						<?php echo $gantry->displayModules('feature','standard','standard'); ?>
						<div class="clear"></div>
					</div>
				</div></div></div></div>
				<?php /** End Feature **/ endif; ?>
				<div id="rt-main-container" <?php echo $gantry->displayClassesByTag('rt-bodyoverlay'); ?>>
					<div id="rt-body-surround" <?php echo $gantry->displayClassesByTag('body-accentoverlay'); ?>>
						<div class="rt-container">
							<?php /** Begin Utility **/ if ($gantry->countModules('utility')) : ?>
							<div id="rt-utility">
								<?php echo $gantry->displayModules('utility','standard','standard'); ?>
								<div class="clear"></div>
							</div>
							<?php /** End Utility **/ endif; ?>
							<?php /** Begin Main Top **/ if ($gantry->countModules('maintop')) : ?>
							<div id="rt-maintop">
								<?php echo $gantry->displayModules('maintop','standard','standard'); ?>
								<div class="clear"></div>
							</div>
							<?php /** End Main Top **/ endif; ?>
							<?php /** Begin Breadcrumbs **/ if ($gantry->countModules('breadcrumb')) : ?>
							<div id="rt-breadcrumbs">
								<?php echo $gantry->displayModules('breadcrumb','basic','breadcrumbs'); ?>
								<div class="clear"></div>
							</div>
							<?php /** End Breadcrumbs **/ endif; ?>
							<?php /** Begin Main Body **/ ?>
						    <?php echo $gantry->displayMainbody('mainbody','sidebar','standard','standard','standard','standard','standard'); ?>
							<?php /** End Main Body **/ ?>
							<?php /** Begin Main Bottom **/ if ($gantry->countModules('mainbottom')) : ?>
							<div id="rt-mainbottom">
								<?php echo $gantry->displayModules('mainbottom','standard','standard'); ?>
								<div class="clear"></div>
							</div>
							<?php /** End Main Bottom **/ endif; ?>
							<?php /** Begin Extension **/ if ($gantry->countModules('extension')) : ?>
							<div id="rt-extension">
								<div class="rt-container">
									<?php echo $gantry->displayModules('extension','standard','standard'); ?>
									<div class="clear"></div>
								</div>
							</div>
							<?php /** End Extension **/ endif; ?>
						</div>
					</div>
				</div>
				<?php /** Begin Social **/ if ($gantry->countModules('social')) : ?>
				<?php echo $gantry->displayModules('social','basic','basic'); ?>
				<?php /** End Social **/ endif; ?>
			</div>
			<?php /** Begin Bottom **/ if ($gantry->countModules('bottom')) : ?>
			<div id="rt-bottom" <?php echo $gantry->displayClassesByTag('rt-showcaseblock'); ?>><div id="rt-bottom2" <?php echo $gantry->displayClassesByTag('rt-showcasepattern'); ?>><div id="rt-bottom3" class="rippedpaper style<?php echo $gantry->get('body-overlay'); ?> showcasepanel-accentoverlay-<?php echo $gantry->get('showcasepanel-accentoverlay'); ?>"><div id="rt-bottom4">
				<div class="rt-container">
					<?php echo $gantry->displayModules('bottom','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div></div></div></div>
			<?php /** End Bottom **/ endif; ?>
			<?php /** Begin Footer Section **/ if ($gantry->countModules('footer-position') or $gantry->countModules('copyright')) : ?>
			<div id="rt-footer-surround" <?php echo $gantry->displayClassesByTag('rt-footerblock'); ?>><div id="rt-footer-surround2" <?php echo $gantry->displayClassesByTag('footerpanel-pattern'); ?>><div id="rt-footer-surround3" class="footerpanel-accentoverlay-<?php echo $gantry->get('footerpanel-accentoverlay'); ?>">
				<?php /** Begin Footer **/ if ($gantry->countModules('footer-position')) : ?>
				<div id="rt-footer"><div id="rt-footer2">
					<div class="rt-container">
						<?php echo $gantry->displayModules('footer-position','standard','standard'); ?>
						<div class="clear"></div>
					</div>
				</div></div>
				<?php /** End Footer **/ endif; ?>
				<?php /** Begin Copyright **/ if ($gantry->countModules('copyright')) : ?>
				<div id="rt-copyright"><div id="rt-copyright2">
					<div class="rt-container">
						<?php echo $gantry->displayModules('copyright','standard','standard'); ?>
						<div class="clear"></div>
					</div>
				</div></div>
				<?php /** End Copyright **/ endif; ?>
			</div></div></div>
			<?php /** End Footer Section **/ endif; ?>
			<?php /** Begin Debug **/ if ($gantry->countModules('debug')) : ?>
			<div id="rt-debug">
				<div class="rt-container">
					<?php echo $gantry->displayModules('debug','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div>
			<?php /** End Debug **/ endif; ?>
			<?php /** Begin Popups **/
			echo $gantry->displayModules('popup','popup','popup');
			echo $gantry->displayModules('login','login','popup');
			/** End Popup s**/ ?>
			<?php /** Begin Analytics **/ if ($gantry->countModules('analytics')) : ?>
			<?php echo $gantry->displayModules('analytics','basic','basic'); ?>
			<?php /** End Analytics **/ endif; ?>
		</div>
		<?php $gantry->displayFooter(); ?>
	</body>
</html>
<?php
$gantry->finalize();
?>