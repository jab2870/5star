<?php
/**
 * @version   1.0 November 17, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
// no direct access
defined( 'GANTRY_VERSION' ) or die( 'Restricted index access' );
global $gantry;
$gantry->set('fixedheader', 0);
$gantry->set('blog-lead-items', $gantry->get('blog-count'));
$gantry->set('blog-columns', '1');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $gantry->language; ?>" lang="<?php echo $gantry->language;?>" >
    <head>
        <?php
            $gantry->displayHead();
            $gantry->addStyles(array('template.css','joomla.css','iphone-gantry.css','android-gantry.css'));
			$gantry->addScript('iscroll.js');

            $gantry->addScript('roktabs.js');
			$hidden = '';
        ?>
			<?php
				$scalable = $gantry->get('iphone-scalable', 0) == "0" ? "0" : "1";
			?>
			<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=<?php echo $scalable; ?>;">

			<script type="text/javascript">
				var orient = function() {
					var dir = "rt-normal";
					switch(window.orientation) {
						case 0: dir = "rt-normal";break;
						case -90: dir = "rt-right";break;
						case 90: dir = "rt-left";break;
						case 180: dir = "rt-flipped";break;
					}
					$$(document.body, '#rt-wrapper')
						.removeClass('rt-normal')
						.removeClass('rt-left')
						.removeClass('rt-right')
						.removeClass('rt-flipped')
						.addClass(dir);
				}

				window.addEvent('domready', function() {
					orient();
					window.scrollTo(0, 1);
					new iScroll($$('#rt-menu ul.menu')[0]);
				});

			</script>
    </head>
   <body <?php echo $gantry->displayBodyTag(); ?> onorientationchange="orient()">
    	<div id="rt-headerblock" <?php echo $gantry->displayClassesByTag('rt-headerblock'); ?>><div id="rt-headerblock2" <?php echo $gantry->displayClassesByTag('rt-headerpattern'); ?>><div id="rt-headerblock3" class="headerpanel-accentoverlay-<?php echo $gantry->get('headerpanel-accentoverlay'); ?>">
	    	<div id="rt-top-surround">
				<?php /** Begin Drawer **/ if ($gantry->countModules('mobile-drawer')) : ?>
				<div id="rt-drawer">
					<div class="rt-container">
						<?php echo $gantry->displayModules('mobile-drawer','standard','standard'); ?>
						<div class="clear"></div>
					</div>
				</div>
				<?php /** End Drawer **/ endif; ?>
				<?php /** Begin Top **/ if ($gantry->countModules('mobile-top')) : ?>
				<div id="rt-navigation" <?php echo $gantry->displayClassesByTag('rt-menuoverlay'); ?>><div id="rt-navigation2"><div id="rt-navigation3">
					<div class="rt-container">
						<?php echo $gantry->displayModules('mobile-top','standard','standard'); ?>
						<div class="clear"></div>
					</div>
				</div></div></div>
				<?php /** End Top **/ endif; ?>
			</div>
			<?php /** Begin Menu **/ if ($gantry->countModules('mobile-navigation')) : ?>
			<div id="rt-menu"><div class="rt-container">
				<div id="rt-left-menu"></div>
				<div id="rt-right-menu"></div>
				<?php echo $gantry->displayModules('mobile-navigation','basic','basic'); ?>
				<div class="clear"></div>
			</div></div>
			<?php /** End Menu **/ endif; ?>
		</div></div></div>
		<?php /** Begin Feature **/ if ($gantry->countModules('mobile-feature')) : ?>
		<div id="rt-feature" <?php echo $gantry->displayClassesByTag('rt-featureblock'); ?>><div id="rt-feature2"><div id="rt-feature3" class="featurepanel-accentoverlay-<?php echo $gantry->get('featurepanel-accentoverlay'); ?>"><div id="rt-feature4" <?php echo $gantry->displayClassesByTag('rt-featurepattern'); ?>>
			<div class="rt-container">
				<?php echo $gantry->displayModules('mobile-feature','standard','standard'); ?>
				<div class="clear"></div>
			</div>
		</div></div></div></div>
		<?php /** End Feature **/ endif; ?>
		<?php /** Begin Showcase **/ if ($gantry->countModules('mobile-showcase')) : ?>
		<div id="rt-showcase" <?php echo $gantry->displayClassesByTag('rt-showcaseblock'); ?>><div id="rt-showcase2" <?php echo $gantry->displayClassesByTag('rt-showcasepattern'); ?>><div id="rt-showcase3" class="showcasepanel-accentoverlay-<?php echo $gantry->get('showcasepanel-accentoverlay'); ?>">
			<div class="rt-container">
				<?php echo $gantry->displayModules('mobile-showcase','standard','standard'); ?>
				<div class="clear"></div>
			</div>
		</div></div></div>
		<?php /** End Header **/ endif; ?>
		<div id="rt-main-container" <?php echo $gantry->displayClassesByTag('rt-bodyoverlay'); ?>>
			<div id="rt-body-surround" <?php echo $gantry->displayClassesByTag('body-accentoverlay'); ?>>
				<div id="rt-container">
					<?php /** Begin Utility **/ if ($gantry->countModules('mobile-utility')) : ?>
					<div id="rt-utility">
						<?php echo $gantry->displayModules('mobile-utility','standard','standard'); ?>
						<div class="clear"></div>
					</div>
					<?php /** End Utility **/ endif; ?>
					<?php /** Begin Breadcrumbs **/ if ($gantry->countModules('breadcrumb')) : ?>
					<div id="rt-breadcrumbs">
						<?php echo $gantry->displayModules('breadcrumb','basic','breadcrumbs'); ?>
						<div class="clear"></div>
					</div>
					<?php /** End Breadcrumbs **/ endif; ?>
					<?php /** Begin Main Body **/
						$display_mainbody = !($gantry->get("mainbody-enabled",true)==false);
					?>

					<?php if ($display_mainbody): ?>
					<?php echo $gantry->displayMainbody('iphonemainbody','sidebar','standard','standard','standard','standard','standard'); ?>
					<?php endif; ?>
					<?php /** End Main Body **/ ?>
					<?php /** Begin Extension **/ if ($gantry->countModules('mobile-extension')) : ?>
					<div id="rt-extension">
						<?php echo $gantry->displayModules('mobile-extension','standard','standard'); ?>
						<div class="clear"></div>
					</div>
					<?php /** End Extension **/ endif; ?>
				</div>
			</div>
		</div>
		<?php /** Begin Bottom **/ if ($gantry->countModules('mobile-bottom')) : ?>
		<div id="rt-bottom" <?php echo $gantry->displayClassesByTag('rt-showcaseblock'); ?>><div id="rt-bottom2" <?php echo $gantry->displayClassesByTag('rt-showcasepattern'); ?>><div id="rt-bottom3" class="rippedpaper style<?php echo $gantry->get('body-overlay'); ?> showcasepanel-accentoverlay-<?php echo $gantry->get('showcasepanel-accentoverlay'); ?>"><div id="rt-bottom4">
			<div class="rt-container">
				<?php echo $gantry->displayModules('mobile-bottom','standard','standard'); ?>
				<div class="clear"></div>
			</div>
		</div></div></div></div>
		<?php /** End Bottom **/ endif; ?>
		<?php /** Begin Footer Section **/ if ($gantry->countModules('mobile-footer') or $gantry->countModules('mobile-copyright')) : ?>
		<div id="rt-footer-surround" <?php echo $gantry->displayClassesByTag('rt-footerblock'); ?>><div id="rt-footer-surround2" <?php echo $gantry->displayClassesByTag('footerpanel-pattern'); ?>><div id="rt-footer-surround3" class="footerpanel-accentoverlay-<?php echo $gantry->get('footerpanel-accentoverlay'); ?>">
			<?php /** Begin Footer **/ if ($gantry->countModules('mobile-footer')) : ?>
			<div id="rt-footer"><div id="rt-footer2">
				<div class="rt-container">
					<?php echo $gantry->displayModules('mobile-footer','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div></div>
			<?php /** End Footer **/ endif; ?>
			<?php /** Begin Copyright **/ if ($gantry->countModules('mobile-copyright')) : ?>
			<div id="rt-copyright"><div id="rt-copyright2">
				<div class="rt-container">
					<?php echo $gantry->displayModules('mobile-copyright','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div></div>
			<?php /** End Copyright **/ endif; ?>
		</div></div></div>
		<?php /** End Footer Section **/ endif; ?>
		<?php /** Begin Debug **/ if ($gantry->countModules('debug')) : ?>
		<div id="rt-debug">
			<div class="rt-container">
				<?php echo $gantry->displayModules('debug','standard','standard'); ?>
				<div class="clear"></div>
			</div>
		</div>
		<?php /** End Debug **/ endif; ?>
		<?php /** Begin Analytics **/ if ($gantry->countModules('analytics')) : ?>
		<?php echo $gantry->displayModules('analytics','basic','basic'); ?>
		<?php /** End Analytics **/ endif; ?>
		<?php $gantry->displayFooter(); ?>
	</body>
</html>
<?php
$gantry->finalize();
?>