<?php
/**
 * @version   1.1 November 13, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
defined('GANTRY_VERSION') or die();

$gantry_config_mapping = array(
    'belatedPNG' => 'belatedPNG'
);

$gantry_presets = array (
    'presets' => array (
        'preset1' => array (
            'name' => 'Preset 1',
            'main-body' => 'light',
            'bgstyle' => 'wavy-lines-light',
            'main-accent' => '#00AFF0'
        ),

        'preset2' => array (
            'name' => 'Preset 2',
            'main-body' => 'dark',
            'bgstyle' => 'diamond-dark',
            'main-accent' => '#F38D2A'
        ),

        'preset3' => array (
            'name' => 'Preset 3',
            'main-body' => 'dark',
            'bgstyle' => 'diagonal-lines-light',
            'main-accent' => '#7EAF28'
        ),

        'preset4' => array (
            'name' => 'Preset 4',
            'main-body' => 'light',
            'bgstyle' => 'cube-light',
            'main-accent' => '#E3231D'
        ),

        'preset5' => array (
            'name' => 'Preset 5',
            'main-body' => 'dark',
            'bgstyle' => 'squares-dark',
            'main-accent' => '#ABA0C6'
        ),

        'preset6' => array (
            'name' => 'Preset 6',
            'main-body' => 'light',
            'bgstyle' => 'textile-light',
            'main-accent' => '#F5859D'
        ),

        'preset7' => array (
            'name' => 'Preset 7',
            'main-body' => 'light',
            'bgstyle' => 'wavy-lines-dark',
            'main-accent' => '#CA572A'
        ),

        'preset8' => array (
            'name' => 'Preset 8',
            'main-body' => 'light',
            'bgstyle' => 'triangles-light',
            'main-accent' => '#4693B8'
        ),

        'preset9' => array (
            'name' => 'Preset 9',
            'main-body' => 'dark',
            'bgstyle' => 'paper-light',
            'main-accent' => '#DA874A'
        ),

        'preset10' => array (
            'name' => 'Preset 10',
            'main-body' => 'light',
            'bgstyle' => 'rubber-dark',
            'main-accent' => '#178F8C'
        ),

        'preset11' => array (
            'name' => 'Preset 11',
            'main-body' => 'dark',
            'bgstyle' => 'elegant-dark',
            'main-accent' => '#F8B923'
        ),

        'preset12' => array (
            'name' => 'Preset 12',
            'main-body' => 'light',
            'bgstyle' => 'diamond-light',
            'main-accent' => '#7EAF28'
        ),

    )
);

$gantry_browser_params = array(
    'ie6' => array(
		'typography' => '0',
		'extensions' => '0',
		'smartload-enabled' => '0',
		'buildspans-enabled' => '0',
		'inputstyling' => '0'
    )
);

$gantry_belatedPNG = array('.png', '#rt-logo', '.component-block', '#rocket');