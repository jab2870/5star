<?php
/**
 * @version   1.1 November 13, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
// no direct access
defined('ABSPATH') or die('Restricted access');

// Create a shortcut for params.
$category = get_the_category();
?>

			<?php /** Begin Post **/ ?>
				
			<div class="rt-article">
				<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
					<div class="rt-article-bg">
						<div class="article-header">

							<?php /** Begin Article Title **/ ?>

							<?php if ($gantry->get('blog-title')) : ?>

								<h1 class="title">
									<?php if ($gantry->get('blog-link-title')) : ?>
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
									<?php else : ?>
										<?php the_title(); ?>
									<?php endif; ?>
								</h1>

							<?php endif; ?>

							<?php /** End Article Title **/ ?>

							<?php /** Begin Extended Meta **/ ?>
			
							<?php if ($gantry->get('blog-meta-date') || $gantry->get('blog-meta-modified') || $gantry->get('blog-meta-author') || $gantry->get('blog-meta-comments')) : ?>
							
								<div class="rt-articleinfo">
									<div class="rt-articleinfo-text">
										<div class="rt-articleinfo-text2">

											<?php /** Begin Date & Time **/ ?>

											<?php if($gantry->get('blog-meta-date')) : ?>

												<span class="rt-date-posted"><!--<?php _re('Created on'); ?> --><?php the_time('d F Y'); ?></span>

											<?php endif; ?>

											<?php /** End Date & Time **/ ?>

											<?php /** Begin Modified Date **/ ?>
						
											<?php if($gantry->get('blog-meta-modified')) : ?>
						
												<span class="rt-date-modified"><?php _re('Last Updated on'); ?> <?php the_modified_date('d F Y'); ?></span>
						
											<?php endif; ?>
						
											<?php /** End Modified Date **/ ?>
									
											<?php /** Begin Author **/ ?>
										
											<?php if ($gantry->get('blog-meta-author')) : ?>

												<span class="rt-author">
													<?php the_author(); ?>
												</span>

											<?php endif; ?>
						
											<?php /** End Author **/ ?>
						
											<?php /** Begin Comments Count **/ ?>
						
											<?php if($gantry->get('blog-meta-comments')) : ?>
						
												<?php if($gantry->get('blog-meta-link-comments')) : ?>
						
													<span class="rt-comments-count">
														<a href="<?php the_permalink(); ?>#comments">
															<?php comments_number(_r('0 Comments'), _r('1 Comment'), _r('% Comments')); ?>
														</a>
													</span>
						
												<?php else : ?>
						
													<span class="rt-comments-count"><?php comments_number(_r('0 Comments'), _r('1 Comment'), _r('% Comments')); ?></span>
						
												<?php endif; ?>
						
											<?php endif; ?>
						
											<?php /** End Comments Count **/ ?>

										</div>
									</div>
									<div class="clear"></div>
								</div>
							
							<?php endif; ?>
			
							<?php /** End Extended Meta **/ ?>

						</div>
					</div>

					<?php /** Begin Thumbnail **/ ?>
			
					<?php if(function_exists('the_post_thumbnail') && has_post_thumbnail()) : ?>
	
						<p>
							<?php the_post_thumbnail('gantryThumb', array('class' => 'rt-image '.$gantry->get('thumb-position'))); ?>			
						</p>
					
					<?php endif; ?>
	
					<?php /** End Thumbnail **/ ?>
					
					<?php /** Begin Post Content **/ ?>	
				
					<?php if($gantry->get('blog-content') == 'content') : ?>
					
						<?php the_content(false); ?>
										
					<?php else : ?>
										
						<?php the_excerpt(); ?>
											
					<?php endif; ?>
					
					<?php if(preg_match('/<!--more(.*?)?-->/', $post->post_content)) : ?>
					
						<p class="rt-readon-surround">																			
							<a href="<?php the_permalink(); ?>" class="readon"><span><?php echo $gantry->get('blog-readmore'); ?></span></a>
						</p>
					
					<?php endif; ?>
	
					<?php /** Begin Parent Category **/ ?>
							
					<?php if ($gantry->get('blog-type') == 'post' && ($gantry->get('blog-meta-category-parent') && $category[0]->parent != '0')) : ?>
	
						<div class="rt-parent-category">
							<?php
								$parent_category = get_category((int)$category[0]->parent);
								$title = $parent_category->cat_name;
								$link = get_category_link($parent_category);
								$url = '<a href="' . esc_url($link) . '">' . $title . '</a>'; 
							?>
		
							<?php if ($gantry->get('blog-meta-link-category-parent')) : ?>
								<?php echo $url; ?>
							<?php else : ?>
								<?php echo $title; ?>
							<?php endif; ?>
		
							<?php if ($gantry->get('blog-meta-category')) : ?>
								<?php echo ' - '; ?>
							<?php endif; ?>
						</div>
	
					<?php endif; ?>
	
					<?php /** End Parent Category **/ ?>
	
					<?php /** Begin Category **/ ?>
	
					<?php if ($gantry->get('blog-type') == 'post' && $gantry->get('blog-meta-category')) : ?>
	
						<div class="rt-category">
							<?php 
								$title = $category[0]->cat_name;
								$link = get_category_link($category[0]->cat_ID);
								$url = '<a href="' . esc_url($link) . '">' . $title . '</a>';
							?>
		
							<?php if ($gantry->get('blog-meta-link-category')) : ?>
								<?php echo $url; ?>
							<?php else : ?>
								<?php echo $title; ?>
							<?php endif; ?>
						</div>
	
					<?php endif; ?>
	
					<?php /** End Category **/ ?>
					
					<?php /** End Post Content **/ ?>

				</div>
			</div>
			
			<?php /** End Post **/ ?>

			<div class="item-separator"></div>