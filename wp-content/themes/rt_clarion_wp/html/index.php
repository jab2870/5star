<?php
/**
 * @version   1.1 November 13, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
// no direct access
defined('ABSPATH') or die('Restricted access');
?>

<?php global $post, $posts, $query_string; ?>

	<div class="rt-blog">
	
		<?php /** Begin Page Title **/ ?>

		<?php if ($gantry->get('blog-page-title') != '') : ?>
		
		<h1 class="rt-pagetitle">
			<?php echo $gantry->get('blog-page-title'); ?>
		</h1>
		
		<?php endif; ?>
		
		<?php /** End Page Title **/ ?>
		
		<?php /** Begin Query Setup **/ ?>

		<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			
		if ($gantry->get('blog-query') != '') : 
		
			$custom_query = new WP_Query('posts_per_page='.$gantry->get('blog-count').'&paged='.$paged.'&'.$gantry->get('blog-query'));
		
		else :
		
			$custom_query = new WP_Query('posts_per_page='.$gantry->get('blog-count').'&paged='.$paged.'&orderby='.$gantry->get('blog-order').'&cat='.$gantry->get('blog-cat').'&post_type='.$gantry->get('blog-type'));
		
		endif;
		
		?>

		<?php /** End Query Setup **/ ?>

		<?php /** Begin Leading Posts **/ ?>

		<?php if($custom_query->have_posts() && $gantry->get('blog-lead-items') > 0) : ?>

			<?php $leadingcount = 0; 
			if($gantry->get('blog-lead-items') > $gantry->get('blog-count')) $gantry->set('blog-lead-items', $gantry->get('blog-count')); ?>

			<div class="rt-leading-articles">

				<?php while ($custom_query->have_posts()) : $custom_query->the_post(); ?>

				<div class="leading-<?php echo $leadingcount; ?>">
			
					<?php $this->locate_type(array('post-blog.php'), true, false); ?>

				</div>

				<?php $leadingcount++; ?>

				<?php if($leadingcount == $gantry->get('blog-lead-items') || $leadingcount == $gantry->get('blog-count')) break; ?>

				<?php endwhile; ?>

			</div>

		<?php endif; ?>

		<?php /** End Leading Posts **/ ?>

		<?php /** Begin Posts **/ ?>

		<?php if($custom_query->have_posts()) : ?>

			<?php $introcount = ($custom_query->post_count - $leadingcount); 
			$counter = 0; 
			if($gantry->get('blog-columns') <= 0) $gantry->set('blog-columns', 1);
			if($gantry->get('blog-columns') > 4) $gantry->set('blog-columns', 4); ?>

			<div class="rt-teaser-articles">

				<?php while ($custom_query->have_posts()) : $custom_query->the_post(); ?>

				<?php $key = ($custom_query->current_post - $leadingcount) + 1;
				$rowcount = ( ((int)$key - 1) % (int) $gantry->get('blog-columns')) + 1;
				$row = $counter / $gantry->get('blog-columns');

				if ($rowcount == 1) : ?>
				<div class="items-row cols-<?php echo (int) $gantry->get('blog-columns'); ?> <?php echo 'row-' . $row; ?>">
				<?php endif; ?>

				<div class="item column-<?php echo $rowcount;?>">

					<?php $this->locate_type(array('post-blog.php'), true, false); ?>
				
				</div>

				<?php $counter++; ?>
				<?php if (($rowcount == $gantry->get('blog-columns')) || ($counter == $introcount)) : ?>
					<span class="row-separator"></span>
				</div>
				<?php endif; ?>

				<?php endwhile; ?>

			</div>

		<?php endif; ?>

		<?php /** End Posts **/ ?>

		<div class="clear"></div>
		
		<?php /** Begin Navigation **/ ?>
		
		<?php if($gantry->get('pagination-style') == 'full' && $custom_query->max_num_pages > 1) { ?>
	
			<?php if (!$current_page = get_query_var('paged')) $current_page = 1;
			
			$permalinks = get_option('permalink_structure');
			if(is_front_page()) {
				$format = empty($permalinks) ? '?paged=%#%' : 'page/%#%/';
			} else {
				$format = empty($permalinks) ? '&paged=%#%' : 'page/%#%/';
			}
			
			$pagination = paginate_links(array(
				'base' => get_pagenum_link(1) . '%_%',
				'format' => $format,
				'current' => $current_page,
				'total' => $custom_query->max_num_pages,
				'mid_size' => $gantry->get('pagination-count'),
				'type' => 'array',
				'next_text' => _r('Next').' &raquo;',
				'prev_text' => '&laquo; '._r('Previous')
			));
			
			$count = count($pagination);
			$last = $count-1;
			
			(preg_match('/class="prev/i', $pagination[0]) == 1) ? $pagination[0] .= '<div class="pages-nav">' : $pagination[0] = '<div class="pages-nav">' . $pagination[0];
			(preg_match('/class="next/i', $pagination[$last]) == 1) ? $pagination[$last] = '</div>' . $pagination[$last] : $pagination[$last] .= '</div>';
			
			?>
			
			<div class="rt-pagination nav">
				<div class="full-nav">
			
					<?php foreach($pagination as $page) {
					
						$page = preg_replace('/prev page-numbers/i', 'newer_posts', $page);
						$page = preg_replace('/next page-numbers/i', 'older_posts', $page);
						
						echo $page;
					} ?>
				
				</div>
				<div class="clear"></div>
			</div>
									
		<?php } else { ?>							
									
			<?php if($custom_query->max_num_pages > 1) : ?>
					
			<div class="rt-pagination nav">
				<div class="alignleft">
					<?php next_posts_link('&laquo; '._r('Previous'), $custom_query->max_num_pages); ?>
				</div>
				<div class="alignright">
					<?php previous_posts_link(_r('Next').' &raquo;', $custom_query->max_num_pages); ?>
				</div>
				<div class="clear"></div>
			</div>
						
			<?php endif; ?>

		<?php } ?>

		<?php /** End Navigation **/ ?>
		
	</div>