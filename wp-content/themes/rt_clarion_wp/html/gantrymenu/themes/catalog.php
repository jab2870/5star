<?php
/**
 * @version   1.1 November 13, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

require_once(dirname(__FILE__).'/clarion_fusion/theme.php');
GantryWidgetMenu::registerTheme(dirname(__FILE__).'/clarion_fusion','clarion_fusion', __('Clarion Fusion'), 'ClarionFusionMenuTheme');

require_once(dirname(__FILE__).'/clarion_splitmenu/theme.php');
GantryWidgetMenu::registerTheme(dirname(__FILE__).'/clarion_splitmenu','clarion_splitmenu', __('Clarion SplitMenu'), 'ClarionSplitMenuTheme');

require_once(dirname(__FILE__).'/clarion_touch/theme.php');
GantryWidgetMenu::registerTheme(dirname(__FILE__).'/clarion_touch','clarion_touch', __('Clarion Touch'), 'ClarionTouchMenuTheme');