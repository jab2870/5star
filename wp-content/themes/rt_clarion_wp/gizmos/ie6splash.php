<?php
/**
 * @version   1.1 November 13, 2012
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

defined('GANTRY_VERSION') or die();

gantry_import('core.gantrygizmo');

/**
 * @package     gantry
 * @subpackage  features
 */
class GantryGizmoIE6Splash extends GantryGizmo {

    var $_name = 'ie6splash';

	function isEnabled() {
	    if ($this->get('enabled')) {
	    	return true;
	    }
    }

    function init() {
        
        global $gantry;
        
        $request = basename($_SERVER['REQUEST_URI']);
		
		if ($request != 'unsupported.php' && $gantry->browser->name == 'ie' && $gantry->browser->shortversion == '6') { 
			add_filter('template_include', array('GantryGizmoIE6Splash', 'ie6splash_redirect'));     
        }

    }
    
    static function ie6splash_redirect($template) {
    	return locate_template(array('unsupported.php'));
    }
    
}