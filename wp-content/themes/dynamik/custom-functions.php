<?php
/* Put Any Custom PHP Functions For Your WordPress Site In This File */

//end custom-functions.php

// Remove WooCommerce content wrappers.
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
// Add opening Catalyst content wrappers, organizing them using different hook priorities.
add_action('woocommerce_before_main_content', create_function('', 'echo "<div id=\"container-wrap\" class=\"clearfix\"><div id=\"container\">";'), 10);
add_action('woocommerce_before_main_content', create_function('', 'echo "<div id=\"content-sidebar-wrap\">";'), 12);
add_action('woocommerce_before_main_content', create_function('', 'echo "<div id=\"content-wrap\"><div id=\"content\">";'), 14);
// Add closing Catalyst content wrappers, organizing them using different hook priorities.
add_action('woocommerce_after_main_content', create_function('', 'echo "</div></div>";'), 14);
add_action('woocommerce_after_main_content', create_function('', 'echo "</div>";'), 12);
add_action('woocommerce_after_main_content', create_function('', 'echo "</div></div>";'), 10);

// Remove WooCommerce sidebar.
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
// Add Catalyst sidebars, organizing them using different hook priorities.
add_action( 'woocommerce_after_main_content', 'catalyst_dual_sidebars', 11);
add_action( 'woocommerce_after_main_content', 'catalyst_sidebar_1', 11);
add_action( 'woocommerce_after_main_content', 'catalyst_sidebar_2', 13);

// Remove WooCommerce breadcrumbs.
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
// Add Catalyst breadcrumbs.
add_action( 'woocommerce_before_main_content', 'catalyst_build_breadcrumbs', 20, 0);  

/*Change default column layout to 3, instead of 4 */
/*
global $woocommerce_loop;
$woocommerce_loop['columns'] = 3;

*/
